package gh.com.dialect.adb.bi.services;

import gh.com.dialect.adb.mm.adb.model.XMLFormatter;
import gh.com.dialect.adb.mm.adb.webserviceUtils.SOAPMessageReader;

public class Test {
	private String response = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" + 
			"  <S:Body>\n" + 
			"    <CREATEFTCONTRACT_FSFS_RES xmlns=\"http://fcubs.ofss.com/service/FCUBSFTService\">\n" + 
			"      <FCUBS_HEADER>\n" + 
			"        <SOURCE>FLEXCUBE</SOURCE>\n" + 
			"        <UBSCOMP>FCUBS</UBSCOMP>\n" + 
			"        <MSGID>9132750008981588</MSGID>\n" + 
			"        <USERID>TRANSUSER</USERID>\n" + 
			"        <BRANCH>000</BRANCH>\n" + 
			"        <MODULEID>FT</MODULEID>\n" + 
			"        <SERVICE>FCUBSFTService</SERVICE>\n" + 
			"        <OPERATION>CreateFTContract</OPERATION>\n" + 
			"        <SOURCE_USERID>TRANSUSER</SOURCE_USERID>\n" + 
			"        <DESTINATION>TRANSFLOW</DESTINATION>\n" + 
			"        <MULTITRIPID>6132750048587169</MULTITRIPID>\n" + 
			"        <FUNCTIONID>FTDCONON</FUNCTIONID>\n" + 
			"        <ACTION>NEW</ACTION>\n" + 
			"        <MSGSTAT>SUCCESS</MSGSTAT>\n" + 
			"      </FCUBS_HEADER>\n" + 
			"      <FCUBS_BODY>\n" + 
			"        <Contract-Details>\n" + 
			"          <XREF>2011124093</XREF>\n" + 
			"          <FCCREF>000INTL132480020</FCCREF>\n" + 
			"          <FCCESN>2</FCCESN>\n" + 
			"          <EVENT>INIT</EVENT>\n" + 
			"          <LATESTVERNO>1</LATESTVERNO>\n" + 
			"          <FCCVER>1</FCCVER>\n" + 
			"          <PROD>INTL</PROD>\n" + 
			"          <PRODESC>TRANSFLOW INTERNALTRANSFER</PRODESC>\n" + 
			"          <BNKOPRCD>CRED</BNKOPRCD>\n" + 
			"          <USRREF>000INTL132480020</USRREF>\n" + 
			"          <EXTREFNO>2011124093</EXTREFNO>\n" + 
			"          <BOOKDT>2013-09-05</BOOKDT>\n" + 
			"          <DRCCY>GHS</DRCCY>\n" + 
			"          <CRCCY>GHS</CRCCY>\n" + 
			"          <DRAMT>10</DRAMT>\n" + 
			"          <CRAMT>10</CRAMT>\n" + 
			"          <DRACCBRN>106</DRACCBRN>\n" + 
			"          <CRACCBRN>503</CRACCBRN>\n" + 
			"          <DRACC>1061010004086101</DRACC>\n" + 
			"          <CRACC>5031000028738301</CRACC>\n" + 
			"          <DRVDT>2013-09-05</DRVDT>\n" + 
			"          <CRVDT>2013-09-05</CRVDT>\n" + 
			"          <DRSPRED>0</DRSPRED>\n" + 
			"          <DRSPREDDATE>2013-09-05</DRSPREDDATE>\n" + 
			"          <CRSPRED>0</CRSPRED>\n" + 
			"          <CRSPREDDATE>2013-09-05</CRSPREDDATE>\n" + 
			"          <RATETYPE>STANDARD</RATETYPE>\n" + 
			"          <SPRDCD>1</SPRDCD>\n" + 
			"          <LCYEQ>10</LCYEQ>\n" + 
			"          <BASERATE>1</BASERATE>\n" + 
			"          <XRATE>1</XRATE>\n" + 
			"          <CHGWHOM>O</CHGWHOM>\n" + 
			"          <MSGASOF>D</MSGASOF>\n" + 
			"          <RTASOF>N</RTASOF>\n" + 
			"          <ACTGASOF>M</ACTGASOF>\n" + 
			"          <OVDOVRDRFT>N</OVDOVRDRFT>\n" + 
			"          <REMITMSG>N</REMITMSG>\n" + 
			"          <BYORDOF1>0000036</BYORDOF1>\n" + 
			"          <ULTBEN1>DAVID  TUTU</ULTBEN1>\n" + 
			"          <ULTBEN2>P O BOX 2 WEGBW KPALIME</ULTBEN2>\n" + 
			"          <ULTBEN3>WK 60 W WEGBE KPALIME</ULTBEN3>\n" + 
			"          <ULTBEN5>WEGBE KPALIME</ULTBEN5>\n" + 
			"          <MSGDATE>2013-09-05</MSGDATE>\n" + 
			"          <ACTGDT>2013-09-05</ACTGDT>\n" + 
			"          <RATEPICKDT>2013-09-05</RATEPICKDT>\n" + 
			"          <MLTCUSTRF>N</MLTCUSTRF>\n" + 
			"          <STORDCUST>0000036</STORDCUST>\n" + 
			"          <STOURSELVES>AGRICULTURAL DEVELOPMENT BANK</STOURSELVES>\n" + 
			"          <STULTBEN>DAVID  TUTU</STULTBEN>\n" + 
			"          <MAKERID>TRANSUSER</MAKERID>\n" + 
			"          <MAKERSTAMP>2013-09-05 14:44:26</MAKERSTAMP>\n" + 
			"          <CHECKERID>TRANSUSER</CHECKERID>\n" + 
			"          <CHECKERSTAMP>2013-09-05 14:44:27</CHECKERSTAMP>\n" + 
			"          <CONTSTAT>L</CONTSTAT>\n" + 
			"          <AUTHSTAT>A</AUTHSTAT>\n" + 
			"          <PROCESSTAT>A</PROCESSTAT>\n" + 
			"          <TRANSTYPE>C</TRANSTYPE>\n" + 
			"          <Settlement-Addl-Details>\n" + 
			"            <ACCBRN>106</ACCBRN>\n" + 
			"            <ACC_CCY>GHS</ACC_CCY>\n" + 
			"            <ACC>1061010004086101</ACC>\n" + 
			"            <AMTTAG>AMT_EQUIV</AMTTAG>\n" + 
			"            <XRATE>1</XRATE>\n" + 
			"            <GENMSG>Y</GENMSG>\n" + 
			"            <PAYRCV>R</PAYRCV>\n" + 
			"            <TAG_CCY>GHS</TAG_CCY>\n" + 
			"            <DRCRIND>D</DRCRIND>\n" + 
			"            <Settlement-Addl-Main>\n" + 
			"              <AMTTAG>AMT_EQUIV</AMTTAG>\n" + 
			"              <BNKPRIO>N</BNKPRIO>\n" + 
			"              <CHGDET>O</CHGDET>\n" + 
			"              <CVRBY>M</CVRBY>\n" + 
			"              <COVER_REQ>N</COVER_REQ>\n" + 
			"              <ORDER_CUST1>0000036</ORDER_CUST1>\n" + 
			"              <PMNT_BY>M</PMNT_BY>\n" + 
			"              <POSTACCNG>N</POSTACCNG>\n" + 
			"              <RECEIVER>00040861</RECEIVER>\n" + 
			"              <RTGSPMNT>N</RTGSPMNT>\n" + 
			"              <TRNFRTYP>X</TRNFRTYP>\n" + 
			"              <ULT_BENEF1>DAVID  TUTU</ULT_BENEF1>\n" + 
			"              <ULT_BENEF2>P O BOX 2 WEGBW KPALIME</ULT_BENEF2>\n" + 
			"              <ULT_BENEF3>WK 60 W WEGBE KPALIME</ULT_BENEF3>\n" + 
			"              <ULT_BENEF5>WEGBE KPALIME</ULT_BENEF5>\n" + 
			"              <AMTTAGDESC>INTERFACE LIMITED</AMTTAGDESC>\n" + 
			"              <WAVEINSTCHARG>N</WAVEINSTCHARG>\n" + 
			"              <BNKOPRCD>CRED</BNKOPRCD>\n" + 
			"            </Settlement-Addl-Main>\n" + 
			"          </Settlement-Addl-Details>\n" + 
			"          <Settlement-Addl-Details>\n" + 
			"            <ACCBRN>503</ACCBRN>\n" + 
			"            <ACC_CCY>GHS</ACC_CCY>\n" + 
			"            <ACC>5031000028738301</ACC>\n" + 
			"            <AMTTAG>TFR_AMT</AMTTAG>\n" + 
			"            <XRATE>1</XRATE>\n" + 
			"            <GENMSG>Y</GENMSG>\n" + 
			"            <PAYRCV>P</PAYRCV>\n" + 
			"            <TAG_CCY>GHS</TAG_CCY>\n" + 
			"            <DRCRIND>C</DRCRIND>\n" + 
			"            <Settlement-Addl-Main>\n" + 
			"              <AMTTAG>TFR_AMT</AMTTAG>\n" + 
			"              <BNKPRIO>N</BNKPRIO>\n" + 
			"              <CHGDET>O</CHGDET>\n" + 
			"              <CVRBY>M</CVRBY>\n" + 
			"              <COVER_REQ>N</COVER_REQ>\n" + 
			"              <ORDER_CUST1>0000036</ORDER_CUST1>\n" + 
			"              <OUR_CORRESP>00287383</OUR_CORRESP>\n" + 
			"              <PMNT_BY>M</PMNT_BY>\n" + 
			"              <POSTACCNG>N</POSTACCNG>\n" + 
			"              <RECEIVER>00287383</RECEIVER>\n" + 
			"              <RTGSPMNT>N</RTGSPMNT>\n" + 
			"              <TRNFRTYP>X</TRNFRTYP>\n" + 
			"              <ULT_BENEF1>DAVID  TUTU</ULT_BENEF1>\n" + 
			"              <ULT_BENEF2>P O BOX 2 WEGBW KPALIME</ULT_BENEF2>\n" + 
			"              <ULT_BENEF3>WK 60 W WEGBE KPALIME</ULT_BENEF3>\n" + 
			"              <ULT_BENEF5>WEGBE KPALIME</ULT_BENEF5>\n" + 
			"              <AMTTAGDESC>TUTU DAVID</AMTTAGDESC>\n" + 
			"              <WAVEINSTCHARG>N</WAVEINSTCHARG>\n" + 
			"              <BNKOPRCD>CRED</BNKOPRCD>\n" + 
			"            </Settlement-Addl-Main>\n" + 
			"          </Settlement-Addl-Details>\n" + 
			"          <Charge-Claim-Details>\n" + 
			"            <CHGREF>2011124093</CHGREF>\n" + 
			"          </Charge-Claim-Details>\n" + 
			"        </Contract-Details>\n" + 
			"        <FCUBS_WARNING_RESP>\n" + 
			"          <WARNING>\n" + 
			"            <WCODE>FT-VALS-201</WCODE>\n" + 
			"            <WDESC>Invalid line number in field 50F</WDESC>\n" + 
			"          </WARNING>\n" + 
			"          <WARNING>\n" + 
			"            <WCODE>AC-VAC05</WCODE>\n" + 
			"            <WDESC>Account 5031000028738301 Dormant</WDESC>\n" + 
			"          </WARNING>\n" + 
			"          <WARNING>\n" + 
			"            <WCODE>AC-TNK01</WCODE>\n" + 
			"            <WDESC>The Transaction is Tanked</WDESC>\n" + 
			"          </WARNING>\n" + 
			"          <WARNING>\n" + 
			"            <WCODE>AC-TNK01</WCODE>\n" + 
			"            <WDESC>The Transaction is Tanked</WDESC>\n" + 
			"          </WARNING>\n" + 
			"          <WARNING>\n" + 
			"            <WCODE>FT-RESP-002</WCODE>\n" + 
			"            <WDESC>Contract Successfully Saved and Authorized</WDESC>\n" + 
			"          </WARNING>\n" + 
			"        </FCUBS_WARNING_RESP>\n" + 
			"      </FCUBS_BODY>\n" + 
			"    </CREATEFTCONTRACT_FSFS_RES>\n" + 
			"  </S:Body>\n" + 
			"</S:Envelope>";
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		Test test = new Test();
		SOAPMessageReader reader = new SOAPMessageReader();
		String response = reader.findSOAPField(test.getResponse(), "MSGSTAT");
			System.out.println(response);
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}

}
