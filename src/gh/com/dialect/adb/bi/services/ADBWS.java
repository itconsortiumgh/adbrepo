package gh.com.dialect.adb.bi.services;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import gh.com.dialect.adb.cb.dao.GeneralDAO;
import gh.com.dialect.adb.exceptions.PendingTransException;
import gh.com.dialect.adb.exceptions.ServiceException;
import gh.com.dialect.adb.mm.adb.BusinessLogic;
import gh.com.dialect.adb.mm.adb.model.AppConstants;
import gh.com.dialect.adb.mm.adb.model.BalanceRequest;
import gh.com.dialect.adb.mm.adb.model.BridgeBalanceRequest;
import gh.com.dialect.adb.mm.adb.model.BridgeBalanceResponse;
import gh.com.dialect.adb.mm.adb.model.BridgeFTRequest;
import gh.com.dialect.adb.mm.adb.model.BridgeFTResponse;
import gh.com.dialect.adb.mm.adb.model.DuplicateTransaction;
import gh.com.dialect.adb.mm.adb.model.TransferRequest;
import gh.com.dialect.transflow.api.thirdparty.response.BalanceEnquiryResponse;
import gh.com.dialect.transflow.api.thirdparty.response.CustInformationResponse;
import gh.com.dialect.transflow.api.thirdparty.response.ResponseCode;
import gh.com.dialect.transflow.api.thirdparty.response.TransactionResponse;
import gh.com.dialect.transflow.bank.interfaces.IBankWsFacade;
public class ADBWS implements IBankWsFacade {

	static Logger log = Logger.getLogger(ADBWS.class);
	@Autowired
	BusinessLogic logic; 
	@Autowired
	GeneralDAO dao;
	
	RestTemplate restTemplate = new RestTemplate();
	@Autowired 
	AppConstants appConstants;

	@Autowired
	TransferRequest request;
	public static void main(String []args) throws ParseException {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("file:///Users/franklineleblu/Documents/workspace/corebank_facade_ADB/WebContent/WEB-INF/spring/applicationContext.xml");
		ADBWS service = (ADBWS)context.getBean("adbws");
		//		BalanceRequest req = new BalanceRequest();
		//		req.setBranchCode("111");
		//		req.setCustAcctNo("1111010033042601");
		//		BalanceEnquiryResponse response = service.getBalance("106", "1061010004086101", "10101", "TRANSUSER", "", "", "");
		//		log.info(response.getAvailBalance());
		//		log.info(response.getAccountNo());
		//		log.info(response.getBranch());

		TransactionResponse r = service.transferFunds("001", "12301", "002", "22303", new BigDecimal("10"), "GHC", "0001", "test", "use", "password", "testtransaction", "na");
		log.info(r.getResponseCode());
		log.info(r.getResponseMessage());
		
//		log.info(service.transferReversal("140228000002C", "000TFFT140380312", "TRANSUSER", "", "testing", "newID"));
	}
	//		http://download.oracle.com/otn-pub/java/jdk/7u40-b43/jre-7u40-linux-i586.rpm
	@Override
	public CustInformationResponse getCustInfo(String branch, String accountNo, String transflowTransactionId, String username, String password, String externalField1, String externalField2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized TransactionResponse transferFunds(String ddrBranch, String ddrAccountNo, String ccrBranch, 
			String ccrAccountNo, BigDecimal amount, String currency, String transflowTransactionId, String productCode, 
			String username, String password, String narration, String externalField2) {//narration
		log.info("**************************the currency from transflow is "+currency);
		TransactionResponse response = new TransactionResponse();
		request.setAmount(amount);
		request.setCcrAccountNo(ccrAccountNo);
		request.setDdrAccountNo(ddrAccountNo);
		request.setCcrBranch(ccrBranch);
		request.setCurrency(currency);
		request.setDdrBranch(ddrBranch);
		request.setNarration(narration);
		request.setTransflowTransactionId(transflowTransactionId);
		request.setUserID(username);
		request.setPassword(password);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");
		
		BridgeFTRequest bridgeRequest = new BridgeFTRequest();
		bridgeRequest.setAmount(amount.toString());
		bridgeRequest.setCcrAccountNo(ccrAccountNo);
		bridgeRequest.setDdrAccountNo(ddrAccountNo);
		bridgeRequest.setCurrency(currency);
		bridgeRequest.setNarration(narration);
		bridgeRequest.setTransflowTransactionId(transflowTransactionId);
		bridgeRequest.setTerminalId(username);
		
		BridgeFTResponse bridgeResponse = new BridgeFTResponse();
		String url = "";
		
		DuplicateTransaction duplicate = new DuplicateTransaction();
		try{
			duplicate = dao.isDuplicateTransaction(transflowTransactionId);
		}catch(PendingTransException e){
			response.setResponseCode(AppConstants.CODE_09_PENDING_TXN);
			response.setResponseMessage(e.getMessage());
			response.setTransflowTransactionId(transflowTransactionId);
			return response;
		}
		if(!duplicate.isDuplicate()){
			log.info("---------there is no duplicate (by our conditions)----------");
			if(duplicate.isFailedTransaction()){
				log.info("---------but there is a failed so we are going to reinitialize the status ----------");
				dao.reInitializeToTransControlTable(request);
			}else{
				log.info("---------so we are going to log the initial ----------");
				dao.logInitialToTransControlTable(request);
			}
			//			log.info("==========here is the request========="+request);
			try {
//				response = logic.transferFunds(request);
				String date = dateFormat.format(new Date());
				
				Gson gson = new GsonBuilder().create();
				String requestStr = gson.toJson(bridgeRequest, BridgeFTRequest.class);
				String responseStr = gson.toJson(bridgeResponse, BridgeFTResponse.class);
				
				url=appConstants.getBridgeFTEndpoint();
				log.info("the url : "+url);
				
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.setCacheControl("no-cache");
				
				log.info("the bridge request : "+requestStr);
				HttpEntity<String> entity = new HttpEntity<String>(requestStr, headers);
				
				responseStr = restTemplate.postForObject(url, entity, String.class);
				log.info("the bridge response String:  "+responseStr);
				
				bridgeResponse = gson.fromJson(responseStr, BridgeFTResponse.class);
				
				response.setResponseCode(bridgeResponse.getResponseCode());
				response.setResponseMessage(bridgeResponse.getResponseMessage());
				response.setTransflowTransactionId(bridgeResponse.getDoFTResponse().getTrx_id());
				response.setBankTransactionId(bridgeResponse.getDoFTResponse().getBank_trxid());
				response.setTransactionDate(dateFormat.parse(date));
			} 
//			catch (ServiceException e) { 
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("************** Service Exception "+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
//			} catch (WebserviceException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("**************"+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
//			} catch (ResponseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("**************"+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
//			}
			catch (Exception e) {
				e.printStackTrace();
				log.error("**************"+e.getMessage());
				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_FAILURE);
				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
			}
			if(StringUtils.equalsIgnoreCase(ResponseCode.CODE_OK, response.getResponseCode())){
				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_SUCCESS);
				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_SUCCESS);
			}else {
				log.info("^^^^^^^^^^^^^^^^^^^^^^^ " + transflowTransactionId +" ResponseCode is BAD so about to update the transaction control");
				log.error("**************"+response.getResponseMessage());
				dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
				log.info("^^^^^^^^^^^^^^^^^^^^^^^ " + transflowTransactionId +" ResponseCode is BAD so about to update the FT");
				dao.logUpdateTransControlTable(request, response, AppConstants.LOG_STATUS_FAILURE);
			}
		}else{
			response.setResponseCode(ResponseCode.CODE_DUPLICATE_TXN);
			response.setResponseMessage("DUPLICATE TRANSFLOW TRANSACTIONID" + transflowTransactionId);
			dao.logFTTransaction(request, response, AppConstants.LOG_STATUS_FAILURE);
		}
		// TODO Auto-generated method stub
		return response;
	}

	@Override
	public synchronized TransactionResponse transferReversal(String oldTransflowTransactionId, String bankTransactionId, String username, String password, String narration, String newTransactionId){
		log.info("old translfotransion Id = " + oldTransflowTransactionId);
		log.info("bankTransactionId " + bankTransactionId);
		log.info("newTransactionId" + newTransactionId);
		// TODO Auto-generated method stub
		TransactionResponse response = new TransactionResponse();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");	
		BridgeFTRequest bridgeRequest = new BridgeFTRequest();
		BridgeFTResponse bridgeResponse = new BridgeFTResponse();
		String url = "";
		try {
			TransferRequest tempReq = dao.findTransferRequestByTransflowTransId(oldTransflowTransactionId);
			//			log.info("tmpRequest as coming from database "+ tempReq);
			request.setAmount(tempReq.getAmount());
			request.setCcrAccountNo(tempReq.getCcrAccountNo());
			request.setCcrBranch(tempReq.getCcrBranch());
			request.setCurrency(tempReq.getCurrency());
			request.setDdrAccountNo(tempReq.getDdrAccountNo());
			request.setDdrBranch(tempReq.getDdrBranch());
			request.setNarration(narration);
			request.setPassword(tempReq.getPassword());
			request.setTransflowTransactionId(tempReq.getTransflowTransactionId());
			request.setUserID(username);
			request.setPassword(password);
			
			
			bridgeRequest.setAmount(tempReq.getAmount().toString());
			bridgeRequest.setCcrAccountNo(tempReq.getCcrAccountNo());
			bridgeRequest.setDdrAccountNo(tempReq.getDdrAccountNo());
			bridgeRequest.setCurrency(tempReq.getCurrency());
			bridgeRequest.setNarration(narration);
//			bridgeRequest.setTransflowTransactionId(tempReq.getTransflowTransactionId());
			bridgeRequest.setTransflowTransactionId(newTransactionId);
			bridgeRequest.setTerminalId(username);
			
			
			//			log.info("REVERSAL------------------------------original request before reversing mode "+ request);
		}catch (ServiceException e1) {
			e1.printStackTrace();
			response.setResponseCode(ResponseCode.CODE_INV_ACCT);
			log.error("REVERSAL**********There is no transaction associated with the transflowTransactionId " + oldTransflowTransactionId);
			response.setResponseMessage("REVERSAL: There is no transaction associated with the transflowTransactionId " + oldTransflowTransactionId);
			return response;
		}
		TransferRequest reversalRequest = request.reversePaymentMode(newTransactionId);
		//		try {
		log.info("REVERSAL-----------------------------reversalRequest after reversing Mode: "+ reversalRequest);

		DuplicateTransaction duplicate = new DuplicateTransaction();
		try{
			log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ tracing " + newTransactionId);
			duplicate = dao.isDuplicateTransaction(newTransactionId);
			log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" not duplicate so proceeding");
		}catch(PendingTransException e){
			log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" is pending so returning");
			response.setResponseCode(AppConstants.CODE_09_PENDING_TXN);
			response.setResponseMessage(e.getMessage());
			response.setTransflowTransactionId(newTransactionId);
			return response;
		}
		if(!duplicate.isDuplicate()){
			log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" about to log initial trans control");
			if(duplicate.isFailedTransaction()){
				dao.reInitializeToTransControlTable(reversalRequest);
			}else{
				dao.logInitialToTransControlTable(reversalRequest);
			}
			//			log.info("==========here is the request========="+request);
			try {
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" about to transfer funds");
//				response = logic.transferFunds(reversalRequest);
				
				String date = dateFormat.format(new Date());
				
				Gson gson = new GsonBuilder().create();
				String requestStr = gson.toJson(bridgeRequest, BridgeFTRequest.class);
				String responseStr = gson.toJson(bridgeResponse, BridgeFTResponse.class);
				
				url=appConstants.getBridgeFTEndpoint();
				log.info("the url : "+url);
				
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.setCacheControl("no-cache");
				
				log.info("the bridge request : "+requestStr);
				HttpEntity<String> entity = new HttpEntity<String>(requestStr, headers);
				
				responseStr = restTemplate.postForObject(url, entity, String.class);
				log.info("the bridge response String:  "+responseStr);
				
				bridgeResponse = gson.fromJson(responseStr, BridgeFTResponse.class);
				
				response.setResponseCode(bridgeResponse.getResponseCode());
				response.setResponseMessage(bridgeResponse.getResponseMessage());
				response.setTransflowTransactionId(bridgeResponse.getDoFTResponse().getTrx_id());
				response.setBankTransactionId(bridgeResponse.getDoFTResponse().getBank_trxid());
				response.setTransactionDate(dateFormat.parse(date));
				
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" fund transfer successful, I think, responseMessage = " + response.getResponseMessage());
			} 
//			catch (ServiceException e) { 
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("REVERSAL************** Service Exception "+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(reversalRequest, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
//			} catch (WebserviceException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("REVERSAL**************"+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(reversalRequest, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
//			} catch (ResponseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				log.error("REVERSAL**************"+e.getMessage());
//				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
//				dao.logUpdateTransControlTable(reversalRequest, response, AppConstants.LOG_STATUS_FAILURE);
//				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
//			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log.error("REVERSAL**************"+e.getMessage());
				response.setResponseCode(ResponseCode.CODE_GENERAL_FAILURE);
				dao.logUpdateTransControlTable(reversalRequest, response, AppConstants.LOG_STATUS_FAILURE);
				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
			}
			if(StringUtils.equalsIgnoreCase(ResponseCode.CODE_OK, response.getResponseCode())){
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" ResponseCode is OK so about to update the transaction control");
				dao.logUpdateTransControlTable(reversalRequest, response, AppConstants.LOG_STATUS_SUCCESS);
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" ResponseCode is OK so about to Log to FTControl");
				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_SUCCESS);
				log.info("------------"+response.getResponseMessage());
			}else {
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" ResponseCode is BAD so about to update the transaction control");
				log.error("REVERSAL**************"+response.getResponseMessage());
				dao.logFTTransaction(reversalRequest, response, AppConstants.LOG_STATUS_FAILURE);
				log.info("REVERSAL^^^^^^^^^^^^^^^^^^^^^^^ " + newTransactionId +" ResponseCode is BAD so about to update the FT");
				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
			}
		}else{
			response.setResponseCode(ResponseCode.CODE_DUPLICATE_TXN);
			response.setResponseMessage("DUPLICATE TRANSFLOW TRANSACTIONID" + newTransactionId);
			dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
		}

		//		response=	transferFunds(reversalRequest.getDdrBranch(), reversalRequest.getDdrAccountNo(), reversalRequest.getCcrBranch(), 
		//					reversalRequest.getCcrAccountNo(), reversalRequest.getAmount(), reversalRequest.getCurrency(), 
		//					reversalRequest.getTransflowTransactionId(), "NA", username, password, narration, "na");




		//			response = logic.transferFunds(reversalRequest);
		//			if(StringUtils.equalsIgnoreCase(ResponseCode.CODE_OK, response.getResponseCode())){
		//				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_SUCCESS);
		//				log.info("------------"+response.getResponseMessage());
		//			}else {
		//				log.error("**************"+response.getResponseMessage());
		//				dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
		//			}
		//		} catch (ServiceException e) {
		//			log.error("**************"+response.getResponseMessage());
		//			dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
		//			e.printStackTrace();
		//
		//		} catch (WebserviceException e) {
		//			log.error("**************"+response.getResponseMessage());
		//			dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
		//			e.printStackTrace();
		//		} catch (ResponseException e) {
		//			log.error("**************"+response.getResponseMessage());
		//			dao.logFReversalTransaction(reversalRequest, oldTransflowTransactionId, response, AppConstants.LOG_STATUS_FAILURE);
		//			e.printStackTrace();
		//		}
		return response;
	}

	@Override
	public BalanceEnquiryResponse getBalance(String branch, String accountNo, String transflowTransactionId, 
			String username, String password, String externalField1, String externalField2){//need to provide the name and address of the teller
		BalanceEnquiryResponse response = new BalanceEnquiryResponse();
		BalanceRequest request = new BalanceRequest();
		request.setBranchCode(branch);
		request.setCustAcctNo(accountNo);
		request.setUserID(username);
		//		response.setAccountNo("testAccount");
		//		response.setAvailBalance(new BigDecimal("250.80"));
		//		response.setBankTransactionId("121324134");
		//		response.setBranch("somebranch");
		//		response.setCurrency("GHS");
		//		response.setOpeningBalance(new BigDecimal("9.59"));
		//		response.setResponseCode(ResponseCode.CODE_OK);
		//		response.setResponseMessage("Kofi Nti P.O.Box 54, Accra Ghana");//This will return the name and address of the customer
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");
		
		BridgeBalanceRequest bridgeRequest  = new BridgeBalanceRequest();
		bridgeRequest.setTerminalId(username);
		bridgeRequest.setAccountNo(accountNo);
		bridgeRequest.setMerchant(appConstants.getFlexCubeSource());
		bridgeRequest.setCurrency(appConstants.getCurrency());
		String url = "";
		BridgeBalanceResponse bridgeResponse = new BridgeBalanceResponse();
		try {
//			response = logic.balanceInquiry(request);
			String date = dateFormat.format(new Date());
			
			url=appConstants.getBridgeBalEndpoint();
			log.info("the url : "+url);
			
	        Gson gson = new GsonBuilder().create();
	        String requestStr = gson.toJson(bridgeRequest, BridgeBalanceRequest.class);
		    String responseStr = gson.toJson(bridgeResponse, BridgeBalanceResponse.class);   
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setCacheControl("no-cache");
			
			log.info("the bridge request : "+requestStr);
			HttpEntity<String> entity = new HttpEntity<String>(requestStr, headers);
			
			responseStr = restTemplate.postForObject(url, entity, String.class);
			log.info("the bridge response :  "+responseStr);
			
			bridgeResponse = gson.fromJson(responseStr, BridgeBalanceResponse.class);
			response.setAccountNo(bridgeResponse.getGetBalanceResponse().getAccount_number());
			response.setAvailBalance(new BigDecimal(bridgeResponse.getGetBalanceResponse().getAvailable_balance()));
			response.setCurrentBalance(new BigDecimal(bridgeResponse.getGetBalanceResponse().getBalance()));
					
			response.setTransactionDate(dateFormat.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
	
}
