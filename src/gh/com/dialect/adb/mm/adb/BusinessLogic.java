package gh.com.dialect.adb.mm.adb;

import gh.com.dialect.adb.exceptions.ResponseException;
import gh.com.dialect.adb.exceptions.ServiceException;
import gh.com.dialect.adb.exceptions.WebserviceException;
import gh.com.dialect.adb.mm.adb.model.AppConstants;
import gh.com.dialect.adb.mm.adb.model.BalanceRequest;
import gh.com.dialect.adb.mm.adb.model.TransferRequest;
import gh.com.dialect.adb.mm.adb.model.XMLFormatter;
import gh.com.dialect.adb.mm.adb.webserviceUtils.SOAPClientUtil;
import gh.com.dialect.adb.mm.adb.webserviceUtils.SOAPMessageReader;
import gh.com.dialect.transflow.api.thirdparty.response.BalanceEnquiryResponse;
import gh.com.dialect.transflow.api.thirdparty.response.ResponseCode;
import gh.com.dialect.transflow.api.thirdparty.response.TransactionResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
//-keep -d generated_src -target 2.0
public class BusinessLogic {
	@Autowired
	AppConstants appConstants;
	private static final Log log = LogFactory.getLog(BusinessLogic.class);
	public static void main(String[] args) throws Exception {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("file:///Users/franklineleblu/Documents/workspace/corebank_facade_ADB/WebContent/WEB-INF/spring/applicationContext.xml");
		BusinessLogic service = (BusinessLogic)context.getBean("logic");
		//		BusinessLogic service = new BusinessLogic();
		TransferRequest request =(TransferRequest) context.getBean("request");
		//		request.setFtEndpoint("http://10.10.1.40:8112/FCUBSFTService/FCUBSFTService?WSDL");
		request.setAmount(new BigDecimal("5"));
		request.setDdrBranch("106");
		request.setDdrAccountNo("1061010004086101");
		request.setCurrency("GHS");
		request.setTransflowTransactionId("20111240936");
		request.setUserID("TRANSUSER");
		request.setCcrBranch("503");
		request.setCcrAccountNo("5031000028738301");
		request.setCurrency("GHS");
		request.setNarration("This is a test transaction");
		//		BalanceRequest req = new BalanceRequest();
		//		req.setBeEndpoint("http://10.10.1.40:8112/FCUBSAccService/FCUBSAccService?WSDL");
		//		req.setBankID("ADB");
		//		req.setUserID("TRANSUSER");

		////////////////////////////////////////////////////////////////////
		///PRE TRANSFER BALANCE
		////////////////////////////////////////////////////////////////////
		//		req.setBranchCode("503");
		//		req.setCustAcctNo("5031000028738301");
		//		BalanceEnquiryResponse resp = service.balanceInquiry(req);
		//		log.info("balace before transaction: "+resp.getAvailBalance());
		//		log.info("currency before transaction: "+resp.getCurrency());
		//		log.info("message before transaction: "+resp.getResponseMessage());
		//		log.info("branch transaction: "+resp.getBranch());
		//		
//		TransactionResponse r= service.transferFunds(request);
//		System.out.println("transactionId: ==================== " +r.getBankTransactionId());
//		System.out.println(r.getResponseCode());
//		System.out.println(r.getResponseMessage());

		//		
		//		////////////////////////////////////////////////////////////////////
		//		///POST TRANSFER BALANCE
		//		////////////////////////////////////////////////////////////////////
		//		resp = service.balanceInquiry(req);
		//		log.info("balace after transaction: "+resp.getAvailBalance());
		//		log.info("currency after transaction: "+resp.getCurrency());
		//		log.info("message after transaction: "+resp.getResponseMessage());
		//		log.info("branch transaction: "+resp.getBranch());
	}

	/**
	 * Debits the customer's account and credits the Bank Integration Account
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	/**
	 * 
	 * @param biRequest
	 * @return
	 */
	public BalanceEnquiryResponse balanceInquiry(BalanceRequest biRequest)throws Exception{
		SOAPClientUtil soapUtil = new SOAPClientUtil();

		BalanceEnquiryResponse response = new BalanceEnquiryResponse();
		String beEndpoint =appConstants.getBeEndpoint();
		log.info("balanceEnquiry endpoint: " + beEndpoint);
		String responseXml = "";
		String result = "";
		String soapRequest = biRequest.createBISOAPMessage();
		log.info("===================");
		log.info(new XMLFormatter().format(soapRequest));
		log.info("===================");
		responseXml = soapUtil.sendMessage(beEndpoint, biRequest.createBISOAPMessage());
		XMLFormatter format = new XMLFormatter();
		String rform = format.format(responseXml);
		System.out.println(rform);
		InputStream in = new ByteArrayInputStream(responseXml.getBytes());
		DocumentBuilderFactory docFactory = null;  
		DocumentBuilder docBuilder = null;  
		Document doc = null;  
		docFactory = DocumentBuilderFactory.newInstance();  
		docBuilder = docFactory.newDocumentBuilder();  
		doc = docBuilder.parse(in);  

		SOAPMessageReader reader = new SOAPMessageReader();
		String status = reader.findSOAPField(doc, "MSGSTAT");
		if("SUCCESS".equals(status)){
			response.setResponseCode(AppConstants.RESPONSECODE_OK);
			response.setResponseMessage(status);

			//Account Number
			String custAccNum = reader.findSOAPField(doc, "CUST_AC_NO");
			response.setAccountNo(custAccNum);

			String transactionId = reader.findSOAPField(doc, "USRREF");
			response.setBankTransactionId(transactionId);
			//Banch
			String branch =reader.findSOAPField(doc, "BRANCH_CODE");
			response.setBranch(branch);


			//Currency
			String currency = reader.findSOAPField(doc, "CCY");
			response.setCurrency(currency);

			//Transaction Date
			String transactionDate = reader.findSOAPField(doc,"TRNDT");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			response.setTransactionDate(dateFormat.parse(transactionDate));

			//Opening Balance
			String opnBal = reader.findSOAPField(doc,"OPNBAL");
			BigDecimal openingBalance = new BigDecimal(opnBal);
			response.setOpeningBalance(openingBalance);

			//Current Balance
			String curBal = reader.findSOAPField(doc,"CURBAL");
			BigDecimal currentBalance = new BigDecimal(curBal);
			response.setCurrentBalance(currentBalance);

			//Available Balance
			String availBal = reader.findSOAPField(doc,"AVLBAL");
			BigDecimal availBalance = new BigDecimal(availBal);
			response.setAvailBalance(availBalance);


		}else{
			String errorcode = reader.findSOAPField(doc,"ECODE");

			response.setResponseCode(AppConstants.RESPONSECODE_ERROR);
			response.setAvailBalance(new BigDecimal("0.00"));

			NodeList list3 = doc.getElementsByTagName("EDESC");
			String errordescription = list3.item(0).getTextContent();
			response.setResponseMessage(errordescription);
		}

		return response;
	}

	/**
	 * @param ftRequest
	 * @return
	 * @throws ServiceException 
	 * @throws WebserviceException 
	 * @throws ResponseException 
	 */
	public TransactionResponse transferFunds(TransferRequest ftRequest) throws ServiceException, WebserviceException, ResponseException{
		SOAPClientUtil soapUtil = new SOAPClientUtil();
		TransactionResponse response = new TransactionResponse();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:hh:mm:ss");
		String date = dateFormat.format(new Date());
		try {
			response.setTransactionDate(dateFormat.parse(date));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		String ftEndpoint = appConstants.getFtEndpoint();
		String responseXml = "";
		String result = "";
		String ftRequestString;
		try {
			ftRequestString = ftRequest.createFTSOAPMessage();
			XMLFormatter format = new XMLFormatter();
			log.info("-----------------------------START REQUEST STRING------------------------");
			log.info(format.format(ftRequestString));
			log.info("-----------------------------END REQUEST STRING------------------------");
		} catch (ServiceException e) {
			e.printStackTrace();
			throw new ServiceException(e.getMessage());
		} 
		try {
			responseXml = soapUtil.sendMessage(ftEndpoint, ftRequestString);
			XMLFormatter format = new XMLFormatter();
			log.info("-----------------------------START RESPONSE STRING------------------------");
			log.info(format.format(responseXml));
			log.info("-----------------------------END RESPONSE STRING------------------------");
			
		} catch (WebserviceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new WebserviceException(e1.getMessage());
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ServiceException(e.getMessage());
		}

		XMLFormatter format = new XMLFormatter();
//		log.info(format.format(responseXml));
		InputStream in = new ByteArrayInputStream(responseXml.getBytes());

		DocumentBuilderFactory docFactory = null;  
		DocumentBuilder docBuilder = null;  
		Document doc = null;  
		docFactory = DocumentBuilderFactory.newInstance();  
		try {
			docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.parse(in);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ResponseException(e.getMessage());
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ResponseException(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ResponseException(e.getMessage());
		}  

		NodeList list2 = doc.getElementsByTagName("ECODE");
		String errorcode = "";
		if(list2.item(0)!=null){
			errorcode = list2.item(0).getTextContent();
		}
		NodeList list3 = doc.getElementsByTagName("EDESC");
		String errordescription = "";
		if(list3.item(0)!=null){
			errordescription = list3.item(0).getTextContent();
		}
		NodeList list = doc.getElementsByTagName("MSGSTAT");
		if(list.getLength() > 0){
			Node node = list.item(0);
			if("SUCCESS".equals(node.getTextContent())){
				NodeList msgId = doc.getElementsByTagName("USRREF");
				String transactionId = msgId.item(0).getTextContent();
				response.setBankTransactionId(transactionId);
				if(StringUtils.isBlank(errorcode)||StringUtils.isEmpty(errorcode)){
					response.setResponseCode(AppConstants.RESPONSECODE_OK);
					response.setResponseMessage(node.getTextContent());
					response.setTransflowTransactionId(ftRequest.getTransflowTransactionId());
				}else{
					response.setResponseCode(AppConstants.RESPONSECODE_ERROR);
					response.setResponseMessage(errordescription);
					response.setTransflowTransactionId(ftRequest.getTransflowTransactionId());
				}
			}else{
				if(errordescription.contains("Duplicate")){
					log.info("It certainly begins with duplicate");
					response.setResponseCode(ResponseCode.CODE_DUPLICATE_TXN);
					response.setResponseMessage(errordescription);
				}else{
					response.setResponseMessage(errordescription);
					response.setResponseCode(AppConstants.RESPONSECODE_ERROR);
				}
				response.setTransflowTransactionId(ftRequest.getTransflowTransactionId());
			}
		}else{
			response.setResponseMessage(errordescription);
			response.setResponseCode(AppConstants.RESPONSECODE_ERROR);
			response.setTransflowTransactionId(ftRequest.getTransflowTransactionId());
		}
		return response;
	}
}