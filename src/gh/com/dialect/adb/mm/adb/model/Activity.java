package gh.com.dialect.adb.mm.adb.model;

public class Activity {
	private String ipAddress;
	private String connId;
	private String subscriberNum;
	private String command;
	private String remarks;
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getConnId() {
		return connId;
	}
	public void setConnId(String connId) {
		this.connId = connId;
	}
	public String getSubscriberNum() {
		return subscriberNum;
	}
	public void setSubscriberNum(String subscriberNum) {
		this.subscriberNum = subscriberNum;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
