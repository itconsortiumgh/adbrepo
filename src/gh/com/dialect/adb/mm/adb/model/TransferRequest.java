package gh.com.dialect.adb.mm.adb.model;

import gh.com.dialect.adb.exceptions.ServiceException;

import java.math.BigDecimal;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TransferRequest {
	@Autowired
	AppConstants appConstants;
	private static final Log log = LogFactory.getLog(TransferRequest.class);
	public static void main(String args[]) throws Exception{
		TransferRequest request = new TransferRequest();
		request.setAmount(new BigDecimal(100));
		request.setDdrAccountNo("1080000003638");
		request.setCcrAccountNo("2030016530464");
		request.setDdrBranch("001");
		request.setCcrBranch("005");
		request.setTransflowTransactionId("14878229200019");
		request.setUserID("TRANSUSER");
		String response = request.createFTSOAPMessage();
		System.out.println(response);
	}

	private String ddrBranch;
	private String ddrAccountNo;
	private String ccrBranch;
	private String ccrAccountNo;
	private BigDecimal amount;
	private String currency;
	private String UserID;
	private String transflowTransactionId;
	private String narration;
	private String password;
	private String transStatus;
	String responseXml="";

	public TransferRequest reversePaymentMode(String newTransactionId) {
		String tmpddrBranch = this.ddrBranch;
		String tmpddrAccountNo = this.ddrAccountNo;
		String tmpccrBranch = this.ccrBranch;
		String tmpccrAccoutNo = this.ccrAccountNo;
		
		setDdrAccountNo(tmpccrAccoutNo);
		setDdrBranch(tmpccrBranch);
		setCcrAccountNo(tmpddrAccountNo);
		setCcrBranch(tmpddrBranch);
		setTransflowTransactionId(newTransactionId);
		return this;
		
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getTransflowTransactionId() {
		return transflowTransactionId;
	}
	public void setTransflowTransactionId(String transflowTransactionId) {
		this.transflowTransactionId = transflowTransactionId;
	}
	public String getDdrBranch() {
		return ddrBranch;
	}
	public void setDdrBranch(String ddrBranch) {
		this.ddrBranch = ddrBranch;
	}
	public String getCcrBranch() {
		return ccrBranch;
	}
	public void setCcrBranch(String ccrBranch) {
		this.ccrBranch = ccrBranch;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDdrAccountNo() {
		return ddrAccountNo;
	}
	public void setDdrAccountNo(String ddrAccountNo) {
		this.ddrAccountNo = ddrAccountNo;
	}
	public String getCcrAccountNo() {
		return ccrAccountNo;
	}
	public void setCcrAccountNo(String ccrAccountNo) {
		this.ccrAccountNo = ccrAccountNo;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String createFTSOAPMessage() throws ServiceException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new ServiceException(e.getMessage());
		}
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null,null,null);
		Element envelope = message.createElement("env:Envelope");
		envelope.setAttribute("xmlns:env", "http://schemas.xmlsoap.org/soap/envelope/");
		message.appendChild(envelope);

		Element header = message.createElement("env:Header");
		envelope.appendChild(header);

		Element body = message.createElement("env:Body");
		envelope.appendChild(body);

		Element CREATEFTCONTRACT_FSFS_REQ = message.createElement("CREATEFTCONTRACT_FSFS_REQ");
		CREATEFTCONTRACT_FSFS_REQ.setAttribute("xmlns", "http://fcubs.ofss.com/service/FCUBSFTService");
		body.appendChild(CREATEFTCONTRACT_FSFS_REQ);

		Element FCUBS_HEADER = message.createElement("FCUBS_HEADER");
		CREATEFTCONTRACT_FSFS_REQ.appendChild(FCUBS_HEADER);

		Element SOURCE = message.createElement("SOURCE");
		SOURCE.setTextContent(appConstants.getFlexCubeSource());
		FCUBS_HEADER.appendChild(SOURCE);

		Element UBSCOMP = message.createElement("UBSCOMP");
		UBSCOMP.setTextContent("FCUBS");
		FCUBS_HEADER.appendChild(UBSCOMP);

		Element MSGID = message.createElement("MSGID");
		FCUBS_HEADER.appendChild(MSGID);

		Element CORRELID = message.createElement("CORRELID");
		FCUBS_HEADER.appendChild(CORRELID);

		Element USERID = message.createElement("USERID");
		USERID.setTextContent(this.getUserID());
		FCUBS_HEADER.appendChild(USERID);

		Element BRANCH = message.createElement("BRANCH");
		BRANCH.setTextContent("000");
		FCUBS_HEADER.appendChild(BRANCH);

		Element MODULEID = message.createElement("MODULEID");
		MODULEID.setTextContent("FT");
		FCUBS_HEADER.appendChild(MODULEID);

		Element SERVICE = message.createElement("SERVICE");
		SERVICE.setTextContent("FCUBSFTService");
		FCUBS_HEADER.appendChild(SERVICE);

		Element OPERATION = message.createElement("OPERATION");
		OPERATION.setTextContent("CreateFTContract");
		FCUBS_HEADER.appendChild(OPERATION);

		Element SOURCE_OPERATION = message.createElement("SOURCE_OPERATION");
		SOURCE_OPERATION.setTextContent("CreateFTContract");
		FCUBS_HEADER.appendChild(SOURCE_OPERATION);

		Element SOURCE_USERID = message.createElement("SOURCE_USERID");
		FCUBS_HEADER.appendChild(SOURCE_USERID);
		SOURCE_USERID.setTextContent(this.getUserID());

		Element DESTINATION = message.createElement("DESTINATION");
		FCUBS_HEADER.appendChild(DESTINATION);

		Element MULTITRIPID = message.createElement("MULTITRIPID");
		FCUBS_HEADER.appendChild(MULTITRIPID);

		Element FUNCTIONID = message.createElement("FUNCTIONID");
		FCUBS_HEADER.appendChild(FUNCTIONID);

		Element ACTION = message.createElement("ACTION");
		FCUBS_HEADER.appendChild(ACTION);

		Element MSGSTAT = message.createElement("MSGSTAT");
		FCUBS_HEADER.appendChild(MSGSTAT);

		Element FCUBS_BODY = message.createElement("FCUBS_BODY");
		CREATEFTCONTRACT_FSFS_REQ.appendChild(FCUBS_BODY);

		Element ContractDetails = message.createElement("Contract-Details");
		FCUBS_BODY.appendChild(ContractDetails);

		Element XREF = message.createElement("XREF");
		XREF.setTextContent(this.getTransflowTransactionId());//Randomly generated number
		ContractDetails.appendChild(XREF);

		Element PROD = message.createElement("PROD");
		PROD.setTextContent(appConstants.getProductCode());
		ContractDetails.appendChild(PROD);

		Element USRREF = message.createElement("USRREF");
		ContractDetails.appendChild(USRREF);

		Element DRCCY = message.createElement("DRCCY");
		DRCCY.setTextContent(this.getCurrency());
		ContractDetails.appendChild(DRCCY);

		Element CRCCY = message.createElement("CRCCY");
		CRCCY.setTextContent(this.getCurrency());
		ContractDetails.appendChild(CRCCY);

		Element DRAMT = message.createElement("DRAMT");
		DRAMT.setTextContent(this.getAmount().toPlainString());
		ContractDetails.appendChild(DRAMT);

		Element CRAMT = message.createElement("CRAMT");
		CRAMT.setTextContent(this.getAmount().toPlainString());
		ContractDetails.appendChild(CRAMT);

		Element DRACCBRN = message.createElement("DRACCBRN");
		DRACCBRN.setTextContent(this.getDdrBranch());
		ContractDetails.appendChild(DRACCBRN);

		Element CRACCBRN = message.createElement("CRACCBRN");
		CRACCBRN.setTextContent(this.getCcrBranch());
		ContractDetails.appendChild(CRACCBRN);

		Element DRACC = message.createElement("DRACC");
		DRACC.setTextContent(this.getDdrAccountNo());
		ContractDetails.appendChild(DRACC);

		Element CRACC = message.createElement("CRACC");
		CRACC.setTextContent(this.getCcrAccountNo());
		ContractDetails.appendChild(CRACC);
		
		Element REMARKS = message.createElement("REMARKS");
		REMARKS.setTextContent(this.getNarration());
		ContractDetails.appendChild(REMARKS);

		// transform the Document into a String
		DOMSource domSource = new DOMSource(message);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer =null;
		try {
			transformer = tf.newTransformer();
			//transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
			transformer.setOutputProperty
			("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

			java.io.StringWriter sw = new java.io.StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			 responseXml = sw.toString();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ServiceException(e.getMessage());
		}

		return responseXml;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}
}