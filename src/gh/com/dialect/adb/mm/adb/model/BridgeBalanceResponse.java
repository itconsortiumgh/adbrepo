package gh.com.dialect.adb.mm.adb.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class BridgeBalanceResponse {
	private String status_code;
	private String status_message;
	private GetBalanceResponse getBalanceResponse;
}
