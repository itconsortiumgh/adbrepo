package gh.com.dialect.adb.mm.adb.model;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class BalanceRequest {
	@Autowired
	AppConstants appConstants;
	
	private static final Log log = LogFactory.getLog(BalanceRequest.class);
	public static void main(String args[]) throws Exception{
		BalanceRequest request = new BalanceRequest();
		request.setUserID("MTNMOBUSR");
		request.setBranchCode("001");
		request.setCustAcctNo("1090000672624");
		String response = request.createBISOAPMessage();
		log.info(response);
	}
	
	private String branchCode;
	private String custAcctNo;
	private String UserID;
	private String bankID;
	private String beEndpoint;
	
	public String getBeEndpoint() {
		return beEndpoint;
	}

	public void setBeEndpoint(String beEndpoint) {
		this.beEndpoint = beEndpoint;
	}

	public String getBankID() {
		return bankID;
	}

	public void setBankID(String bankID) {
		this.bankID = bankID;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCustAcctNo() {
		return custAcctNo;
	}

	public void setCustAcctNo(String custAcctNo) {
		this.custAcctNo = custAcctNo;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public static Log getLog() {
		return log;
	}

	public String createBISOAPMessage() throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null,null,null);
		Element envelope = message.createElement("env:Envelope");
		envelope.setAttribute("xmlns:env", "http://schemas.xmlsoap.org/soap/envelope/");
		message.appendChild(envelope);

		Element header = message.createElement("env:Header");
		envelope.appendChild(header);

		Element body = message.createElement("env:Body");
		envelope.appendChild(body);

		Element QUERYACCBAL_IOFS_REQ = message.createElement("QUERYACCBAL_IOFS_REQ");
		QUERYACCBAL_IOFS_REQ.setAttribute("xmlns", "http://fcubs.ofss.com/service/FCUBSAccService");
		body.appendChild(QUERYACCBAL_IOFS_REQ);
		
		Element FCUBS_HEADER = message.createElement("FCUBS_HEADER");
		QUERYACCBAL_IOFS_REQ.appendChild(FCUBS_HEADER);
		
		Element SOURCE = message.createElement("SOURCE");
	    SOURCE.setTextContent("TRANSFLOW");
//	    SOURCE.setTextContent(appConstants.getFlexCubeSource());
	    FCUBS_HEADER.appendChild(SOURCE);
	    
	    Element UBSCOMP = message.createElement("UBSCOMP");
	    UBSCOMP.setTextContent("FCUBS");
	    FCUBS_HEADER.appendChild(UBSCOMP);
	    
	    Element MSGID = message.createElement("MSGID");
	    FCUBS_HEADER.appendChild(MSGID);
	    
	    Element CORRELID = message.createElement("CORRELID");
	    FCUBS_HEADER.appendChild(CORRELID);
	    
	    Element USERID = message.createElement("USERID");
//	    USERID.setTextContent("MTNMOBUSR");
	    USERID.setTextContent(this.getUserID());
	    FCUBS_HEADER.appendChild(USERID);
	    
	    Element BRANCH = message.createElement("BRANCH");
	    BRANCH.setTextContent("000");
	    FCUBS_HEADER.appendChild(BRANCH);
	    
	    Element MODULEID = message.createElement("MODULEID");
	    MODULEID.setTextContent("CO");
	    FCUBS_HEADER.appendChild(MODULEID);
	    
	    Element SERVICE = message.createElement("SERVICE");
	    SERVICE.setTextContent("FCUBSAccService");
	    FCUBS_HEADER.appendChild(SERVICE);
	    
	    Element OPERATION = message.createElement("OPERATION");
	    OPERATION.setTextContent("QueryAccBal");
	    FCUBS_HEADER.appendChild(OPERATION);
	    
	    Element SOURCE_OPERATION = message.createElement("SOURCE_OPERATION");
	    SOURCE_OPERATION.setTextContent("QueryAccBal");
	    FCUBS_HEADER.appendChild(SOURCE_OPERATION);
	    
	    Element SOURCE_USERID = message.createElement("SOURCE_USERID");
	    FCUBS_HEADER.appendChild(SOURCE_USERID);
	    
	    Element DESTINATION = message.createElement("DESTINATION");
	    FCUBS_HEADER.appendChild(DESTINATION);
	    
	    Element MULTITRIPID = message.createElement("MULTITRIPID");
	    FCUBS_HEADER.appendChild(MULTITRIPID);
	    
	    Element FUNCTIONID = message.createElement("FUNCTIONID");
	    FCUBS_HEADER.appendChild(FUNCTIONID);
	    
	    Element ACTION = message.createElement("ACTION");
//	    FCUBS_HEADER.appendChild(ACTION);
	    ACTION.setTextContent("EXECUTEQUERY");
	    FCUBS_HEADER.appendChild(ACTION);
	    
	    Element MSGSTAT = message.createElement("MSGSTAT");
	    MSGSTAT.setTextContent("SUCCESS");
	    FCUBS_HEADER.appendChild(MSGSTAT);
	    
	    Element FCUBS_BODY = message.createElement("FCUBS_BODY");
	    QUERYACCBAL_IOFS_REQ.appendChild(FCUBS_BODY);
	    
	    Element ACCBalance = message.createElement("ACC-Balance");
	    FCUBS_BODY.appendChild(ACCBalance);

	    Element ACC_BAL = message.createElement("ACC_BAL");
	    ACCBalance.appendChild(ACC_BAL);
	    
	    
	    Element BRANCH_CODE = message.createElement("BRANCH_CODE");
	    BRANCH_CODE.setTextContent(this.getBranchCode());//Randomly generated number
	    ACC_BAL.appendChild(BRANCH_CODE);
	    
	    Element CUST_AC_NO = message.createElement("CUST_AC_NO");
	    CUST_AC_NO.setTextContent(this.getCustAcctNo());
	    ACC_BAL.appendChild(CUST_AC_NO);
	    
		// transform the Document into a String
		DOMSource domSource = new DOMSource(message);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		//transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
		transformer.setOutputProperty
		("{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		java.io.StringWriter sw = new java.io.StringWriter();
		StreamResult sr = new StreamResult(sw);
		transformer.transform(domSource, sr);
		String responseXml = sw.toString();
		return responseXml;
		
	}
	public String toString(){
		String result = " bankId: "+this.bankID+ "\n branch code: "+this.branchCode+ "\n custAccNum: "+this.custAcctNo+ "\n UserId:"+ this.UserID;
		return result;
	}
}