package gh.com.dialect.adb.mm.adb.model;

public class DuplicateTransaction {

	public DuplicateTransaction() {
		// TODO Auto-generated constructor stub
	}
	boolean duplicate;
	boolean failedTransaction;
	public boolean isDuplicate() {
		return duplicate;
	}
	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}
	public boolean isFailedTransaction() {
		return failedTransaction;
	}
	public void setFailedTransaction(boolean failedTransaction) {
		this.failedTransaction = failedTransaction;
	}
	
}
