package gh.com.dialect.adb.mm.adb.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class BridgeBalanceRequest {
	private String terminalId;
	private String accountNo;
	private String currency;
	private String merchant;
}
