package gh.com.dialect.adb.mm.adb.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CopyOfTransferRequest {
	private static final Log log = LogFactory.getLog(CopyOfTransferRequest.class);
	public static void main(String args[]) throws Exception{
		CopyOfTransferRequest request = new CopyOfTransferRequest();
		request.setAmount(new BigDecimal(100));
		request.setDrAccount("1080000003638");
		request.setCrAccount("2030016530464");
		request.setDdrAcctBranch("001");
		request.setCcrAcctBranch("005");
		request.setxRef("14878229200019");
		request.setUserID("TRANSUSER");
		String response = request.createFTSOAPMessage();
		System.out.println(response);
	}
	
	private String drAccount;
	private String crAccount;
	private BigDecimal amount;
	private String currency;
	private String ddrAcctBranch;
	private String ccrAcctBranch;
	private String UserID;
	private String xRef;
	private String subscriberNumber;
	private String accountType;
	private String bankId;
	private String ftEndpoint;
	private String beEndpoint;
	private boolean activatedSubsciber;
	
	public boolean isActivatedSubsciber() {
		return activatedSubsciber;
	}
	public void setActivatedSubsciber(boolean activatedSubsciber) {
		this.activatedSubsciber = activatedSubsciber;
	}
	public String getFtEndpoint() {
		return ftEndpoint;
	}
	public void setFtEndpoint(String ftEndpoint) {
		this.ftEndpoint = ftEndpoint;
	}
	public String getBeEndpoint() {
		return beEndpoint;
	}
	public void setBeEndpoint(String beEndpoint) {
		this.beEndpoint = beEndpoint;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getSubscriberNumber() {
		return subscriberNumber;
	}
	public void setSubscriberNumber(String subscriberNumber) {
		this.subscriberNumber = subscriberNumber;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getxRef() {
		return xRef;
	}
	public void setxRef(String xRef) {
		this.xRef = xRef;
	}
	public String getDdrAcctBranch() {
		return ddrAcctBranch;
	}
	public void setDdrAcctBranch(String ddrAcctBranch) {
		this.ddrAcctBranch = ddrAcctBranch;
	}
	public String getCcrAcctBranch() {
		return ccrAcctBranch;
	}
	public void setCcrAcctBranch(String ccrAcctBranch) {
		this.ccrAcctBranch = ccrAcctBranch;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDrAccount() {
		return drAccount;
	}
	public void setDrAccount(String drAccount) {
		this.drAccount = drAccount;
	}
	public String getCrAccount() {
		return crAccount;
	}
	public void setCrAccount(String crAccount) {
		this.crAccount = crAccount;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String createFTSOAPMessage() throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		DOMImplementation impl = builder.getDOMImplementation();

		Document message = impl.createDocument(null,null,null);
		Element envelope = message.createElement("env:Envelope");
		envelope.setAttribute("xmlns:env", "http://schemas.xmlsoap.org/soap/envelope/");
		message.appendChild(envelope);

		Element header = message.createElement("env:Header");
		envelope.appendChild(header);

		Element body = message.createElement("env:Body");
		envelope.appendChild(body);

		Element CREATEFTCONTRACT_FSFS_REQ = message.createElement("CREATEFTCONTRACT_FSFS_REQ");
		CREATEFTCONTRACT_FSFS_REQ.setAttribute("xmlns", "http://fcubs.ofss.com/service/FCUBSFTService");
		body.appendChild(CREATEFTCONTRACT_FSFS_REQ);
		
		Element FCUBS_HEADER = message.createElement("FCUBS_HEADER");
		CREATEFTCONTRACT_FSFS_REQ.appendChild(FCUBS_HEADER);
		
		Element SOURCE = message.createElement("SOURCE");
	    SOURCE.setTextContent("TRANSFLOW");
	    FCUBS_HEADER.appendChild(SOURCE);
	    
	    Element UBSCOMP = message.createElement("UBSCOMP");
	    UBSCOMP.setTextContent("FCUBS");
	    FCUBS_HEADER.appendChild(UBSCOMP);
	    
	    Element MSGID = message.createElement("MSGID");
	    FCUBS_HEADER.appendChild(MSGID);
	    
	    Element CORRELID = message.createElement("CORRELID");
	    FCUBS_HEADER.appendChild(CORRELID);
	    
	    Element USERID = message.createElement("USERID");
//	    USERID.setTextContent("MTNMOBUSR");
	    USERID.setTextContent(this.getUserID());
	    FCUBS_HEADER.appendChild(USERID);
	    
	    Element BRANCH = message.createElement("BRANCH");
	    BRANCH.setTextContent("000");
	    FCUBS_HEADER.appendChild(BRANCH);
	    
	    Element MODULEID = message.createElement("MODULEID");
	    MODULEID.setTextContent("FT");
	    FCUBS_HEADER.appendChild(MODULEID);
	    
	    Element SERVICE = message.createElement("SERVICE");
	    SERVICE.setTextContent("FCUBSFTService");
	    FCUBS_HEADER.appendChild(SERVICE);
	    
	    Element OPERATION = message.createElement("OPERATION");
	    OPERATION.setTextContent("CreateFTContract");
	    FCUBS_HEADER.appendChild(OPERATION);
	    
	    Element SOURCE_OPERATION = message.createElement("SOURCE_OPERATION");
	    SOURCE_OPERATION.setTextContent("CreateFTContract");
	    FCUBS_HEADER.appendChild(SOURCE_OPERATION);
	    
	    Element SOURCE_USERID = message.createElement("SOURCE_USERID");
	    FCUBS_HEADER.appendChild(SOURCE_USERID);
	    SOURCE_USERID.setTextContent(this.getUserID());
	    
	    Element DESTINATION = message.createElement("DESTINATION");
	    FCUBS_HEADER.appendChild(DESTINATION);
	    
	    Element MULTITRIPID = message.createElement("MULTITRIPID");
	    FCUBS_HEADER.appendChild(MULTITRIPID);
	    
	    Element FUNCTIONID = message.createElement("FUNCTIONID");
	    FCUBS_HEADER.appendChild(FUNCTIONID);
	    
	    Element ACTION = message.createElement("ACTION");
	    FCUBS_HEADER.appendChild(ACTION);
	    
	    Element MSGSTAT = message.createElement("MSGSTAT");
	    MSGSTAT.setTextContent("SUCCESS");
	    FCUBS_HEADER.appendChild(MSGSTAT);
	    
	    Element FCUBS_BODY = message.createElement("FCUBS_BODY");
	    CREATEFTCONTRACT_FSFS_REQ.appendChild(FCUBS_BODY);
	    
	    Element ContractDetails = message.createElement("Contract-Details");
	    FCUBS_BODY.appendChild(ContractDetails);
	    
	    Element XREF = message.createElement("XREF");
	    XREF.setTextContent(this.getxRef());//Randomly generated number
	    ContractDetails.appendChild(XREF);
	    
	    Element PROD = message.createElement("PROD");
	    PROD.setTextContent("INTL");
	    ContractDetails.appendChild(PROD);
	    
	    Element USRREF = message.createElement("USRREF");
	    ContractDetails.appendChild(USRREF);
	    
	    Element DRCCY = message.createElement("DRCCY");
	    DRCCY.setTextContent("GHS");
	    ContractDetails.appendChild(DRCCY);
	    
	    Element CRCCY = message.createElement("CRCCY");
	    CRCCY.setTextContent("GHS");
	    ContractDetails.appendChild(CRCCY);
	    
	    Element DRAMT = message.createElement("DRAMT");
	    DRAMT.setTextContent(this.getAmount().toPlainString());
	    ContractDetails.appendChild(DRAMT);
	    
	    Element CRAMT = message.createElement("CRAMT");
	    CRAMT.setTextContent(this.getAmount().toPlainString());
	    ContractDetails.appendChild(CRAMT);
	    
	    
	    Element DRACCBRN = message.createElement("DRACCBRN");
	    DRACCBRN.setTextContent(this.getDdrAcctBranch());
	    ContractDetails.appendChild(DRACCBRN);
	    
	    Element CRACCBRN = message.createElement("CRACCBRN");
	    CRACCBRN.setTextContent(this.getCcrAcctBranch());
	    ContractDetails.appendChild(CRACCBRN);
	    
	    Element DRACC = message.createElement("DRACC");
	    DRACC.setTextContent(this.getDrAccount());
	    ContractDetails.appendChild(DRACC);
	    
	    Element CRACC = message.createElement("CRACC");
	    CRACC.setTextContent(this.getCrAccount());
	    ContractDetails.appendChild(CRACC);

//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//		String valueDate = formatter.format(new Date());
//
//	    Element DRVDT = message.createElement("DRVDT");
//	    DRVDT.setTextContent(valueDate);
//	    ContractDetails.appendChild(DRVDT);
//	    
//	    Element CRVDT = message.createElement("CRVDT");
//	    CRVDT.setTextContent(valueDate);
//	    ContractDetails.appendChild(CRVDT);
//	    
	    Element REMARKS = message.createElement("REMARKS");
	    ContractDetails.appendChild(REMARKS);
	    
	    Element AUTHSTAT = message.createElement("AUTHSTAT");
	    AUTHSTAT.setTextContent("A");
	    ContractDetails.appendChild(AUTHSTAT);
//	    
	    Element FXCNTREF = message.createElement("FXCNTREF");
	    ContractDetails.appendChild(FXCNTREF);

	    Element BYORDOF1 = message.createElement("BYORDOF1");
	    BYORDOF1.setTextContent("0000036");
	    ContractDetails.appendChild(BYORDOF1);
	    
		// transform the Document into a String
		DOMSource domSource = new DOMSource(message);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		//transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
		transformer.setOutputProperty
		("{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		java.io.StringWriter sw = new java.io.StringWriter();
		StreamResult sr = new StreamResult(sw);
		transformer.transform(domSource, sr);
		String responseXml = sw.toString();
		return responseXml;
	}
	
	public String toString(){
		String result = " accountType: "+this.accountType +"\n amount: "+this.amount+"\n bankId: " + this.bankId+"\n" +
				" credit branch: "+this.ccrAcctBranch +"\n creditAccount" + this.crAccount + "\n currency: "+this.currency +"" +
						"\n debitBrankch: "+this.ddrAcctBranch +"\n debit account: "+this.drAccount +"" + "\n userId"+this.UserID;
				
		return result;
	}
	
}