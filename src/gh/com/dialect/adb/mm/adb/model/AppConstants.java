package gh.com.dialect.adb.mm.adb.model;

public class AppConstants {
	public static final String RESPONSECODE_OK = "01";
	public static final String RESPONSECODE_ERROR = "100";
	public static final String RESPONSECODE_INSUFFICIENT_FUNDS  = "101";
	public static final String STR_INSUFFICIENT_FUNDS = "You have insufficient funds to perform the operation";
	public static final String LOG_STATUS_SUCCESS="success";
	public static final String LOG_STATUS_FAILURE="failure";
	public static final String LOG_STATUS_PENDING="pending";
//	public static final String LOG_STATUS_INITIATED="initiated";
	public static final String ACTION_BANK2PHONE="bank2phone";
	public static final String ACTION_PHONE2BANK="phone2bank";
	public static final String ACTION_REVERSAL="reversal";
	public static final String ACTION_BALENQ="balenq";
	public static final String LOG_NOT_APPLICABLE="na";
	public static final String EXTERNAL_TRANSACTION_FAILURE="External Transaction Failure";
	public static final String STR_DAILY_AMT_LIMIT="You have exceeded your daily amount limit";
	public static final String STR_DAILY_FREQ_LIMIT="you have exceeded your maximum number of transactions for the day";
	public static final String STR_INACTIVE_SUBSCRIBER="subscriber's status is not active for this service";
	public static final String CODE_09_PENDING_TXN="09";
	public static final String SUCCESS_CODE = "0000";
	
	private String testFlag;
	private String ftEndpoint;
	private String beEndpoint;
	private String productCode;
	private String flexCubeSource;
	
	private String bridgeBalEndpoint;
	private String bridgeFTEndpoint;
	public String getBridgeBalEndpoint() {
		return bridgeBalEndpoint;
	}

	public void setBridgeBalEndpoint(String bridgeBalEndpoint) {
		this.bridgeBalEndpoint = bridgeBalEndpoint;
	}

	public String getBridgeFTEndpoint() {
		return bridgeFTEndpoint;
	}

	public void setBridgeFTEndpoint(String bridgeFTEndpoint) {
		this.bridgeFTEndpoint = bridgeFTEndpoint;
	}

	private String currency;
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTestFlag() {
		return testFlag;
	}

	public void setTestFlag(String testFlag) {
		this.testFlag = testFlag;
	}

	public String getFtEndpoint() {
		return ftEndpoint;
	}

	public void setFtEndpoint(String ftEndpoint) {
		this.ftEndpoint = ftEndpoint;
	}

	public String getBeEndpoint() {
		return beEndpoint;
	}

	public void setBeEndpoint(String beEndpoint) {
		this.beEndpoint = beEndpoint;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getFlexCubeSource() {
		return flexCubeSource;
	}

	public void setFlexCubeSource(String flexCubeSource) {
		this.flexCubeSource = flexCubeSource;
	}
}
