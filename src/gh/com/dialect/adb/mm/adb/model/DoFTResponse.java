package gh.com.dialect.adb.mm.adb.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class DoFTResponse {
	private String xref;
	private String trx_id;
	private String bank_trxid;
}
