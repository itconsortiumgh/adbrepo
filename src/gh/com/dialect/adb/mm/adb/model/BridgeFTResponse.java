package gh.com.dialect.adb.mm.adb.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class BridgeFTResponse {
	private String responseCode;
	private String responseMessage;
	private DoFTResponse doFTResponse;
}
