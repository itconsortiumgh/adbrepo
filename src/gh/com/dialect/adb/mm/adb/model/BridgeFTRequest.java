package gh.com.dialect.adb.mm.adb.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class BridgeFTRequest {
	private String terminalId;
	private String ddrAccountNo;
	private String currency;
	private String ccrAccountNo;
	private String transflowTransactionId;
	private String narration;
	private String amount;
}
