package gh.com.dialect.adb.mm.adb.webserviceUtils;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.axiom.om.OMElement;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class SOAPMessageReader {
	public String findSOAPField(Document doc, String elementName)throws Exception{
		String response = "";
		try{
		  
		  NodeList list = doc.getElementsByTagName(elementName);
		  if(list.getLength() > 0){
			  Node node = list.item(0);
			  response = node.getTextContent();
		  }else{
			  
		  }
		}catch(Exception e){
			
		}finally{
//			in.close();
		}
		return response;
	}
	
	public String findSOAPField(OMElement element, String elementName) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(element.toString())));    
		TransformerFactory transformerFactory = TransformerFactory.newInstance(); 
		Transformer transformer = transformerFactory.newTransformer();
		String response = "";
		try{
		  
		  NodeList list = doc.getElementsByTagName(elementName);
		  if(list.getLength() > 0){
			  Node node = list.item(0);
			  response = node.getTextContent();
		  }else{
			  
		  }
		}catch(Exception e){
			
		}finally{
//			in.close();
		}
		return response;	
	}
	
	public String findSOAPField(String XMLString, String elementName) throws Exception{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(XMLString)));    
		TransformerFactory transformerFactory = TransformerFactory.newInstance(); 
		Transformer transformer = transformerFactory.newTransformer();
		String response = "";
		try{
		  
		  NodeList list = doc.getElementsByTagName(elementName);
		  if(list.getLength() > 0){
			  Node node = list.item(0);
			  response = node.getTextContent();
		  }else{
			  
		  }
		}catch(Exception e){
			
		}finally{
//			in.close();
		}
		return response;	
	}
}
