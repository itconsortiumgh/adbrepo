package gh.com.dialect.adb.mm.adb.webserviceUtils;
/** 
 * SOAPClient4XG. Read the SOAP envelope file passed as the second

 * parameter, pass it to the SOAP endpoint passed as the first parameter, and
 * print out the SOAP envelope passed as a response.  with help from Michael
 * Brennan 03/09/01
 * 
 *
 * @author  Bob DuCharme
 * @version 1.1
 * @param   SOAPUrl      URL of SOAP Endpoint to send request.
 * @param   xmlFile2Send A file with an XML document of the request.  
 *
 * 5/23/01 revision: SOAPAction added
 */

import gh.com.dialect.adb.exceptions.WebserviceException;
import gh.com.dialect.adb.mm.adb.model.TransferRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



public class SOAPClientUtil{
	private static final Log log = LogFactory.getLog(SOAPClientUtil.class);
	public static void main(String[] args) throws Exception {
		SOAPClientUtil client = new SOAPClientUtil();
		TransferRequest request = new TransferRequest();
		request.setAmount(new BigDecimal(100));
		request.setDdrAccountNo("1080000003638");
		request.setCcrAccountNo("2030016530464");
		request.setDdrBranch("001");
		request.setCcrBranch("005");
		request.setTransflowTransactionId("14878229200018");
		request.setUserID("MTNMOBUSR");
		
//		String message = client.createFTMessage(request);
//		String message = client.createFTMessage("1080000003638", "2030016530464", "50", "MTNMOBUSR", "001", "005", "14878229200016");
//		Date d = GregorianCalendar.getInstance().getTime();

		//    	String xmlStr = ResponseUtils.makeDepositToXML("TRS", "233542057875", "1234123423", "100", "kofieleblu", "en", "gh", "1.0");
		    	SOAPClientUtil c = new SOAPClientUtil();

		//    	String response = c.sendMessage("http://196.201.33.90:90/services/FundamoDeposit?wsdl", xmlStr);
		//    	String response = c.sendMessage("http://susumanager.com:8080/airtel_transflow/services/AirtelPayment?wsdl", xmlStr);
//		    	String response = c.sendMessage("http://10.179.100.3:9001/FCUBSFTService/FCUBSFTService?WSDL", message);
//		    	log.info("-------------" + response);
		//    	System.out.println(response);
	}
	
	public String sendMessage(String endpointURL, String soapMessage) throws WebserviceException{
		String result = "";
		BufferedReader in = null;
		OutputStream out = null;
		//        // Create the connection where we're going to send the file.
		URL url =null;
		try {
			url = new URL(endpointURL);

			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) connection;

			// Open the input file. After we copy it to a byte array, we can see
			// how big it is so that we can set the HTTP Cotent-Length
			// property. (See complete e-mail below for more on this.)


			byte[] b = soapMessage.getBytes();
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty( "Content-Length",
					String.valueOf( b.length ) );
			httpConn.setRequestProperty("Content-Type","text/xml; charset=utf-8");
			httpConn.setRequestProperty("SOAPAction","");
			httpConn.setRequestMethod( "POST" );
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML that was read in to b.
			out = httpConn.getOutputStream();
			out.write(b);    
			out.flush();

			// Read the response and write it to standard out.

			InputStream _is = null;
			if (httpConn.getResponseCode() >= 400) {
				_is = httpConn.getErrorStream(); 
			} else { 
				/* error from server */ 
				_is = httpConn.getInputStream(); 
			} 
			InputStreamReader isr = new InputStreamReader(_is);
			in = new BufferedReader(isr);
			String inputLine;

			while ((inputLine = in.readLine()) != null)
				result = result.concat(inputLine);
			//            System.out.println(inputLine);
			//        return result;
		}catch(MalformedURLException e){
			log.error("malformedURL exception");
			e.printStackTrace();
			throw new WebserviceException(e.getMessage());
		} catch (IOException e) {
			log.error("IOException");
			e.printStackTrace();
			throw new WebserviceException(e.getMessage());
		}catch(Exception e) {
			throw new WebserviceException(e.getMessage());
		}finally{
			try {
				out.close();
				in.close();

			} catch (IOException e) {
				log.error("IOException inside finally clause");
				e.printStackTrace();
			}
		}
		return result;
	}

	// copy method from From E.R. Harold's book "Java I/O"
	private static void copy(InputStream in, OutputStream out) 
	throws IOException {

		// do not allow other threads to read from the
		// input or write to the output while copying is
		// taking place

		synchronized (in) {
			synchronized (out) {

				byte[] buffer = new byte[256];
				while (true) {
					int bytesRead = in.read(buffer);
					if (bytesRead == -1) break;
					out.write(buffer, 0, bytesRead);
				}
			}
		}
	} 



}
