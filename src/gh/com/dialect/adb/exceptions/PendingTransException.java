package gh.com.dialect.adb.exceptions;

public class PendingTransException extends Exception {

	public PendingTransException() {
		// TODO Auto-generated constructor stub
	}

	public PendingTransException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PendingTransException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PendingTransException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
