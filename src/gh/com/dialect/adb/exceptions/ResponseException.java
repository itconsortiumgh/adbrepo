package gh.com.dialect.adb.exceptions;

public class ResponseException extends Exception {

	public ResponseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ResponseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ResponseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ResponseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
