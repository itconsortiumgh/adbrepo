package gh.com.dialect.adb.cb.dao;

import java.math.BigDecimal;

import gh.com.dialect.adb.exceptions.PendingTransException;
import gh.com.dialect.adb.exceptions.ServiceException;
import gh.com.dialect.adb.mm.adb.model.AppConstants;
import gh.com.dialect.adb.mm.adb.model.DuplicateTransaction;
import gh.com.dialect.adb.mm.adb.model.TransferRequest;
import gh.com.dialect.transflow.api.thirdparty.response.ResponseCode;
import gh.com.dialect.transflow.api.thirdparty.response.TransactionResponse;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class GeneralDAO {

	private JdbcTemplate jdbcTemplate;
	static Logger log = Logger.getLogger(GeneralDAO.class);	
	public GeneralDAO(DataSource datasource) {
		super();
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}

	public static void main(String[]args) throws Exception{
		//		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("file:///Users/franklineleblu/Documents/workspace/corebank_facade_ADB/WebContent/WEB-INF/spring/applicationContext.xml");
		//		GeneralDAO dao = (GeneralDAO)context.getBean("generalDAO");
		//		log.info(dao.findTransferRequestByTransflowTransId("140228000002C"));
		log.info("hello");
	}

	public void logFTTransaction(TransferRequest request, TransactionResponse response, String transStatus) {
		String query = "insert into ft_log (ddrBranch,ddrAccountNo,ccrBranch,ccrAccountNo,amount,currency,\n" + 
				"transflowTransactionId,bankTransactionId,narration,transStatus, responseCode, responseMessage)VALUES\n" + 
				"(?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(query,
					new Object[] {request.getDdrBranch(), request.getDdrAccountNo(), request.getCcrBranch(), request.getCcrAccountNo(),
					request.getAmount(), request.getCurrency(), request.getTransflowTransactionId(), response.getBankTransactionId(),
					request.getNarration(),transStatus, response.getResponseCode(), response.getResponseMessage()});
		}catch(Exception e){
			log.error("Logging FT failed");
			e.printStackTrace();
		} 
	}
	public void logUpdateTransControlTable(TransferRequest request, TransactionResponse response, String transStatus) {
		String query = "update transaction_control_log set bankTransactionId=?, transStatus=?, responseCode=?, responseMessage=? where "
				+ "transflowTransactionId=?"; 
		try {
			jdbcTemplate.update(query,
					new Object[] {response.getBankTransactionId(),transStatus, response.getResponseCode(), response.getResponseMessage(), request.getTransflowTransactionId()});
		}catch(Exception e){
			log.error("Logging to TransactionControl failed");
			e.printStackTrace();
		} 
	}

	public void logInitialToTransControlTable(TransferRequest request) {
		String query = "insert into transaction_control_log (ddrBranch,ddrAccountNo,ccrBranch,ccrAccountNo,amount,currency,\n" + 
				"transflowTransactionId,narration,transStatus)VALUES\n" + 
				"(?,?,?,?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(query,
					new Object[] {request.getDdrBranch(), request.getDdrAccountNo(), request.getCcrBranch(), request.getCcrAccountNo(),
					request.getAmount(), request.getCurrency(), request.getTransflowTransactionId(), 
					request.getNarration(),AppConstants.LOG_STATUS_PENDING});
		}catch(Exception e){
			log.error("Logging 'Pending' To TransactionControl failed");
			e.printStackTrace();
		}
	}

	public void reInitializeToTransControlTable(TransferRequest request) {
		String query = "insert into transaction_control_log (ddrBranch,ddrAccountNo,ccrBranch,ccrAccountNo,amount,currency,\n" + 
				"transflowTransactionId,narration,transStatus)VALUES\n" + 
				"(?,?,?,?,?,?,?,?,?) on duplicate key update transStatus=?";
		try {
			jdbcTemplate.update(query,
					new Object[] {request.getDdrBranch(), request.getDdrAccountNo(), request.getCcrBranch(), request.getCcrAccountNo(),
					request.getAmount(), request.getCurrency(), request.getTransflowTransactionId(), 
					request.getNarration(),AppConstants.LOG_STATUS_PENDING, AppConstants.LOG_STATUS_PENDING});
		}catch(Exception e){
			log.error("Logging 'Pending' To TransactionControl failed");
			e.printStackTrace();
		}
	}

	public void logFReversalTransaction(TransferRequest request,String oldTransflowTransactionId, TransactionResponse response, String transStatus) {
		String query = "insert into reversal_log (ddrBranch,ddrAccountNo,ccrBranch,ccrAccountNo,amount,currency,\n" + 
				"newTransflowTransactionId,bankTransactionId,narration,transStatus,oldTransflowTransactionId,responseCode, responseMessage)VALUES\n" + 
				"(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			jdbcTemplate.update(query,
					new Object[] {request.getDdrBranch(), request.getDdrAccountNo(), request.getCcrBranch(), request.getCcrAccountNo(),
					request.getAmount(), request.getCurrency(), request.getTransflowTransactionId(), response.getBankTransactionId(),
					request.getNarration(),transStatus, oldTransflowTransactionId,response.getResponseCode(), response.getResponseMessage()});
		}catch(Exception e){
			log.error("Logging reversal failed"+ request);
			e.printStackTrace();
		} 
	}

	public TransferRequest findTransferRequestByTransflowTransId(String transflowTransactionId) throws ServiceException {
		TransferRequest request = null;
		String query = "select * from transaction_control_log where transflowTransactionId =? and transStatus=? "; 
		try {
			request = (TransferRequest) jdbcTemplate.queryForObject(query,
					new Object[] {transflowTransactionId, AppConstants.LOG_STATUS_SUCCESS},
					new BeanPropertyRowMapper<TransferRequest>(TransferRequest.class));
		}catch(EmptyResultDataAccessException e){
			throw new ServiceException("No Record found for transflowTransactionId " + transflowTransactionId);
		}
		log.info("---inside DAO, request found by transflowTransactionId " + request);
		return request;
	}

	public DuplicateTransaction isDuplicateTransaction(String transflowTransactionId) throws PendingTransException{
		log.info("=========================================================================================");
		log.info("----------------------going to check whether transaction is duplicated" + transflowTransactionId);
		log.info("=========================================================================================");
		boolean duplicate = false;
		DuplicateTransaction dc = new DuplicateTransaction();
		String query = "select * from transaction_control_log where  (transStatus=? OR transStatus=?) AND transflowTransactionId =? "; 
		try {
			TransferRequest request=null;
			request = (TransferRequest) jdbcTemplate.queryForObject(query,
					new Object[] {AppConstants.LOG_STATUS_PENDING, AppConstants.LOG_STATUS_SUCCESS, transflowTransactionId},
					new BeanPropertyRowMapper<TransferRequest>(TransferRequest.class));
			try{
				if(request.getTransflowTransactionId().equalsIgnoreCase(transflowTransactionId)){
					if(StringUtils.equalsIgnoreCase(request.getTransStatus(), AppConstants.LOG_STATUS_PENDING)){
						log.info("transaction with id "+transflowTransactionId+" pending");
						throw new PendingTransException("Transaction Already Pending");
					}else if(StringUtils.equalsIgnoreCase(request.getTransStatus(), AppConstants.LOG_STATUS_SUCCESS)){
						log.info("------duplicate exists for "+transflowTransactionId);
						duplicate = true;
					}else{
						log.info("eadasd");
					}
				}
				if(AppConstants.LOG_STATUS_FAILURE.equalsIgnoreCase(request.getTransStatus())){
					log.info("-----------------we have a failed transaction:----------------");
					log.info(request);
					dc.setFailedTransaction(true);
				}else{
					log.info("-----------------there is no failed transaction:----------------");
					log.info(request);
					dc.setFailedTransaction(false);
				}
			}catch(NullPointerException e){
				log.error("--------Nullpointer so no duplicate exists");
				duplicate = false; 
			}
		}catch(EmptyResultDataAccessException e){
			log.error("---------- No duplicate for "+transflowTransactionId+" because  " + e.getMessage());
			duplicate = false;
			String query2 = "select transStatus from transaction_control_log where transflowTransactionId =? ";
			try{
				String transStatus = (String)jdbcTemplate.queryForObject(query2,new Object[] {transflowTransactionId},  String.class);
				if(AppConstants.LOG_STATUS_FAILURE.equalsIgnoreCase(transStatus)){
					log.info("-----------------we have a failed transaction:----------------");
					log.info(transStatus);
					dc.setFailedTransaction(true);
				}else{
					log.info("-----------------there is no failed transaction:----------------");
					log.info(transStatus);
					dc.setFailedTransaction(false);
				}
			}catch(EmptyResultDataAccessException e2){
				dc.setFailedTransaction(false);
			}
		}
		log.info("-----about to return "+duplicate);
		log.info("=========================================================================================");
		log.info("=========================================================================================");
		dc.setDuplicate(duplicate);
		return dc;

	}
	//	public Boolean isDuplicateTransaction(String transflowTransactionId) throws PendingTransException{
	//		log.info("=========================================================================================");
	//		log.info("----------------------going to check whether transaction is duplicated" + transflowTransactionId);
	//		log.info("=========================================================================================");
	//		boolean duplicate = false;
	//		String query = "select * from transaction_control_log where  (transStatus=? OR transStatus=?) AND transflowTransactionId =? "; 
	//		try {
	//			TransferRequest request=null;
	//			request = (TransferRequest) jdbcTemplate.queryForObject(query,
	//					new Object[] {AppConstants.LOG_STATUS_PENDING, AppConstants.LOG_STATUS_SUCCESS, transflowTransactionId},
	//					new BeanPropertyRowMapper<TransferRequest>(TransferRequest.class));
	//			try{
	//				if(request.getTransflowTransactionId().equalsIgnoreCase(transflowTransactionId)){
	//					if(StringUtils.equalsIgnoreCase(request.getTransStatus(), AppConstants.LOG_STATUS_PENDING)){
	//						log.info("transaction with id "+transflowTransactionId+" pending");
	//						throw new PendingTransException("Transaction Already Pending");
	//					}else if(StringUtils.equalsIgnoreCase(request.getTransStatus(), AppConstants.LOG_STATUS_SUCCESS)){
	//						log.info("------duplicate exists for "+transflowTransactionId);
	//						duplicate = true;	
	//					}else{
	//						log.info("eadasd"  );
	//					}
	//				}
	//			}catch(NullPointerException e){
	//				log.error("--------Nullpointer so no duplicate exists");
	//				duplicate = false; 
	//			}
	//		}catch(EmptyResultDataAccessException e){
	//			log.error("---------- No duplicate for "+transflowTransactionId+" because  " + e.getMessage());
	//			return false;
	//		}
	//		log.info("-----about to return "+duplicate);
	//		log.info("=========================================================================================");
	//		log.info("=========================================================================================");
	//		return duplicate;
	//	}

	public void logBalanceEnquiry(String branchCode, String accountNo, BigDecimal availBalance, String currency) throws ServiceException {
		String query = "insert into balenq_log(branchCode, accountNo, availBalance, currency) values(?,?,?,?)";
		try {
			jdbcTemplate.update(query, new Object[] {branchCode, accountNo, availBalance, currency});
		}catch(Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ServiceException(e.getMessage());
		}
	}
}