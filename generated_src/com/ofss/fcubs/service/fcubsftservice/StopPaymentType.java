
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for StopPaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StopPaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PMTCHKNOTRCVD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DRAUTH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RFNDCVR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHKNOTDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHKLOST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCDET" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STMTNO" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="STMTDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NRTV1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NRTV2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NRTV3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NRTV4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NRTV5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NRTV6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopPaymentType", propOrder = {
    "pmtchknotrcvd",
    "drauth",
    "rfndcvr",
    "chknotdr",
    "chklost",
    "accdet",
    "stmtno",
    "stmtdt",
    "nrtv1",
    "nrtv2",
    "nrtv3",
    "nrtv4",
    "nrtv5",
    "nrtv6"
})
public class StopPaymentType {

    @XmlElement(name = "PMTCHKNOTRCVD")
    protected String pmtchknotrcvd;
    @XmlElement(name = "DRAUTH")
    protected String drauth;
    @XmlElement(name = "RFNDCVR")
    protected String rfndcvr;
    @XmlElement(name = "CHKNOTDR")
    protected String chknotdr;
    @XmlElement(name = "CHKLOST")
    protected String chklost;
    @XmlElement(name = "ACCDET")
    protected String accdet;
    @XmlElement(name = "STMTNO")
    protected BigInteger stmtno;
    @XmlElement(name = "STMTDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stmtdt;
    @XmlElement(name = "NRTV1")
    protected String nrtv1;
    @XmlElement(name = "NRTV2")
    protected String nrtv2;
    @XmlElement(name = "NRTV3")
    protected String nrtv3;
    @XmlElement(name = "NRTV4")
    protected String nrtv4;
    @XmlElement(name = "NRTV5")
    protected String nrtv5;
    @XmlElement(name = "NRTV6")
    protected String nrtv6;

    /**
     * Gets the value of the pmtchknotrcvd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTCHKNOTRCVD() {
        return pmtchknotrcvd;
    }

    /**
     * Sets the value of the pmtchknotrcvd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTCHKNOTRCVD(String value) {
        this.pmtchknotrcvd = value;
    }

    /**
     * Gets the value of the drauth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRAUTH() {
        return drauth;
    }

    /**
     * Sets the value of the drauth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRAUTH(String value) {
        this.drauth = value;
    }

    /**
     * Gets the value of the rfndcvr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRFNDCVR() {
        return rfndcvr;
    }

    /**
     * Sets the value of the rfndcvr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRFNDCVR(String value) {
        this.rfndcvr = value;
    }

    /**
     * Gets the value of the chknotdr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHKNOTDR() {
        return chknotdr;
    }

    /**
     * Sets the value of the chknotdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHKNOTDR(String value) {
        this.chknotdr = value;
    }

    /**
     * Gets the value of the chklost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHKLOST() {
        return chklost;
    }

    /**
     * Sets the value of the chklost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHKLOST(String value) {
        this.chklost = value;
    }

    /**
     * Gets the value of the accdet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCDET() {
        return accdet;
    }

    /**
     * Sets the value of the accdet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCDET(String value) {
        this.accdet = value;
    }

    /**
     * Gets the value of the stmtno property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSTMTNO() {
        return stmtno;
    }

    /**
     * Sets the value of the stmtno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSTMTNO(BigInteger value) {
        this.stmtno = value;
    }

    /**
     * Gets the value of the stmtdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSTMTDT() {
        return stmtdt;
    }

    /**
     * Sets the value of the stmtdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSTMTDT(XMLGregorianCalendar value) {
        this.stmtdt = value;
    }

    /**
     * Gets the value of the nrtv1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV1() {
        return nrtv1;
    }

    /**
     * Sets the value of the nrtv1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV1(String value) {
        this.nrtv1 = value;
    }

    /**
     * Gets the value of the nrtv2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV2() {
        return nrtv2;
    }

    /**
     * Sets the value of the nrtv2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV2(String value) {
        this.nrtv2 = value;
    }

    /**
     * Gets the value of the nrtv3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV3() {
        return nrtv3;
    }

    /**
     * Sets the value of the nrtv3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV3(String value) {
        this.nrtv3 = value;
    }

    /**
     * Gets the value of the nrtv4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV4() {
        return nrtv4;
    }

    /**
     * Sets the value of the nrtv4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV4(String value) {
        this.nrtv4 = value;
    }

    /**
     * Gets the value of the nrtv5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV5() {
        return nrtv5;
    }

    /**
     * Sets the value of the nrtv5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV5(String value) {
        this.nrtv5 = value;
    }

    /**
     * Gets the value of the nrtv6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRTV6() {
        return nrtv6;
    }

    /**
     * Sets the value of the nrtv6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRTV6(String value) {
        this.nrtv6 = value;
    }

}
