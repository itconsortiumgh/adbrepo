
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractOverrides complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractOverrides">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCCREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ESN" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="OVDSN" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="CONFIRMED" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHBY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHDTSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TXTSTATUS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OVDTXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractOverrides", propOrder = {
    "fccref",
    "esn",
    "ovdsn",
    "confirmed",
    "authby",
    "authdtstamp",
    "txtstatus",
    "ovdtxt"
})
public class ContractOverrides {

    @XmlElement(name = "FCCREF")
    protected String fccref;
    @XmlElement(name = "ESN")
    protected BigInteger esn;
    @XmlElement(name = "OVDSN")
    protected BigInteger ovdsn;
    @XmlElement(name = "CONFIRMED")
    protected String confirmed;
    @XmlElement(name = "AUTHBY")
    protected String authby;
    @XmlElement(name = "AUTHDTSTAMP")
    protected String authdtstamp;
    @XmlElement(name = "TXTSTATUS")
    protected String txtstatus;
    @XmlElement(name = "OVDTXT")
    protected String ovdtxt;

    /**
     * Gets the value of the fccref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFCCREF() {
        return fccref;
    }

    /**
     * Sets the value of the fccref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFCCREF(String value) {
        this.fccref = value;
    }

    /**
     * Gets the value of the esn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getESN() {
        return esn;
    }

    /**
     * Sets the value of the esn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setESN(BigInteger value) {
        this.esn = value;
    }

    /**
     * Gets the value of the ovdsn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOVDSN() {
        return ovdsn;
    }

    /**
     * Sets the value of the ovdsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOVDSN(BigInteger value) {
        this.ovdsn = value;
    }

    /**
     * Gets the value of the confirmed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONFIRMED() {
        return confirmed;
    }

    /**
     * Sets the value of the confirmed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONFIRMED(String value) {
        this.confirmed = value;
    }

    /**
     * Gets the value of the authby property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHBY() {
        return authby;
    }

    /**
     * Sets the value of the authby property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHBY(String value) {
        this.authby = value;
    }

    /**
     * Gets the value of the authdtstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHDTSTAMP() {
        return authdtstamp;
    }

    /**
     * Sets the value of the authdtstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHDTSTAMP(String value) {
        this.authdtstamp = value;
    }

    /**
     * Gets the value of the txtstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTXTSTATUS() {
        return txtstatus;
    }

    /**
     * Sets the value of the txtstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTXTSTATUS(String value) {
        this.txtstatus = value;
    }

    /**
     * Gets the value of the ovdtxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOVDTXT() {
        return ovdtxt;
    }

    /**
     * Sets the value of the ovdtxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOVDTXT(String value) {
        this.ovdtxt = value;
    }

}
