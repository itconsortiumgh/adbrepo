
package com.ofss.fcubs.service.fcubsftservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBS_HEADER" type="{http://fcubs.ofss.com/service/FCUBSFTService}FCUBS_HEADERType"/>
 *         &lt;element name="FCUBS_BODY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="MSTMS-MT101-MASTER-IO" type="{http://fcubs.ofss.com/service/FCUBSFTService}Ftdmt101-PK-ACR-Type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsheader",
    "fcubsbody"
})
@XmlRootElement(name = "AUTHORIZEMT101_IOPK_REQ")
public class AUTHORIZEMT101IOPKREQ {

    @XmlElement(name = "FCUBS_HEADER", required = true)
    protected FCUBSHEADERType fcubsheader;
    @XmlElement(name = "FCUBS_BODY", required = true)
    protected AUTHORIZEMT101IOPKREQ.FCUBSBODY fcubsbody;

    /**
     * Gets the value of the fcubsheader property.
     * 
     * @return
     *     possible object is
     *     {@link FCUBSHEADERType }
     *     
     */
    public FCUBSHEADERType getFCUBSHEADER() {
        return fcubsheader;
    }

    /**
     * Sets the value of the fcubsheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link FCUBSHEADERType }
     *     
     */
    public void setFCUBSHEADER(FCUBSHEADERType value) {
        this.fcubsheader = value;
    }

    /**
     * Gets the value of the fcubsbody property.
     * 
     * @return
     *     possible object is
     *     {@link AUTHORIZEMT101IOPKREQ.FCUBSBODY }
     *     
     */
    public AUTHORIZEMT101IOPKREQ.FCUBSBODY getFCUBSBODY() {
        return fcubsbody;
    }

    /**
     * Sets the value of the fcubsbody property.
     * 
     * @param value
     *     allowed object is
     *     {@link AUTHORIZEMT101IOPKREQ.FCUBSBODY }
     *     
     */
    public void setFCUBSBODY(AUTHORIZEMT101IOPKREQ.FCUBSBODY value) {
        this.fcubsbody = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="MSTMS-MT101-MASTER-IO" type="{http://fcubs.ofss.com/service/FCUBSFTService}Ftdmt101-PK-ACR-Type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mstmsmt101MASTERIO"
    })
    public static class FCUBSBODY {

        @XmlElement(name = "MSTMS-MT101-MASTER-IO", required = true)
        protected Ftdmt101PKACRType mstmsmt101MASTERIO;

        /**
         * Gets the value of the mstmsmt101MASTERIO property.
         * 
         * @return
         *     possible object is
         *     {@link Ftdmt101PKACRType }
         *     
         */
        public Ftdmt101PKACRType getMSTMSMT101MASTERIO() {
            return mstmsmt101MASTERIO;
        }

        /**
         * Sets the value of the mstmsmt101MASTERIO property.
         * 
         * @param value
         *     allowed object is
         *     {@link Ftdmt101PKACRType }
         *     
         */
        public void setMSTMSMT101MASTERIO(Ftdmt101PKACRType value) {
            this.mstmsmt101MASTERIO = value;
        }

    }

}
