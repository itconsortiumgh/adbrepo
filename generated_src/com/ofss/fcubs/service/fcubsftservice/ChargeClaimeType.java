
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargeClaimeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargeClaimeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CHGREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CHGDET1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGDET2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGDET3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGDET4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGDET5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGDET6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRRCVRINFO6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargeClaimeType", propOrder = {
    "chgref",
    "chgccy",
    "chgamt",
    "chgdet1",
    "chgdet2",
    "chgdet3",
    "chgdet4",
    "chgdet5",
    "chgdet6",
    "sndrrcvrinfo1",
    "sndrrcvrinfo2",
    "sndrrcvrinfo3",
    "sndrrcvrinfo4",
    "sndrrcvrinfo5",
    "sndrrcvrinfo6",
    "acwthinst1",
    "acwthinst2",
    "acwthinst3",
    "acwthinst4",
    "acwthinst5",
    "ordinst1",
    "ordinst2",
    "ordinst3",
    "ordinst4",
    "ordinst5"
})
public class ChargeClaimeType {

    @XmlElement(name = "CHGREF")
    protected String chgref;
    @XmlElement(name = "CHGCCY")
    protected String chgccy;
    @XmlElement(name = "CHGAMT")
    protected BigDecimal chgamt;
    @XmlElement(name = "CHGDET1")
    protected String chgdet1;
    @XmlElement(name = "CHGDET2")
    protected String chgdet2;
    @XmlElement(name = "CHGDET3")
    protected String chgdet3;
    @XmlElement(name = "CHGDET4")
    protected String chgdet4;
    @XmlElement(name = "CHGDET5")
    protected String chgdet5;
    @XmlElement(name = "CHGDET6")
    protected String chgdet6;
    @XmlElement(name = "SNDRRCVRINFO1")
    protected String sndrrcvrinfo1;
    @XmlElement(name = "SNDRRCVRINFO2")
    protected String sndrrcvrinfo2;
    @XmlElement(name = "SNDRRCVRINFO3")
    protected String sndrrcvrinfo3;
    @XmlElement(name = "SNDRRCVRINFO4")
    protected String sndrrcvrinfo4;
    @XmlElement(name = "SNDRRCVRINFO5")
    protected String sndrrcvrinfo5;
    @XmlElement(name = "SNDRRCVRINFO6")
    protected String sndrrcvrinfo6;
    @XmlElement(name = "ACWTHINST1")
    protected String acwthinst1;
    @XmlElement(name = "ACWTHINST2")
    protected String acwthinst2;
    @XmlElement(name = "ACWTHINST3")
    protected String acwthinst3;
    @XmlElement(name = "ACWTHINST4")
    protected String acwthinst4;
    @XmlElement(name = "ACWTHINST5")
    protected String acwthinst5;
    @XmlElement(name = "ORDINST1")
    protected String ordinst1;
    @XmlElement(name = "ORDINST2")
    protected String ordinst2;
    @XmlElement(name = "ORDINST3")
    protected String ordinst3;
    @XmlElement(name = "ORDINST4")
    protected String ordinst4;
    @XmlElement(name = "ORDINST5")
    protected String ordinst5;

    /**
     * Gets the value of the chgref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGREF() {
        return chgref;
    }

    /**
     * Sets the value of the chgref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGREF(String value) {
        this.chgref = value;
    }

    /**
     * Gets the value of the chgccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGCCY() {
        return chgccy;
    }

    /**
     * Sets the value of the chgccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGCCY(String value) {
        this.chgccy = value;
    }

    /**
     * Gets the value of the chgamt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCHGAMT() {
        return chgamt;
    }

    /**
     * Sets the value of the chgamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCHGAMT(BigDecimal value) {
        this.chgamt = value;
    }

    /**
     * Gets the value of the chgdet1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET1() {
        return chgdet1;
    }

    /**
     * Sets the value of the chgdet1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET1(String value) {
        this.chgdet1 = value;
    }

    /**
     * Gets the value of the chgdet2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET2() {
        return chgdet2;
    }

    /**
     * Sets the value of the chgdet2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET2(String value) {
        this.chgdet2 = value;
    }

    /**
     * Gets the value of the chgdet3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET3() {
        return chgdet3;
    }

    /**
     * Sets the value of the chgdet3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET3(String value) {
        this.chgdet3 = value;
    }

    /**
     * Gets the value of the chgdet4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET4() {
        return chgdet4;
    }

    /**
     * Sets the value of the chgdet4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET4(String value) {
        this.chgdet4 = value;
    }

    /**
     * Gets the value of the chgdet5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET5() {
        return chgdet5;
    }

    /**
     * Sets the value of the chgdet5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET5(String value) {
        this.chgdet5 = value;
    }

    /**
     * Gets the value of the chgdet6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGDET6() {
        return chgdet6;
    }

    /**
     * Sets the value of the chgdet6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGDET6(String value) {
        this.chgdet6 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO1() {
        return sndrrcvrinfo1;
    }

    /**
     * Sets the value of the sndrrcvrinfo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO1(String value) {
        this.sndrrcvrinfo1 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO2() {
        return sndrrcvrinfo2;
    }

    /**
     * Sets the value of the sndrrcvrinfo2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO2(String value) {
        this.sndrrcvrinfo2 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO3() {
        return sndrrcvrinfo3;
    }

    /**
     * Sets the value of the sndrrcvrinfo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO3(String value) {
        this.sndrrcvrinfo3 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO4() {
        return sndrrcvrinfo4;
    }

    /**
     * Sets the value of the sndrrcvrinfo4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO4(String value) {
        this.sndrrcvrinfo4 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO5() {
        return sndrrcvrinfo5;
    }

    /**
     * Sets the value of the sndrrcvrinfo5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO5(String value) {
        this.sndrrcvrinfo5 = value;
    }

    /**
     * Gets the value of the sndrrcvrinfo6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRRCVRINFO6() {
        return sndrrcvrinfo6;
    }

    /**
     * Sets the value of the sndrrcvrinfo6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRRCVRINFO6(String value) {
        this.sndrrcvrinfo6 = value;
    }

    /**
     * Gets the value of the acwthinst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST1() {
        return acwthinst1;
    }

    /**
     * Sets the value of the acwthinst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST1(String value) {
        this.acwthinst1 = value;
    }

    /**
     * Gets the value of the acwthinst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST2() {
        return acwthinst2;
    }

    /**
     * Sets the value of the acwthinst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST2(String value) {
        this.acwthinst2 = value;
    }

    /**
     * Gets the value of the acwthinst3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST3() {
        return acwthinst3;
    }

    /**
     * Sets the value of the acwthinst3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST3(String value) {
        this.acwthinst3 = value;
    }

    /**
     * Gets the value of the acwthinst4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST4() {
        return acwthinst4;
    }

    /**
     * Sets the value of the acwthinst4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST4(String value) {
        this.acwthinst4 = value;
    }

    /**
     * Gets the value of the acwthinst5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST5() {
        return acwthinst5;
    }

    /**
     * Sets the value of the acwthinst5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST5(String value) {
        this.acwthinst5 = value;
    }

    /**
     * Gets the value of the ordinst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST1() {
        return ordinst1;
    }

    /**
     * Sets the value of the ordinst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST1(String value) {
        this.ordinst1 = value;
    }

    /**
     * Gets the value of the ordinst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST2() {
        return ordinst2;
    }

    /**
     * Sets the value of the ordinst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST2(String value) {
        this.ordinst2 = value;
    }

    /**
     * Gets the value of the ordinst3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST3() {
        return ordinst3;
    }

    /**
     * Sets the value of the ordinst3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST3(String value) {
        this.ordinst3 = value;
    }

    /**
     * Gets the value of the ordinst4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST4() {
        return ordinst4;
    }

    /**
     * Sets the value of the ordinst4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST4(String value) {
        this.ordinst4 = value;
    }

    /**
     * Gets the value of the ordinst5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST5() {
        return ordinst5;
    }

    /**
     * Sets the value of the ordinst5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST5(String value) {
        this.ordinst5 = value;
    }

}
