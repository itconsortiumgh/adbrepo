
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FTContract-Full-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FTContract-Full-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="XREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FCCREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FCCESN" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="EVENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LATESTVERNO" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="FCCVER" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="PROD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PRODESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BNKOPRCD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSTRCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSTRCODEDESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRNTYPCD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="USRREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSGREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EXTREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BOOKDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DRCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRCCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DRAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CRAMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DRACCBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRACCBRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DRACC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRACC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DRVDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="CRVDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DRSPRED" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="DRSPREDDATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="CRSPRED" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="CRSPREDDATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DRIBAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CRIBAN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RATETYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SPRDCD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LCYEQ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SPREDDEFN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CUSTSPREAD" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="BASERATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="XRATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="MCKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHGWHOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSGASOF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RTASOF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACTGASOF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AFTRTCHG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OVDOVRDRFT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMITMSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPLOAD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RECIEVER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CORESPREQD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PMTDET1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PMTDET2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PMTDET3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PMTDET4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BYORDOF6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBEN6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACWTHINST6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMARKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSGDATE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ACTGDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="RATEPICKDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="FXCNTREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="XRATEDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="XRATESER" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="MLTCUSTRF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MLTCRDTREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CONSACREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CONSOLSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REGREP1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REGREP2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REGREP3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENVCNTNT1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENVCNTNT2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENVCNTNT3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENVCNTNT4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ENVCNTNT5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STORDCUST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STOURSELVES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STRECEIVER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STACWTINST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STULTBEN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STCORRESP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STREMINST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STRECORRESP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STINTRMED" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELMODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKERSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CONTSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROCESSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TANKSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRANSTYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENFAX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENMOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENEMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EXTDEALLINKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Custtfr-Contract-Dtls" type="{http://fcubs.ofss.com/service/FCUBSFTService}CusttfrContractDtls" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Settlement-Addl-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}SettlementAddlDetailsType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Charge-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}ChargeDetailsType1" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Taxdetails-Main" type="{http://fcubs.ofss.com/service/FCUBSFTService}TaxMainType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Taxdetails" type="{http://fcubs.ofss.com/service/FCUBSFTService}TaxDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Advice-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}ADVDETAILSType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Charge-Claim-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}ChargeClaimeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Mis-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}MISDETAILSType" minOccurs="0"/>
 *         &lt;element name="Udf-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}UDFDETAILSType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Contract-Overrides" type="{http://fcubs.ofss.com/service/FCUBSFTService}ContractOverrides" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Contract-Project-Details" type="{http://fcubs.ofss.com/service/FCUBSFTService}ProjectDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FTContract-Full-Type", propOrder = {
    "xref",
    "fccref",
    "fccesn",
    "event",
    "latestverno",
    "fccver",
    "prod",
    "prodesc",
    "bnkoprcd",
    "instrcode",
    "instrcodedesc",
    "trntypcd",
    "usrref",
    "msgref",
    "extrefno",
    "bookdt",
    "drccy",
    "crccy",
    "dramt",
    "cramt",
    "draccbrn",
    "craccbrn",
    "dracc",
    "cracc",
    "drvdt",
    "crvdt",
    "drspred",
    "drspreddate",
    "crspred",
    "crspreddate",
    "driban",
    "criban",
    "ratetype",
    "sprdcd",
    "lcyeq",
    "spreddefn",
    "ssn",
    "custspread",
    "baserate",
    "xrate",
    "mckno",
    "chkno",
    "chgwhom",
    "msgasof",
    "rtasof",
    "actgasof",
    "aftrtchg",
    "ovdovrdrft",
    "remitmsg",
    "upload",
    "reciever",
    "corespreqd",
    "pmtdet1",
    "pmtdet2",
    "pmtdet3",
    "pmtdet4",
    "byordof1",
    "byordof2",
    "byordof3",
    "byordof4",
    "byordof5",
    "byordof6",
    "ultben1",
    "ultben2",
    "ultben3",
    "ultben4",
    "ultben5",
    "ultben6",
    "acwthinst1",
    "acwthinst2",
    "acwthinst3",
    "acwthinst4",
    "acwthinst5",
    "acwthinst6",
    "remarks",
    "msgdate",
    "actgdt",
    "ratepickdt",
    "fxcntref",
    "xratedt",
    "xrateser",
    "mltcustrf",
    "mltcrdtref",
    "consacref",
    "consolstat",
    "regrep1",
    "regrep2",
    "regrep3",
    "envcntnt1",
    "envcntnt2",
    "envcntnt3",
    "envcntnt4",
    "envcntnt5",
    "stordcust",
    "stourselves",
    "streceiver",
    "stacwtinst",
    "stultben",
    "stcorresp",
    "streminst",
    "strecorresp",
    "stintrmed",
    "deladdrs1",
    "deladdrs2",
    "deladdrs3",
    "deladdrs4",
    "delmode",
    "makerid",
    "makerstamp",
    "checkerid",
    "checkerstamp",
    "contstat",
    "authstat",
    "processtat",
    "tankstat",
    "transtype",
    "benfax",
    "benmob",
    "benemail",
    "extdeallinkno",
    "custtfrContractDtls",
    "settlementAddlDetails",
    "chargeDetails",
    "taxdetailsMain",
    "taxdetails",
    "adviceDetails",
    "chargeClaimDetails",
    "misDetails",
    "udfDetails",
    "contractOverrides",
    "contractProjectDetails"
})
public class FTContractFullType {

    @XmlElement(name = "XREF")
    protected String xref;
    @XmlElement(name = "FCCREF")
    protected String fccref;
    @XmlElement(name = "FCCESN")
    protected BigInteger fccesn;
    @XmlElement(name = "EVENT")
    protected String event;
    @XmlElement(name = "LATESTVERNO")
    protected BigInteger latestverno;
    @XmlElement(name = "FCCVER")
    protected BigInteger fccver;
    @XmlElement(name = "PROD")
    protected String prod;
    @XmlElement(name = "PRODESC")
    protected String prodesc;
    @XmlElement(name = "BNKOPRCD")
    protected String bnkoprcd;
    @XmlElement(name = "INSTRCODE")
    protected String instrcode;
    @XmlElement(name = "INSTRCODEDESC")
    protected String instrcodedesc;
    @XmlElement(name = "TRNTYPCD")
    protected String trntypcd;
    @XmlElement(name = "USRREF")
    protected String usrref;
    @XmlElement(name = "MSGREF")
    protected String msgref;
    @XmlElement(name = "EXTREFNO")
    protected String extrefno;
    @XmlElement(name = "BOOKDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar bookdt;
    @XmlElement(name = "DRCCY")
    protected String drccy;
    @XmlElement(name = "CRCCY")
    protected String crccy;
    @XmlElement(name = "DRAMT")
    protected BigDecimal dramt;
    @XmlElement(name = "CRAMT")
    protected BigDecimal cramt;
    @XmlElement(name = "DRACCBRN")
    protected String draccbrn;
    @XmlElement(name = "CRACCBRN")
    protected String craccbrn;
    @XmlElement(name = "DRACC")
    protected String dracc;
    @XmlElement(name = "CRACC")
    protected String cracc;
    @XmlElement(name = "DRVDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar drvdt;
    @XmlElement(name = "CRVDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar crvdt;
    @XmlElement(name = "DRSPRED")
    protected BigInteger drspred;
    @XmlElement(name = "DRSPREDDATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar drspreddate;
    @XmlElement(name = "CRSPRED")
    protected BigInteger crspred;
    @XmlElement(name = "CRSPREDDATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar crspreddate;
    @XmlElement(name = "DRIBAN")
    protected String driban;
    @XmlElement(name = "CRIBAN")
    protected String criban;
    @XmlElement(name = "RATETYPE")
    protected String ratetype;
    @XmlElement(name = "SPRDCD")
    protected String sprdcd;
    @XmlElement(name = "LCYEQ")
    protected BigDecimal lcyeq;
    @XmlElement(name = "SPREDDEFN")
    protected String spreddefn;
    @XmlElement(name = "SSN")
    protected String ssn;
    @XmlElement(name = "CUSTSPREAD")
    protected BigDecimal custspread;
    @XmlElement(name = "BASERATE")
    protected BigDecimal baserate;
    @XmlElement(name = "XRATE")
    protected BigDecimal xrate;
    @XmlElement(name = "MCKNO")
    protected String mckno;
    @XmlElement(name = "CHKNO")
    protected String chkno;
    @XmlElement(name = "CHGWHOM")
    protected String chgwhom;
    @XmlElement(name = "MSGASOF")
    protected String msgasof;
    @XmlElement(name = "RTASOF")
    protected String rtasof;
    @XmlElement(name = "ACTGASOF")
    protected String actgasof;
    @XmlElement(name = "AFTRTCHG")
    protected String aftrtchg;
    @XmlElement(name = "OVDOVRDRFT")
    protected String ovdovrdrft;
    @XmlElement(name = "REMITMSG")
    protected String remitmsg;
    @XmlElement(name = "UPLOAD")
    protected String upload;
    @XmlElement(name = "RECIEVER")
    protected String reciever;
    @XmlElement(name = "CORESPREQD")
    protected String corespreqd;
    @XmlElement(name = "PMTDET1")
    protected String pmtdet1;
    @XmlElement(name = "PMTDET2")
    protected String pmtdet2;
    @XmlElement(name = "PMTDET3")
    protected String pmtdet3;
    @XmlElement(name = "PMTDET4")
    protected String pmtdet4;
    @XmlElement(name = "BYORDOF1")
    protected String byordof1;
    @XmlElement(name = "BYORDOF2")
    protected String byordof2;
    @XmlElement(name = "BYORDOF3")
    protected String byordof3;
    @XmlElement(name = "BYORDOF4")
    protected String byordof4;
    @XmlElement(name = "BYORDOF5")
    protected String byordof5;
    @XmlElement(name = "BYORDOF6")
    protected String byordof6;
    @XmlElement(name = "ULTBEN1")
    protected String ultben1;
    @XmlElement(name = "ULTBEN2")
    protected String ultben2;
    @XmlElement(name = "ULTBEN3")
    protected String ultben3;
    @XmlElement(name = "ULTBEN4")
    protected String ultben4;
    @XmlElement(name = "ULTBEN5")
    protected String ultben5;
    @XmlElement(name = "ULTBEN6")
    protected String ultben6;
    @XmlElement(name = "ACWTHINST1")
    protected String acwthinst1;
    @XmlElement(name = "ACWTHINST2")
    protected String acwthinst2;
    @XmlElement(name = "ACWTHINST3")
    protected String acwthinst3;
    @XmlElement(name = "ACWTHINST4")
    protected String acwthinst4;
    @XmlElement(name = "ACWTHINST5")
    protected String acwthinst5;
    @XmlElement(name = "ACWTHINST6")
    protected String acwthinst6;
    @XmlElement(name = "REMARKS")
    protected String remarks;
    @XmlElement(name = "MSGDATE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar msgdate;
    @XmlElement(name = "ACTGDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar actgdt;
    @XmlElement(name = "RATEPICKDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar ratepickdt;
    @XmlElement(name = "FXCNTREF")
    protected String fxcntref;
    @XmlElement(name = "XRATEDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar xratedt;
    @XmlElement(name = "XRATESER")
    protected BigInteger xrateser;
    @XmlElement(name = "MLTCUSTRF")
    protected String mltcustrf;
    @XmlElement(name = "MLTCRDTREF")
    protected String mltcrdtref;
    @XmlElement(name = "CONSACREF")
    protected String consacref;
    @XmlElement(name = "CONSOLSTAT")
    protected String consolstat;
    @XmlElement(name = "REGREP1")
    protected String regrep1;
    @XmlElement(name = "REGREP2")
    protected String regrep2;
    @XmlElement(name = "REGREP3")
    protected String regrep3;
    @XmlElement(name = "ENVCNTNT1")
    protected String envcntnt1;
    @XmlElement(name = "ENVCNTNT2")
    protected String envcntnt2;
    @XmlElement(name = "ENVCNTNT3")
    protected String envcntnt3;
    @XmlElement(name = "ENVCNTNT4")
    protected String envcntnt4;
    @XmlElement(name = "ENVCNTNT5")
    protected String envcntnt5;
    @XmlElement(name = "STORDCUST")
    protected String stordcust;
    @XmlElement(name = "STOURSELVES")
    protected String stourselves;
    @XmlElement(name = "STRECEIVER")
    protected String streceiver;
    @XmlElement(name = "STACWTINST")
    protected String stacwtinst;
    @XmlElement(name = "STULTBEN")
    protected String stultben;
    @XmlElement(name = "STCORRESP")
    protected String stcorresp;
    @XmlElement(name = "STREMINST")
    protected String streminst;
    @XmlElement(name = "STRECORRESP")
    protected String strecorresp;
    @XmlElement(name = "STINTRMED")
    protected String stintrmed;
    @XmlElement(name = "DELADDRS1")
    protected String deladdrs1;
    @XmlElement(name = "DELADDRS2")
    protected String deladdrs2;
    @XmlElement(name = "DELADDRS3")
    protected String deladdrs3;
    @XmlElement(name = "DELADDRS4")
    protected String deladdrs4;
    @XmlElement(name = "DELMODE")
    protected String delmode;
    @XmlElement(name = "MAKERID")
    protected String makerid;
    @XmlElement(name = "MAKERSTAMP")
    protected String makerstamp;
    @XmlElement(name = "CHECKERID")
    protected String checkerid;
    @XmlElement(name = "CHECKERSTAMP")
    protected String checkerstamp;
    @XmlElement(name = "CONTSTAT")
    protected String contstat;
    @XmlElement(name = "AUTHSTAT")
    protected String authstat;
    @XmlElement(name = "PROCESSTAT")
    protected String processtat;
    @XmlElement(name = "TANKSTAT")
    protected String tankstat;
    @XmlElement(name = "TRANSTYPE")
    protected String transtype;
    @XmlElement(name = "BENFAX")
    protected String benfax;
    @XmlElement(name = "BENMOB")
    protected String benmob;
    @XmlElement(name = "BENEMAIL")
    protected String benemail;
    @XmlElement(name = "EXTDEALLINKNO")
    protected String extdeallinkno;
    @XmlElement(name = "Custtfr-Contract-Dtls")
    protected List<CusttfrContractDtls> custtfrContractDtls;
    @XmlElement(name = "Settlement-Addl-Details")
    protected List<SettlementAddlDetailsType> settlementAddlDetails;
    @XmlElement(name = "Charge-Details")
    protected List<ChargeDetailsType1> chargeDetails;
    @XmlElement(name = "Taxdetails-Main")
    protected List<TaxMainType> taxdetailsMain;
    @XmlElement(name = "Taxdetails")
    protected List<TaxDetailType> taxdetails;
    @XmlElement(name = "Advice-Details")
    protected List<ADVDETAILSType> adviceDetails;
    @XmlElement(name = "Charge-Claim-Details")
    protected List<ChargeClaimeType> chargeClaimDetails;
    @XmlElement(name = "Mis-Details")
    protected MISDETAILSType misDetails;
    @XmlElement(name = "Udf-Details")
    protected List<UDFDETAILSType> udfDetails;
    @XmlElement(name = "Contract-Overrides")
    protected List<ContractOverrides> contractOverrides;
    @XmlElement(name = "Contract-Project-Details")
    protected List<ProjectDetails> contractProjectDetails;

    /**
     * Gets the value of the xref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXREF() {
        return xref;
    }

    /**
     * Sets the value of the xref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXREF(String value) {
        this.xref = value;
    }

    /**
     * Gets the value of the fccref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFCCREF() {
        return fccref;
    }

    /**
     * Sets the value of the fccref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFCCREF(String value) {
        this.fccref = value;
    }

    /**
     * Gets the value of the fccesn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFCCESN() {
        return fccesn;
    }

    /**
     * Sets the value of the fccesn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFCCESN(BigInteger value) {
        this.fccesn = value;
    }

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEVENT() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEVENT(String value) {
        this.event = value;
    }

    /**
     * Gets the value of the latestverno property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLATESTVERNO() {
        return latestverno;
    }

    /**
     * Sets the value of the latestverno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLATESTVERNO(BigInteger value) {
        this.latestverno = value;
    }

    /**
     * Gets the value of the fccver property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFCCVER() {
        return fccver;
    }

    /**
     * Sets the value of the fccver property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFCCVER(BigInteger value) {
        this.fccver = value;
    }

    /**
     * Gets the value of the prod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROD() {
        return prod;
    }

    /**
     * Sets the value of the prod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROD(String value) {
        this.prod = value;
    }

    /**
     * Gets the value of the prodesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRODESC() {
        return prodesc;
    }

    /**
     * Sets the value of the prodesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRODESC(String value) {
        this.prodesc = value;
    }

    /**
     * Gets the value of the bnkoprcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBNKOPRCD() {
        return bnkoprcd;
    }

    /**
     * Sets the value of the bnkoprcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBNKOPRCD(String value) {
        this.bnkoprcd = value;
    }

    /**
     * Gets the value of the instrcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRCODE() {
        return instrcode;
    }

    /**
     * Sets the value of the instrcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRCODE(String value) {
        this.instrcode = value;
    }

    /**
     * Gets the value of the instrcodedesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRCODEDESC() {
        return instrcodedesc;
    }

    /**
     * Sets the value of the instrcodedesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRCODEDESC(String value) {
        this.instrcodedesc = value;
    }

    /**
     * Gets the value of the trntypcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRNTYPCD() {
        return trntypcd;
    }

    /**
     * Sets the value of the trntypcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRNTYPCD(String value) {
        this.trntypcd = value;
    }

    /**
     * Gets the value of the usrref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSRREF() {
        return usrref;
    }

    /**
     * Sets the value of the usrref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSRREF(String value) {
        this.usrref = value;
    }

    /**
     * Gets the value of the msgref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGREF() {
        return msgref;
    }

    /**
     * Sets the value of the msgref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGREF(String value) {
        this.msgref = value;
    }

    /**
     * Gets the value of the extrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTREFNO() {
        return extrefno;
    }

    /**
     * Sets the value of the extrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTREFNO(String value) {
        this.extrefno = value;
    }

    /**
     * Gets the value of the bookdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBOOKDT() {
        return bookdt;
    }

    /**
     * Sets the value of the bookdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBOOKDT(XMLGregorianCalendar value) {
        this.bookdt = value;
    }

    /**
     * Gets the value of the drccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRCCY() {
        return drccy;
    }

    /**
     * Sets the value of the drccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRCCY(String value) {
        this.drccy = value;
    }

    /**
     * Gets the value of the crccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRCCY() {
        return crccy;
    }

    /**
     * Sets the value of the crccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRCCY(String value) {
        this.crccy = value;
    }

    /**
     * Gets the value of the dramt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDRAMT() {
        return dramt;
    }

    /**
     * Sets the value of the dramt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDRAMT(BigDecimal value) {
        this.dramt = value;
    }

    /**
     * Gets the value of the cramt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCRAMT() {
        return cramt;
    }

    /**
     * Sets the value of the cramt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCRAMT(BigDecimal value) {
        this.cramt = value;
    }

    /**
     * Gets the value of the draccbrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRACCBRN() {
        return draccbrn;
    }

    /**
     * Sets the value of the draccbrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRACCBRN(String value) {
        this.draccbrn = value;
    }

    /**
     * Gets the value of the craccbrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRACCBRN() {
        return craccbrn;
    }

    /**
     * Sets the value of the craccbrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRACCBRN(String value) {
        this.craccbrn = value;
    }

    /**
     * Gets the value of the dracc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRACC() {
        return dracc;
    }

    /**
     * Sets the value of the dracc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRACC(String value) {
        this.dracc = value;
    }

    /**
     * Gets the value of the cracc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRACC() {
        return cracc;
    }

    /**
     * Sets the value of the cracc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRACC(String value) {
        this.cracc = value;
    }

    /**
     * Gets the value of the drvdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDRVDT() {
        return drvdt;
    }

    /**
     * Sets the value of the drvdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDRVDT(XMLGregorianCalendar value) {
        this.drvdt = value;
    }

    /**
     * Gets the value of the crvdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCRVDT() {
        return crvdt;
    }

    /**
     * Sets the value of the crvdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCRVDT(XMLGregorianCalendar value) {
        this.crvdt = value;
    }

    /**
     * Gets the value of the drspred property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDRSPRED() {
        return drspred;
    }

    /**
     * Sets the value of the drspred property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDRSPRED(BigInteger value) {
        this.drspred = value;
    }

    /**
     * Gets the value of the drspreddate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDRSPREDDATE() {
        return drspreddate;
    }

    /**
     * Sets the value of the drspreddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDRSPREDDATE(XMLGregorianCalendar value) {
        this.drspreddate = value;
    }

    /**
     * Gets the value of the crspred property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCRSPRED() {
        return crspred;
    }

    /**
     * Sets the value of the crspred property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCRSPRED(BigInteger value) {
        this.crspred = value;
    }

    /**
     * Gets the value of the crspreddate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCRSPREDDATE() {
        return crspreddate;
    }

    /**
     * Sets the value of the crspreddate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCRSPREDDATE(XMLGregorianCalendar value) {
        this.crspreddate = value;
    }

    /**
     * Gets the value of the driban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRIBAN() {
        return driban;
    }

    /**
     * Sets the value of the driban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRIBAN(String value) {
        this.driban = value;
    }

    /**
     * Gets the value of the criban property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCRIBAN() {
        return criban;
    }

    /**
     * Sets the value of the criban property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCRIBAN(String value) {
        this.criban = value;
    }

    /**
     * Gets the value of the ratetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRATETYPE() {
        return ratetype;
    }

    /**
     * Sets the value of the ratetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRATETYPE(String value) {
        this.ratetype = value;
    }

    /**
     * Gets the value of the sprdcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPRDCD() {
        return sprdcd;
    }

    /**
     * Sets the value of the sprdcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPRDCD(String value) {
        this.sprdcd = value;
    }

    /**
     * Gets the value of the lcyeq property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLCYEQ() {
        return lcyeq;
    }

    /**
     * Sets the value of the lcyeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLCYEQ(BigDecimal value) {
        this.lcyeq = value;
    }

    /**
     * Gets the value of the spreddefn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPREDDEFN() {
        return spreddefn;
    }

    /**
     * Sets the value of the spreddefn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPREDDEFN(String value) {
        this.spreddefn = value;
    }

    /**
     * Gets the value of the ssn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSN() {
        return ssn;
    }

    /**
     * Sets the value of the ssn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSN(String value) {
        this.ssn = value;
    }

    /**
     * Gets the value of the custspread property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCUSTSPREAD() {
        return custspread;
    }

    /**
     * Sets the value of the custspread property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCUSTSPREAD(BigDecimal value) {
        this.custspread = value;
    }

    /**
     * Gets the value of the baserate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASERATE() {
        return baserate;
    }

    /**
     * Sets the value of the baserate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASERATE(BigDecimal value) {
        this.baserate = value;
    }

    /**
     * Gets the value of the xrate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getXRATE() {
        return xrate;
    }

    /**
     * Sets the value of the xrate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setXRATE(BigDecimal value) {
        this.xrate = value;
    }

    /**
     * Gets the value of the mckno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMCKNO() {
        return mckno;
    }

    /**
     * Sets the value of the mckno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMCKNO(String value) {
        this.mckno = value;
    }

    /**
     * Gets the value of the chkno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHKNO() {
        return chkno;
    }

    /**
     * Sets the value of the chkno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHKNO(String value) {
        this.chkno = value;
    }

    /**
     * Gets the value of the chgwhom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHGWHOM() {
        return chgwhom;
    }

    /**
     * Sets the value of the chgwhom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHGWHOM(String value) {
        this.chgwhom = value;
    }

    /**
     * Gets the value of the msgasof property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSGASOF() {
        return msgasof;
    }

    /**
     * Sets the value of the msgasof property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSGASOF(String value) {
        this.msgasof = value;
    }

    /**
     * Gets the value of the rtasof property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRTASOF() {
        return rtasof;
    }

    /**
     * Sets the value of the rtasof property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRTASOF(String value) {
        this.rtasof = value;
    }

    /**
     * Gets the value of the actgasof property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTGASOF() {
        return actgasof;
    }

    /**
     * Sets the value of the actgasof property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTGASOF(String value) {
        this.actgasof = value;
    }

    /**
     * Gets the value of the aftrtchg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAFTRTCHG() {
        return aftrtchg;
    }

    /**
     * Sets the value of the aftrtchg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAFTRTCHG(String value) {
        this.aftrtchg = value;
    }

    /**
     * Gets the value of the ovdovrdrft property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOVDOVRDRFT() {
        return ovdovrdrft;
    }

    /**
     * Sets the value of the ovdovrdrft property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOVDOVRDRFT(String value) {
        this.ovdovrdrft = value;
    }

    /**
     * Gets the value of the remitmsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMITMSG() {
        return remitmsg;
    }

    /**
     * Sets the value of the remitmsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMITMSG(String value) {
        this.remitmsg = value;
    }

    /**
     * Gets the value of the upload property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPLOAD() {
        return upload;
    }

    /**
     * Sets the value of the upload property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPLOAD(String value) {
        this.upload = value;
    }

    /**
     * Gets the value of the reciever property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECIEVER() {
        return reciever;
    }

    /**
     * Sets the value of the reciever property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECIEVER(String value) {
        this.reciever = value;
    }

    /**
     * Gets the value of the corespreqd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCORESPREQD() {
        return corespreqd;
    }

    /**
     * Sets the value of the corespreqd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCORESPREQD(String value) {
        this.corespreqd = value;
    }

    /**
     * Gets the value of the pmtdet1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTDET1() {
        return pmtdet1;
    }

    /**
     * Sets the value of the pmtdet1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTDET1(String value) {
        this.pmtdet1 = value;
    }

    /**
     * Gets the value of the pmtdet2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTDET2() {
        return pmtdet2;
    }

    /**
     * Sets the value of the pmtdet2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTDET2(String value) {
        this.pmtdet2 = value;
    }

    /**
     * Gets the value of the pmtdet3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTDET3() {
        return pmtdet3;
    }

    /**
     * Sets the value of the pmtdet3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTDET3(String value) {
        this.pmtdet3 = value;
    }

    /**
     * Gets the value of the pmtdet4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMTDET4() {
        return pmtdet4;
    }

    /**
     * Sets the value of the pmtdet4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMTDET4(String value) {
        this.pmtdet4 = value;
    }

    /**
     * Gets the value of the byordof1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF1() {
        return byordof1;
    }

    /**
     * Sets the value of the byordof1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF1(String value) {
        this.byordof1 = value;
    }

    /**
     * Gets the value of the byordof2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF2() {
        return byordof2;
    }

    /**
     * Sets the value of the byordof2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF2(String value) {
        this.byordof2 = value;
    }

    /**
     * Gets the value of the byordof3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF3() {
        return byordof3;
    }

    /**
     * Sets the value of the byordof3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF3(String value) {
        this.byordof3 = value;
    }

    /**
     * Gets the value of the byordof4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF4() {
        return byordof4;
    }

    /**
     * Sets the value of the byordof4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF4(String value) {
        this.byordof4 = value;
    }

    /**
     * Gets the value of the byordof5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF5() {
        return byordof5;
    }

    /**
     * Sets the value of the byordof5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF5(String value) {
        this.byordof5 = value;
    }

    /**
     * Gets the value of the byordof6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBYORDOF6() {
        return byordof6;
    }

    /**
     * Sets the value of the byordof6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBYORDOF6(String value) {
        this.byordof6 = value;
    }

    /**
     * Gets the value of the ultben1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN1() {
        return ultben1;
    }

    /**
     * Sets the value of the ultben1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN1(String value) {
        this.ultben1 = value;
    }

    /**
     * Gets the value of the ultben2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN2() {
        return ultben2;
    }

    /**
     * Sets the value of the ultben2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN2(String value) {
        this.ultben2 = value;
    }

    /**
     * Gets the value of the ultben3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN3() {
        return ultben3;
    }

    /**
     * Sets the value of the ultben3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN3(String value) {
        this.ultben3 = value;
    }

    /**
     * Gets the value of the ultben4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN4() {
        return ultben4;
    }

    /**
     * Sets the value of the ultben4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN4(String value) {
        this.ultben4 = value;
    }

    /**
     * Gets the value of the ultben5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN5() {
        return ultben5;
    }

    /**
     * Sets the value of the ultben5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN5(String value) {
        this.ultben5 = value;
    }

    /**
     * Gets the value of the ultben6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBEN6() {
        return ultben6;
    }

    /**
     * Sets the value of the ultben6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBEN6(String value) {
        this.ultben6 = value;
    }

    /**
     * Gets the value of the acwthinst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST1() {
        return acwthinst1;
    }

    /**
     * Sets the value of the acwthinst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST1(String value) {
        this.acwthinst1 = value;
    }

    /**
     * Gets the value of the acwthinst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST2() {
        return acwthinst2;
    }

    /**
     * Sets the value of the acwthinst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST2(String value) {
        this.acwthinst2 = value;
    }

    /**
     * Gets the value of the acwthinst3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST3() {
        return acwthinst3;
    }

    /**
     * Sets the value of the acwthinst3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST3(String value) {
        this.acwthinst3 = value;
    }

    /**
     * Gets the value of the acwthinst4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST4() {
        return acwthinst4;
    }

    /**
     * Sets the value of the acwthinst4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST4(String value) {
        this.acwthinst4 = value;
    }

    /**
     * Gets the value of the acwthinst5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST5() {
        return acwthinst5;
    }

    /**
     * Sets the value of the acwthinst5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST5(String value) {
        this.acwthinst5 = value;
    }

    /**
     * Gets the value of the acwthinst6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACWTHINST6() {
        return acwthinst6;
    }

    /**
     * Sets the value of the acwthinst6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACWTHINST6(String value) {
        this.acwthinst6 = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMARKS() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMARKS(String value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the msgdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMSGDATE() {
        return msgdate;
    }

    /**
     * Sets the value of the msgdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMSGDATE(XMLGregorianCalendar value) {
        this.msgdate = value;
    }

    /**
     * Gets the value of the actgdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getACTGDT() {
        return actgdt;
    }

    /**
     * Sets the value of the actgdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setACTGDT(XMLGregorianCalendar value) {
        this.actgdt = value;
    }

    /**
     * Gets the value of the ratepickdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRATEPICKDT() {
        return ratepickdt;
    }

    /**
     * Sets the value of the ratepickdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRATEPICKDT(XMLGregorianCalendar value) {
        this.ratepickdt = value;
    }

    /**
     * Gets the value of the fxcntref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFXCNTREF() {
        return fxcntref;
    }

    /**
     * Sets the value of the fxcntref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFXCNTREF(String value) {
        this.fxcntref = value;
    }

    /**
     * Gets the value of the xratedt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getXRATEDT() {
        return xratedt;
    }

    /**
     * Sets the value of the xratedt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setXRATEDT(XMLGregorianCalendar value) {
        this.xratedt = value;
    }

    /**
     * Gets the value of the xrateser property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getXRATESER() {
        return xrateser;
    }

    /**
     * Sets the value of the xrateser property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setXRATESER(BigInteger value) {
        this.xrateser = value;
    }

    /**
     * Gets the value of the mltcustrf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMLTCUSTRF() {
        return mltcustrf;
    }

    /**
     * Sets the value of the mltcustrf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMLTCUSTRF(String value) {
        this.mltcustrf = value;
    }

    /**
     * Gets the value of the mltcrdtref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMLTCRDTREF() {
        return mltcrdtref;
    }

    /**
     * Sets the value of the mltcrdtref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMLTCRDTREF(String value) {
        this.mltcrdtref = value;
    }

    /**
     * Gets the value of the consacref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONSACREF() {
        return consacref;
    }

    /**
     * Sets the value of the consacref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONSACREF(String value) {
        this.consacref = value;
    }

    /**
     * Gets the value of the consolstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONSOLSTAT() {
        return consolstat;
    }

    /**
     * Sets the value of the consolstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONSOLSTAT(String value) {
        this.consolstat = value;
    }

    /**
     * Gets the value of the regrep1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREGREP1() {
        return regrep1;
    }

    /**
     * Sets the value of the regrep1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREGREP1(String value) {
        this.regrep1 = value;
    }

    /**
     * Gets the value of the regrep2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREGREP2() {
        return regrep2;
    }

    /**
     * Sets the value of the regrep2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREGREP2(String value) {
        this.regrep2 = value;
    }

    /**
     * Gets the value of the regrep3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREGREP3() {
        return regrep3;
    }

    /**
     * Sets the value of the regrep3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREGREP3(String value) {
        this.regrep3 = value;
    }

    /**
     * Gets the value of the envcntnt1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENVCNTNT1() {
        return envcntnt1;
    }

    /**
     * Sets the value of the envcntnt1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENVCNTNT1(String value) {
        this.envcntnt1 = value;
    }

    /**
     * Gets the value of the envcntnt2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENVCNTNT2() {
        return envcntnt2;
    }

    /**
     * Sets the value of the envcntnt2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENVCNTNT2(String value) {
        this.envcntnt2 = value;
    }

    /**
     * Gets the value of the envcntnt3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENVCNTNT3() {
        return envcntnt3;
    }

    /**
     * Sets the value of the envcntnt3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENVCNTNT3(String value) {
        this.envcntnt3 = value;
    }

    /**
     * Gets the value of the envcntnt4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENVCNTNT4() {
        return envcntnt4;
    }

    /**
     * Sets the value of the envcntnt4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENVCNTNT4(String value) {
        this.envcntnt4 = value;
    }

    /**
     * Gets the value of the envcntnt5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENVCNTNT5() {
        return envcntnt5;
    }

    /**
     * Sets the value of the envcntnt5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENVCNTNT5(String value) {
        this.envcntnt5 = value;
    }

    /**
     * Gets the value of the stordcust property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTORDCUST() {
        return stordcust;
    }

    /**
     * Sets the value of the stordcust property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTORDCUST(String value) {
        this.stordcust = value;
    }

    /**
     * Gets the value of the stourselves property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTOURSELVES() {
        return stourselves;
    }

    /**
     * Sets the value of the stourselves property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTOURSELVES(String value) {
        this.stourselves = value;
    }

    /**
     * Gets the value of the streceiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTRECEIVER() {
        return streceiver;
    }

    /**
     * Sets the value of the streceiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTRECEIVER(String value) {
        this.streceiver = value;
    }

    /**
     * Gets the value of the stacwtinst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTACWTINST() {
        return stacwtinst;
    }

    /**
     * Sets the value of the stacwtinst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTACWTINST(String value) {
        this.stacwtinst = value;
    }

    /**
     * Gets the value of the stultben property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTULTBEN() {
        return stultben;
    }

    /**
     * Sets the value of the stultben property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTULTBEN(String value) {
        this.stultben = value;
    }

    /**
     * Gets the value of the stcorresp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTCORRESP() {
        return stcorresp;
    }

    /**
     * Sets the value of the stcorresp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTCORRESP(String value) {
        this.stcorresp = value;
    }

    /**
     * Gets the value of the streminst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTREMINST() {
        return streminst;
    }

    /**
     * Sets the value of the streminst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTREMINST(String value) {
        this.streminst = value;
    }

    /**
     * Gets the value of the strecorresp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTRECORRESP() {
        return strecorresp;
    }

    /**
     * Sets the value of the strecorresp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTRECORRESP(String value) {
        this.strecorresp = value;
    }

    /**
     * Gets the value of the stintrmed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTINTRMED() {
        return stintrmed;
    }

    /**
     * Sets the value of the stintrmed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTINTRMED(String value) {
        this.stintrmed = value;
    }

    /**
     * Gets the value of the deladdrs1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS1() {
        return deladdrs1;
    }

    /**
     * Sets the value of the deladdrs1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS1(String value) {
        this.deladdrs1 = value;
    }

    /**
     * Gets the value of the deladdrs2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS2() {
        return deladdrs2;
    }

    /**
     * Sets the value of the deladdrs2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS2(String value) {
        this.deladdrs2 = value;
    }

    /**
     * Gets the value of the deladdrs3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS3() {
        return deladdrs3;
    }

    /**
     * Sets the value of the deladdrs3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS3(String value) {
        this.deladdrs3 = value;
    }

    /**
     * Gets the value of the deladdrs4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS4() {
        return deladdrs4;
    }

    /**
     * Sets the value of the deladdrs4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS4(String value) {
        this.deladdrs4 = value;
    }

    /**
     * Gets the value of the delmode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELMODE() {
        return delmode;
    }

    /**
     * Sets the value of the delmode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELMODE(String value) {
        this.delmode = value;
    }

    /**
     * Gets the value of the makerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKERID() {
        return makerid;
    }

    /**
     * Sets the value of the makerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKERID(String value) {
        this.makerid = value;
    }

    /**
     * Gets the value of the makerstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKERSTAMP() {
        return makerstamp;
    }

    /**
     * Sets the value of the makerstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKERSTAMP(String value) {
        this.makerstamp = value;
    }

    /**
     * Gets the value of the checkerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKERID() {
        return checkerid;
    }

    /**
     * Sets the value of the checkerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKERID(String value) {
        this.checkerid = value;
    }

    /**
     * Gets the value of the checkerstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKERSTAMP() {
        return checkerstamp;
    }

    /**
     * Sets the value of the checkerstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKERSTAMP(String value) {
        this.checkerstamp = value;
    }

    /**
     * Gets the value of the contstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTSTAT() {
        return contstat;
    }

    /**
     * Sets the value of the contstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTSTAT(String value) {
        this.contstat = value;
    }

    /**
     * Gets the value of the authstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHSTAT() {
        return authstat;
    }

    /**
     * Sets the value of the authstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHSTAT(String value) {
        this.authstat = value;
    }

    /**
     * Gets the value of the processtat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROCESSTAT() {
        return processtat;
    }

    /**
     * Sets the value of the processtat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROCESSTAT(String value) {
        this.processtat = value;
    }

    /**
     * Gets the value of the tankstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTANKSTAT() {
        return tankstat;
    }

    /**
     * Sets the value of the tankstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTANKSTAT(String value) {
        this.tankstat = value;
    }

    /**
     * Gets the value of the transtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRANSTYPE() {
        return transtype;
    }

    /**
     * Sets the value of the transtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRANSTYPE(String value) {
        this.transtype = value;
    }

    /**
     * Gets the value of the benfax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENFAX() {
        return benfax;
    }

    /**
     * Sets the value of the benfax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENFAX(String value) {
        this.benfax = value;
    }

    /**
     * Gets the value of the benmob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENMOB() {
        return benmob;
    }

    /**
     * Sets the value of the benmob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENMOB(String value) {
        this.benmob = value;
    }

    /**
     * Gets the value of the benemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENEMAIL() {
        return benemail;
    }

    /**
     * Sets the value of the benemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENEMAIL(String value) {
        this.benemail = value;
    }

    /**
     * Gets the value of the extdeallinkno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTDEALLINKNO() {
        return extdeallinkno;
    }

    /**
     * Sets the value of the extdeallinkno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTDEALLINKNO(String value) {
        this.extdeallinkno = value;
    }

    /**
     * Gets the value of the custtfrContractDtls property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custtfrContractDtls property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCusttfrContractDtls().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CusttfrContractDtls }
     * 
     * 
     */
    public List<CusttfrContractDtls> getCusttfrContractDtls() {
        if (custtfrContractDtls == null) {
            custtfrContractDtls = new ArrayList<CusttfrContractDtls>();
        }
        return this.custtfrContractDtls;
    }

    /**
     * Gets the value of the settlementAddlDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the settlementAddlDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSettlementAddlDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SettlementAddlDetailsType }
     * 
     * 
     */
    public List<SettlementAddlDetailsType> getSettlementAddlDetails() {
        if (settlementAddlDetails == null) {
            settlementAddlDetails = new ArrayList<SettlementAddlDetailsType>();
        }
        return this.settlementAddlDetails;
    }

    /**
     * Gets the value of the chargeDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chargeDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChargeDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChargeDetailsType1 }
     * 
     * 
     */
    public List<ChargeDetailsType1> getChargeDetails() {
        if (chargeDetails == null) {
            chargeDetails = new ArrayList<ChargeDetailsType1>();
        }
        return this.chargeDetails;
    }

    /**
     * Gets the value of the taxdetailsMain property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxdetailsMain property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxdetailsMain().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxMainType }
     * 
     * 
     */
    public List<TaxMainType> getTaxdetailsMain() {
        if (taxdetailsMain == null) {
            taxdetailsMain = new ArrayList<TaxMainType>();
        }
        return this.taxdetailsMain;
    }

    /**
     * Gets the value of the taxdetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taxdetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaxdetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaxDetailType }
     * 
     * 
     */
    public List<TaxDetailType> getTaxdetails() {
        if (taxdetails == null) {
            taxdetails = new ArrayList<TaxDetailType>();
        }
        return this.taxdetails;
    }

    /**
     * Gets the value of the adviceDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adviceDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdviceDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ADVDETAILSType }
     * 
     * 
     */
    public List<ADVDETAILSType> getAdviceDetails() {
        if (adviceDetails == null) {
            adviceDetails = new ArrayList<ADVDETAILSType>();
        }
        return this.adviceDetails;
    }

    /**
     * Gets the value of the chargeClaimDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chargeClaimDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChargeClaimDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChargeClaimeType }
     * 
     * 
     */
    public List<ChargeClaimeType> getChargeClaimDetails() {
        if (chargeClaimDetails == null) {
            chargeClaimDetails = new ArrayList<ChargeClaimeType>();
        }
        return this.chargeClaimDetails;
    }

    /**
     * Gets the value of the misDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MISDETAILSType }
     *     
     */
    public MISDETAILSType getMisDetails() {
        return misDetails;
    }

    /**
     * Sets the value of the misDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MISDETAILSType }
     *     
     */
    public void setMisDetails(MISDETAILSType value) {
        this.misDetails = value;
    }

    /**
     * Gets the value of the udfDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the udfDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUdfDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UDFDETAILSType }
     * 
     * 
     */
    public List<UDFDETAILSType> getUdfDetails() {
        if (udfDetails == null) {
            udfDetails = new ArrayList<UDFDETAILSType>();
        }
        return this.udfDetails;
    }

    /**
     * Gets the value of the contractOverrides property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractOverrides property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractOverrides().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContractOverrides }
     * 
     * 
     */
    public List<ContractOverrides> getContractOverrides() {
        if (contractOverrides == null) {
            contractOverrides = new ArrayList<ContractOverrides>();
        }
        return this.contractOverrides;
    }

    /**
     * Gets the value of the contractProjectDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractProjectDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractProjectDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDetails }
     * 
     * 
     */
    public List<ProjectDetails> getContractProjectDetails() {
        if (contractProjectDetails == null) {
            contractProjectDetails = new ArrayList<ProjectDetails>();
        }
        return this.contractProjectDetails;
    }

}
