
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CusttfrContractDtls complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CusttfrContractDtls">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EXTREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDCUST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDCUST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDCUST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDCUST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDCUST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDINST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INTERMEDIARY1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INTERMEDIARY2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INTERMEDIARY3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INTERMEDIARY4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INTERMEDIARY5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCWITHINST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCWITHINST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCWITHINST3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCWITHINST4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACCWITHINST5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBENEF1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBENEF2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBENEF3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBENEF4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ULTBENEF5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMINFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMINFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMINFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REMINFO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SNDRCVR6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="VERSIONNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CusttfrContractDtls", propOrder = {
    "extrefno",
    "ordcust1",
    "ordcust2",
    "ordcust3",
    "ordcust4",
    "ordcust5",
    "ordinst1",
    "ordinst2",
    "ordinst3",
    "ordinst4",
    "ordinst5",
    "intermediary1",
    "intermediary2",
    "intermediary3",
    "intermediary4",
    "intermediary5",
    "accwithinst1",
    "accwithinst2",
    "accwithinst3",
    "accwithinst4",
    "accwithinst5",
    "ultbenef1",
    "ultbenef2",
    "ultbenef3",
    "ultbenef4",
    "ultbenef5",
    "reminfo1",
    "reminfo2",
    "reminfo3",
    "reminfo4",
    "sndrcvr1",
    "sndrcvr2",
    "sndrcvr3",
    "sndrcvr4",
    "sndrcvr5",
    "sndrcvr6",
    "ccy",
    "amount",
    "versionno"
})
public class CusttfrContractDtls {

    @XmlElement(name = "EXTREFNO")
    protected String extrefno;
    @XmlElement(name = "ORDCUST1")
    protected String ordcust1;
    @XmlElement(name = "ORDCUST2")
    protected String ordcust2;
    @XmlElement(name = "ORDCUST3")
    protected String ordcust3;
    @XmlElement(name = "ORDCUST4")
    protected String ordcust4;
    @XmlElement(name = "ORDCUST5")
    protected String ordcust5;
    @XmlElement(name = "ORDINST1")
    protected String ordinst1;
    @XmlElement(name = "ORDINST2")
    protected String ordinst2;
    @XmlElement(name = "ORDINST3")
    protected String ordinst3;
    @XmlElement(name = "ORDINST4")
    protected String ordinst4;
    @XmlElement(name = "ORDINST5")
    protected String ordinst5;
    @XmlElement(name = "INTERMEDIARY1")
    protected String intermediary1;
    @XmlElement(name = "INTERMEDIARY2")
    protected String intermediary2;
    @XmlElement(name = "INTERMEDIARY3")
    protected String intermediary3;
    @XmlElement(name = "INTERMEDIARY4")
    protected String intermediary4;
    @XmlElement(name = "INTERMEDIARY5")
    protected String intermediary5;
    @XmlElement(name = "ACCWITHINST1")
    protected String accwithinst1;
    @XmlElement(name = "ACCWITHINST2")
    protected String accwithinst2;
    @XmlElement(name = "ACCWITHINST3")
    protected String accwithinst3;
    @XmlElement(name = "ACCWITHINST4")
    protected String accwithinst4;
    @XmlElement(name = "ACCWITHINST5")
    protected String accwithinst5;
    @XmlElement(name = "ULTBENEF1")
    protected String ultbenef1;
    @XmlElement(name = "ULTBENEF2")
    protected String ultbenef2;
    @XmlElement(name = "ULTBENEF3")
    protected String ultbenef3;
    @XmlElement(name = "ULTBENEF4")
    protected String ultbenef4;
    @XmlElement(name = "ULTBENEF5")
    protected String ultbenef5;
    @XmlElement(name = "REMINFO1")
    protected String reminfo1;
    @XmlElement(name = "REMINFO2")
    protected String reminfo2;
    @XmlElement(name = "REMINFO3")
    protected String reminfo3;
    @XmlElement(name = "REMINFO4")
    protected String reminfo4;
    @XmlElement(name = "SNDRCVR1")
    protected String sndrcvr1;
    @XmlElement(name = "SNDRCVR2")
    protected String sndrcvr2;
    @XmlElement(name = "SNDRCVR3")
    protected String sndrcvr3;
    @XmlElement(name = "SNDRCVR4")
    protected String sndrcvr4;
    @XmlElement(name = "SNDRCVR5")
    protected String sndrcvr5;
    @XmlElement(name = "SNDRCVR6")
    protected String sndrcvr6;
    @XmlElement(name = "CCY")
    protected String ccy;
    @XmlElement(name = "AMOUNT")
    protected BigDecimal amount;
    @XmlElement(name = "VERSIONNO")
    protected BigDecimal versionno;

    /**
     * Gets the value of the extrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTREFNO() {
        return extrefno;
    }

    /**
     * Sets the value of the extrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTREFNO(String value) {
        this.extrefno = value;
    }

    /**
     * Gets the value of the ordcust1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDCUST1() {
        return ordcust1;
    }

    /**
     * Sets the value of the ordcust1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDCUST1(String value) {
        this.ordcust1 = value;
    }

    /**
     * Gets the value of the ordcust2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDCUST2() {
        return ordcust2;
    }

    /**
     * Sets the value of the ordcust2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDCUST2(String value) {
        this.ordcust2 = value;
    }

    /**
     * Gets the value of the ordcust3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDCUST3() {
        return ordcust3;
    }

    /**
     * Sets the value of the ordcust3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDCUST3(String value) {
        this.ordcust3 = value;
    }

    /**
     * Gets the value of the ordcust4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDCUST4() {
        return ordcust4;
    }

    /**
     * Sets the value of the ordcust4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDCUST4(String value) {
        this.ordcust4 = value;
    }

    /**
     * Gets the value of the ordcust5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDCUST5() {
        return ordcust5;
    }

    /**
     * Sets the value of the ordcust5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDCUST5(String value) {
        this.ordcust5 = value;
    }

    /**
     * Gets the value of the ordinst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST1() {
        return ordinst1;
    }

    /**
     * Sets the value of the ordinst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST1(String value) {
        this.ordinst1 = value;
    }

    /**
     * Gets the value of the ordinst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST2() {
        return ordinst2;
    }

    /**
     * Sets the value of the ordinst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST2(String value) {
        this.ordinst2 = value;
    }

    /**
     * Gets the value of the ordinst3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST3() {
        return ordinst3;
    }

    /**
     * Sets the value of the ordinst3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST3(String value) {
        this.ordinst3 = value;
    }

    /**
     * Gets the value of the ordinst4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST4() {
        return ordinst4;
    }

    /**
     * Sets the value of the ordinst4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST4(String value) {
        this.ordinst4 = value;
    }

    /**
     * Gets the value of the ordinst5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDINST5() {
        return ordinst5;
    }

    /**
     * Sets the value of the ordinst5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDINST5(String value) {
        this.ordinst5 = value;
    }

    /**
     * Gets the value of the intermediary1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERMEDIARY1() {
        return intermediary1;
    }

    /**
     * Sets the value of the intermediary1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERMEDIARY1(String value) {
        this.intermediary1 = value;
    }

    /**
     * Gets the value of the intermediary2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERMEDIARY2() {
        return intermediary2;
    }

    /**
     * Sets the value of the intermediary2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERMEDIARY2(String value) {
        this.intermediary2 = value;
    }

    /**
     * Gets the value of the intermediary3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERMEDIARY3() {
        return intermediary3;
    }

    /**
     * Sets the value of the intermediary3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERMEDIARY3(String value) {
        this.intermediary3 = value;
    }

    /**
     * Gets the value of the intermediary4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERMEDIARY4() {
        return intermediary4;
    }

    /**
     * Sets the value of the intermediary4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERMEDIARY4(String value) {
        this.intermediary4 = value;
    }

    /**
     * Gets the value of the intermediary5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTERMEDIARY5() {
        return intermediary5;
    }

    /**
     * Sets the value of the intermediary5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTERMEDIARY5(String value) {
        this.intermediary5 = value;
    }

    /**
     * Gets the value of the accwithinst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCWITHINST1() {
        return accwithinst1;
    }

    /**
     * Sets the value of the accwithinst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCWITHINST1(String value) {
        this.accwithinst1 = value;
    }

    /**
     * Gets the value of the accwithinst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCWITHINST2() {
        return accwithinst2;
    }

    /**
     * Sets the value of the accwithinst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCWITHINST2(String value) {
        this.accwithinst2 = value;
    }

    /**
     * Gets the value of the accwithinst3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCWITHINST3() {
        return accwithinst3;
    }

    /**
     * Sets the value of the accwithinst3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCWITHINST3(String value) {
        this.accwithinst3 = value;
    }

    /**
     * Gets the value of the accwithinst4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCWITHINST4() {
        return accwithinst4;
    }

    /**
     * Sets the value of the accwithinst4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCWITHINST4(String value) {
        this.accwithinst4 = value;
    }

    /**
     * Gets the value of the accwithinst5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCWITHINST5() {
        return accwithinst5;
    }

    /**
     * Sets the value of the accwithinst5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCWITHINST5(String value) {
        this.accwithinst5 = value;
    }

    /**
     * Gets the value of the ultbenef1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBENEF1() {
        return ultbenef1;
    }

    /**
     * Sets the value of the ultbenef1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBENEF1(String value) {
        this.ultbenef1 = value;
    }

    /**
     * Gets the value of the ultbenef2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBENEF2() {
        return ultbenef2;
    }

    /**
     * Sets the value of the ultbenef2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBENEF2(String value) {
        this.ultbenef2 = value;
    }

    /**
     * Gets the value of the ultbenef3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBENEF3() {
        return ultbenef3;
    }

    /**
     * Sets the value of the ultbenef3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBENEF3(String value) {
        this.ultbenef3 = value;
    }

    /**
     * Gets the value of the ultbenef4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBENEF4() {
        return ultbenef4;
    }

    /**
     * Sets the value of the ultbenef4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBENEF4(String value) {
        this.ultbenef4 = value;
    }

    /**
     * Gets the value of the ultbenef5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULTBENEF5() {
        return ultbenef5;
    }

    /**
     * Sets the value of the ultbenef5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULTBENEF5(String value) {
        this.ultbenef5 = value;
    }

    /**
     * Gets the value of the reminfo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMINFO1() {
        return reminfo1;
    }

    /**
     * Sets the value of the reminfo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMINFO1(String value) {
        this.reminfo1 = value;
    }

    /**
     * Gets the value of the reminfo2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMINFO2() {
        return reminfo2;
    }

    /**
     * Sets the value of the reminfo2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMINFO2(String value) {
        this.reminfo2 = value;
    }

    /**
     * Gets the value of the reminfo3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMINFO3() {
        return reminfo3;
    }

    /**
     * Sets the value of the reminfo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMINFO3(String value) {
        this.reminfo3 = value;
    }

    /**
     * Gets the value of the reminfo4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREMINFO4() {
        return reminfo4;
    }

    /**
     * Sets the value of the reminfo4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREMINFO4(String value) {
        this.reminfo4 = value;
    }

    /**
     * Gets the value of the sndrcvr1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR1() {
        return sndrcvr1;
    }

    /**
     * Sets the value of the sndrcvr1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR1(String value) {
        this.sndrcvr1 = value;
    }

    /**
     * Gets the value of the sndrcvr2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR2() {
        return sndrcvr2;
    }

    /**
     * Sets the value of the sndrcvr2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR2(String value) {
        this.sndrcvr2 = value;
    }

    /**
     * Gets the value of the sndrcvr3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR3() {
        return sndrcvr3;
    }

    /**
     * Sets the value of the sndrcvr3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR3(String value) {
        this.sndrcvr3 = value;
    }

    /**
     * Gets the value of the sndrcvr4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR4() {
        return sndrcvr4;
    }

    /**
     * Sets the value of the sndrcvr4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR4(String value) {
        this.sndrcvr4 = value;
    }

    /**
     * Gets the value of the sndrcvr5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR5() {
        return sndrcvr5;
    }

    /**
     * Sets the value of the sndrcvr5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR5(String value) {
        this.sndrcvr5 = value;
    }

    /**
     * Gets the value of the sndrcvr6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSNDRCVR6() {
        return sndrcvr6;
    }

    /**
     * Sets the value of the sndrcvr6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSNDRCVR6(String value) {
        this.sndrcvr6 = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCY() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCY(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMOUNT(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the versionno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVERSIONNO() {
        return versionno;
    }

    /**
     * Sets the value of the versionno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVERSIONNO(BigDecimal value) {
        this.versionno = value;
    }

}
