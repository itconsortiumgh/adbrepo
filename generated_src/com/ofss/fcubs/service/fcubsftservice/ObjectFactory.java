
package com.ofss.fcubs.service.fcubsftservice;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ofss.fcubs.service.fcubsftservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ofss.fcubs.service.fcubsftservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTIOPKRES }
     * 
     */
    public MODIFYPRODUCTIOPKRES createMODIFYPRODUCTIOPKRES() {
        return new MODIFYPRODUCTIOPKRES();
    }

    /**
     * Create an instance of {@link CREATEMT101IOPKREQ }
     * 
     */
    public CREATEMT101IOPKREQ createCREATEMT101IOPKREQ() {
        return new CREATEMT101IOPKREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTIOPKREQ }
     * 
     */
    public AUTHORIZEFTCONTRACTIOPKREQ createAUTHORIZEFTCONTRACTIOPKREQ() {
        return new AUTHORIZEFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTIOPKREQ }
     * 
     */
    public MODIFYPRODUCTIOPKREQ createMODIFYPRODUCTIOPKREQ() {
        return new MODIFYPRODUCTIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEMT101IOPKRES }
     * 
     */
    public CREATEMT101IOPKRES createCREATEMT101IOPKRES() {
        return new CREATEMT101IOPKRES();
    }

    /**
     * Create an instance of {@link QUERYFTCONTRACTIOFSRES }
     * 
     */
    public QUERYFTCONTRACTIOFSRES createQUERYFTCONTRACTIOFSRES() {
        return new QUERYFTCONTRACTIOFSRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTIOPKRES }
     * 
     */
    public AUTHORIZEFTCONTRACTIOPKRES createAUTHORIZEFTCONTRACTIOPKRES() {
        return new AUTHORIZEFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEMT101FSFSRES }
     * 
     */
    public DELETEMT101FSFSRES createDELETEMT101FSFSRES() {
        return new DELETEMT101FSFSRES();
    }

    /**
     * Create an instance of {@link MODIFYMT101IOPKREQ }
     * 
     */
    public MODIFYMT101IOPKREQ createMODIFYMT101IOPKREQ() {
        return new MODIFYMT101IOPKREQ();
    }

    /**
     * Create an instance of {@link QUERYFTCONTRACTIOFSREQ }
     * 
     */
    public QUERYFTCONTRACTIOFSREQ createQUERYFTCONTRACTIOFSREQ() {
        return new QUERYFTCONTRACTIOFSREQ();
    }

    /**
     * Create an instance of {@link DELETEMT101FSFSREQ }
     * 
     */
    public DELETEMT101FSFSREQ createDELETEMT101FSFSREQ() {
        return new DELETEMT101FSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTFSFSRES }
     * 
     */
    public MODIFYPRODUCTFSFSRES createMODIFYPRODUCTFSFSRES() {
        return new MODIFYPRODUCTFSFSRES();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTFSFSREQ }
     * 
     */
    public MODIFYPRODUCTFSFSREQ createMODIFYPRODUCTFSFSREQ() {
        return new MODIFYPRODUCTFSFSREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTFSFSRES }
     * 
     */
    public AUTHORIZEFTCONTRACTFSFSRES createAUTHORIZEFTCONTRACTFSFSRES() {
        return new AUTHORIZEFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTFSFSREQ }
     * 
     */
    public AUTHORIZEFTCONTRACTFSFSREQ createAUTHORIZEFTCONTRACTFSFSREQ() {
        return new AUTHORIZEFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101FSFSREQ }
     * 
     */
    public AUTHORIZEMT101FSFSREQ createAUTHORIZEMT101FSFSREQ() {
        return new AUTHORIZEMT101FSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYMT101IOPKRES }
     * 
     */
    public MODIFYMT101IOPKRES createMODIFYMT101IOPKRES() {
        return new MODIFYMT101IOPKRES();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTIOPKREQ }
     * 
     */
    public CREATEFTCONTRACTIOPKREQ createCREATEFTCONTRACTIOPKREQ() {
        return new CREATEFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEMT101FSFSREQ }
     * 
     */
    public CREATEMT101FSFSREQ createCREATEMT101FSFSREQ() {
        return new CREATEMT101FSFSREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101FSFSRES }
     * 
     */
    public AUTHORIZEMT101FSFSRES createAUTHORIZEMT101FSFSRES() {
        return new AUTHORIZEMT101FSFSRES();
    }

    /**
     * Create an instance of {@link CREATEMT101FSFSRES }
     * 
     */
    public CREATEMT101FSFSRES createCREATEMT101FSFSRES() {
        return new CREATEMT101FSFSRES();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTIOPKRES }
     * 
     */
    public CREATEFTCONTRACTIOPKRES createCREATEFTCONTRACTIOPKRES() {
        return new CREATEFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101IOPKRES }
     * 
     */
    public AUTHORIZEMT101IOPKRES createAUTHORIZEMT101IOPKRES() {
        return new AUTHORIZEMT101IOPKRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101IOPKREQ }
     * 
     */
    public AUTHORIZEMT101IOPKREQ createAUTHORIZEMT101IOPKREQ() {
        return new AUTHORIZEMT101IOPKREQ();
    }

    /**
     * Create an instance of {@link MODIFYMT101FSFSREQ }
     * 
     */
    public MODIFYMT101FSFSREQ createMODIFYMT101FSFSREQ() {
        return new MODIFYMT101FSFSREQ();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTIOPKRES }
     * 
     */
    public LIQUIDATEFTCONTRACTIOPKRES createLIQUIDATEFTCONTRACTIOPKRES() {
        return new LIQUIDATEFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTIOPKREQ }
     * 
     */
    public LIQUIDATEFTCONTRACTIOPKREQ createLIQUIDATEFTCONTRACTIOPKREQ() {
        return new LIQUIDATEFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTFSFSREQ }
     * 
     */
    public AMENDFTCONTRACTFSFSREQ createAMENDFTCONTRACTFSFSREQ() {
        return new AMENDFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTFSFSREQ }
     * 
     */
    public CREATEPRODUCTFSFSREQ createCREATEPRODUCTFSFSREQ() {
        return new CREATEPRODUCTFSFSREQ();
    }

    /**
     * Create an instance of {@link MODIFYMT101FSFSRES }
     * 
     */
    public MODIFYMT101FSFSRES createMODIFYMT101FSFSRES() {
        return new MODIFYMT101FSFSRES();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTFSFSRES }
     * 
     */
    public AMENDFTCONTRACTFSFSRES createAMENDFTCONTRACTFSFSRES() {
        return new AMENDFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTFSFSRES }
     * 
     */
    public CREATEPRODUCTFSFSRES createCREATEPRODUCTFSFSRES() {
        return new CREATEPRODUCTFSFSRES();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTFSFSREQ }
     * 
     */
    public DELETEFTCONTRACTFSFSREQ createDELETEFTCONTRACTFSFSREQ() {
        return new DELETEFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTFSFSREQ }
     * 
     */
    public DELETEPRODUCTFSFSREQ createDELETEPRODUCTFSFSREQ() {
        return new DELETEPRODUCTFSFSREQ();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTFSFSRES }
     * 
     */
    public DELETEFTCONTRACTFSFSRES createDELETEFTCONTRACTFSFSRES() {
        return new DELETEFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTFSFSRES }
     * 
     */
    public DELETEPRODUCTFSFSRES createDELETEPRODUCTFSFSRES() {
        return new DELETEPRODUCTFSFSRES();
    }

    /**
     * Create an instance of {@link REASSIGNFTCONTRACTFSFSREQ }
     * 
     */
    public REASSIGNFTCONTRACTFSFSREQ createREASSIGNFTCONTRACTFSFSREQ() {
        return new REASSIGNFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTFSFSRES }
     * 
     */
    public LIQUIDATEFTCONTRACTFSFSRES createLIQUIDATEFTCONTRACTFSFSRES() {
        return new LIQUIDATEFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link REASSIGNFTCONTRACTFSFSRES }
     * 
     */
    public REASSIGNFTCONTRACTFSFSRES createREASSIGNFTCONTRACTFSFSRES() {
        return new REASSIGNFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTFSFSREQ }
     * 
     */
    public LIQUIDATEFTCONTRACTFSFSREQ createLIQUIDATEFTCONTRACTFSFSREQ() {
        return new LIQUIDATEFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTIOPKREQ }
     * 
     */
    public CREATEPRODUCTIOPKREQ createCREATEPRODUCTIOPKREQ() {
        return new CREATEPRODUCTIOPKREQ();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTIOPKRES }
     * 
     */
    public CREATEPRODUCTIOPKRES createCREATEPRODUCTIOPKRES() {
        return new CREATEPRODUCTIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEMT101IOPKREQ }
     * 
     */
    public DELETEMT101IOPKREQ createDELETEMT101IOPKREQ() {
        return new DELETEMT101IOPKREQ();
    }

    /**
     * Create an instance of {@link QUERYMT101IOFSRES }
     * 
     */
    public QUERYMT101IOFSRES createQUERYMT101IOFSRES() {
        return new QUERYMT101IOFSRES();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTFSFSREQ }
     * 
     */
    public CREATEFTCONTRACTFSFSREQ createCREATEFTCONTRACTFSFSREQ() {
        return new CREATEFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTFSFSRES }
     * 
     */
    public CREATEFTCONTRACTFSFSRES createCREATEFTCONTRACTFSFSRES() {
        return new CREATEFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTFSFSRES }
     * 
     */
    public REVERSEFTCONTRACTFSFSRES createREVERSEFTCONTRACTFSFSRES() {
        return new REVERSEFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link QUERYMT101IOFSREQ }
     * 
     */
    public QUERYMT101IOFSREQ createQUERYMT101IOFSREQ() {
        return new QUERYMT101IOFSREQ();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTFSFSRES }
     * 
     */
    public CANCELFTCONTRACTFSFSRES createCANCELFTCONTRACTFSFSRES() {
        return new CANCELFTCONTRACTFSFSRES();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTFSFSREQ }
     * 
     */
    public REVERSEFTCONTRACTFSFSREQ createREVERSEFTCONTRACTFSFSREQ() {
        return new REVERSEFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTIOPKREQ }
     * 
     */
    public CANCELFTCONTRACTIOPKREQ createCANCELFTCONTRACTIOPKREQ() {
        return new CANCELFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTIOPKRES }
     * 
     */
    public CANCELFTCONTRACTIOPKRES createCANCELFTCONTRACTIOPKRES() {
        return new CANCELFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link QUERYPRODUCTIOFSRES }
     * 
     */
    public QUERYPRODUCTIOFSRES createQUERYPRODUCTIOFSRES() {
        return new QUERYPRODUCTIOFSRES();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTFSFSREQ }
     * 
     */
    public CANCELFTCONTRACTFSFSREQ createCANCELFTCONTRACTFSFSREQ() {
        return new CANCELFTCONTRACTFSFSREQ();
    }

    /**
     * Create an instance of {@link QUERYPRODUCTIOFSREQ }
     * 
     */
    public QUERYPRODUCTIOFSREQ createQUERYPRODUCTIOFSREQ() {
        return new QUERYPRODUCTIOFSREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTFSFSREQ }
     * 
     */
    public AUTHORIZEPRODUCTFSFSREQ createAUTHORIZEPRODUCTFSFSREQ() {
        return new AUTHORIZEPRODUCTFSFSREQ();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTIOPKREQ }
     * 
     */
    public AMENDFTCONTRACTIOPKREQ createAMENDFTCONTRACTIOPKREQ() {
        return new AMENDFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTIOPKREQ }
     * 
     */
    public DELETEPRODUCTIOPKREQ createDELETEPRODUCTIOPKREQ() {
        return new DELETEPRODUCTIOPKREQ();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTIOPKRES }
     * 
     */
    public AMENDFTCONTRACTIOPKRES createAMENDFTCONTRACTIOPKRES() {
        return new AMENDFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTIOPKREQ }
     * 
     */
    public REVERSEFTCONTRACTIOPKREQ createREVERSEFTCONTRACTIOPKREQ() {
        return new REVERSEFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTFSFSRES }
     * 
     */
    public AUTHORIZEPRODUCTFSFSRES createAUTHORIZEPRODUCTFSFSRES() {
        return new AUTHORIZEPRODUCTFSFSRES();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTIOPKRES }
     * 
     */
    public REVERSEFTCONTRACTIOPKRES createREVERSEFTCONTRACTIOPKRES() {
        return new REVERSEFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTIOPKRES }
     * 
     */
    public DELETEPRODUCTIOPKRES createDELETEPRODUCTIOPKRES() {
        return new DELETEPRODUCTIOPKRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTIOPKRES }
     * 
     */
    public AUTHORIZEPRODUCTIOPKRES createAUTHORIZEPRODUCTIOPKRES() {
        return new AUTHORIZEPRODUCTIOPKRES();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTIOPKRES }
     * 
     */
    public DELETEFTCONTRACTIOPKRES createDELETEFTCONTRACTIOPKRES() {
        return new DELETEFTCONTRACTIOPKRES();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTIOPKREQ }
     * 
     */
    public AUTHORIZEPRODUCTIOPKREQ createAUTHORIZEPRODUCTIOPKREQ() {
        return new AUTHORIZEPRODUCTIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTIOPKREQ }
     * 
     */
    public DELETEFTCONTRACTIOPKREQ createDELETEFTCONTRACTIOPKREQ() {
        return new DELETEFTCONTRACTIOPKREQ();
    }

    /**
     * Create an instance of {@link DELETEMT101IOPKRES }
     * 
     */
    public DELETEMT101IOPKRES createDELETEMT101IOPKRES() {
        return new DELETEMT101IOPKRES();
    }

    /**
     * Create an instance of {@link ProdEventDtlsModifyIOType }
     * 
     */
    public ProdEventDtlsModifyIOType createProdEventDtlsModifyIOType() {
        return new ProdEventDtlsModifyIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsModifyIOType.EventDetails }
     * 
     */
    public ProdEventDtlsModifyIOType.EventDetails createProdEventDtlsModifyIOTypeEventDetails() {
        return new ProdEventDtlsModifyIOType.EventDetails();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingFullType }
     * 
     */
    public RoleToHeadMappingFullType createRoleToHeadMappingFullType() {
        return new RoleToHeadMappingFullType();
    }

    /**
     * Create an instance of {@link ProductUDFCreateIOType }
     * 
     */
    public ProductUDFCreateIOType createProductUDFCreateIOType() {
        return new ProductUDFCreateIOType();
    }

    /**
     * Create an instance of {@link CustRestrictionModifyIOType }
     * 
     */
    public CustRestrictionModifyIOType createCustRestrictionModifyIOType() {
        return new CustRestrictionModifyIOType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsModifyIOType }
     * 
     */
    public ProdMisDetailsModifyIOType createProdMisDetailsModifyIOType() {
        return new ProdMisDetailsModifyIOType();
    }

    /**
     * Create an instance of {@link ProductUDFFullType }
     * 
     */
    public ProductUDFFullType createProductUDFFullType() {
        return new ProductUDFFullType();
    }

    /**
     * Create an instance of {@link ProductModifyIOType }
     * 
     */
    public ProductModifyIOType createProductModifyIOType() {
        return new ProductModifyIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingModifyIOType }
     * 
     */
    public RoleToHeadMappingModifyIOType createRoleToHeadMappingModifyIOType() {
        return new RoleToHeadMappingModifyIOType();
    }

    /**
     * Create an instance of {@link Ftdmt101ModifyIOType }
     * 
     */
    public Ftdmt101ModifyIOType createFtdmt101ModifyIOType() {
        return new Ftdmt101ModifyIOType();
    }

    /**
     * Create an instance of {@link Ftdmt101ModifyIOType.MstmsMt101Detail }
     * 
     */
    public Ftdmt101ModifyIOType.MstmsMt101Detail createFtdmt101ModifyIOTypeMstmsMt101Detail() {
        return new Ftdmt101ModifyIOType.MstmsMt101Detail();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingCreateIOType }
     * 
     */
    public RoleToHeadMappingCreateIOType createRoleToHeadMappingCreateIOType() {
        return new RoleToHeadMappingCreateIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsCreateIOType }
     * 
     */
    public ProdEventDtlsCreateIOType createProdEventDtlsCreateIOType() {
        return new ProdEventDtlsCreateIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsCreateIOType.EventDetails }
     * 
     */
    public ProdEventDtlsCreateIOType.EventDetails createProdEventDtlsCreateIOTypeEventDetails() {
        return new ProdEventDtlsCreateIOType.EventDetails();
    }

    /**
     * Create an instance of {@link BranchRestrictionModifyIOType }
     * 
     */
    public BranchRestrictionModifyIOType createBranchRestrictionModifyIOType() {
        return new BranchRestrictionModifyIOType();
    }

    /**
     * Create an instance of {@link CustRestrictionCreateIOType }
     * 
     */
    public CustRestrictionCreateIOType createCustRestrictionCreateIOType() {
        return new CustRestrictionCreateIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionCreateIOType }
     * 
     */
    public BranchRestrictionCreateIOType createBranchRestrictionCreateIOType() {
        return new BranchRestrictionCreateIOType();
    }

    /**
     * Create an instance of {@link ProductFullType }
     * 
     */
    public ProductFullType createProductFullType() {
        return new ProductFullType();
    }

    /**
     * Create an instance of {@link ProductFullType.TaxScheme }
     * 
     */
    public ProductFullType.TaxScheme createProductFullTypeTaxScheme() {
        return new ProductFullType.TaxScheme();
    }

    /**
     * Create an instance of {@link CustRestrictionFullType }
     * 
     */
    public CustRestrictionFullType createCustRestrictionFullType() {
        return new CustRestrictionFullType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsFullType }
     * 
     */
    public ProdMisDetailsFullType createProdMisDetailsFullType() {
        return new ProdMisDetailsFullType();
    }

    /**
     * Create an instance of {@link ProductUDFModifyIOType }
     * 
     */
    public ProductUDFModifyIOType createProductUDFModifyIOType() {
        return new ProductUDFModifyIOType();
    }

    /**
     * Create an instance of {@link Ftdmt101IOType }
     * 
     */
    public Ftdmt101IOType createFtdmt101IOType() {
        return new Ftdmt101IOType();
    }

    /**
     * Create an instance of {@link Ftdmt101IOType.MstmsMt101Detail }
     * 
     */
    public Ftdmt101IOType.MstmsMt101Detail createFtdmt101IOTypeMstmsMt101Detail() {
        return new Ftdmt101IOType.MstmsMt101Detail();
    }

    /**
     * Create an instance of {@link ProductCreateIOType }
     * 
     */
    public ProductCreateIOType createProductCreateIOType() {
        return new ProductCreateIOType();
    }

    /**
     * Create an instance of {@link ProductCreateIOType.TaxScheme }
     * 
     */
    public ProductCreateIOType.TaxScheme createProductCreateIOTypeTaxScheme() {
        return new ProductCreateIOType.TaxScheme();
    }

    /**
     * Create an instance of {@link Ftdmt101FullType }
     * 
     */
    public Ftdmt101FullType createFtdmt101FullType() {
        return new Ftdmt101FullType();
    }

    /**
     * Create an instance of {@link Ftdmt101FullType.MstmsMt101Detail }
     * 
     */
    public Ftdmt101FullType.MstmsMt101Detail createFtdmt101FullTypeMstmsMt101Detail() {
        return new Ftdmt101FullType.MstmsMt101Detail();
    }

    /**
     * Create an instance of {@link ProdMisDetailsCreateIOType }
     * 
     */
    public ProdMisDetailsCreateIOType createProdMisDetailsCreateIOType() {
        return new ProdMisDetailsCreateIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsFullType }
     * 
     */
    public ProdEventDtlsFullType createProdEventDtlsFullType() {
        return new ProdEventDtlsFullType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsFullType.EventDetails }
     * 
     */
    public ProdEventDtlsFullType.EventDetails createProdEventDtlsFullTypeEventDetails() {
        return new ProdEventDtlsFullType.EventDetails();
    }

    /**
     * Create an instance of {@link BranchRestrictionFullType }
     * 
     */
    public BranchRestrictionFullType createBranchRestrictionFullType() {
        return new BranchRestrictionFullType();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType }
     * 
     */
    public FCUBSHEADERType createFCUBSHEADERType() {
        return new FCUBSHEADERType();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType.ADDL }
     * 
     */
    public FCUBSHEADERType.ADDL createFCUBSHEADERTypeADDL() {
        return new FCUBSHEADERType.ADDL();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTIOPKRES.FCUBSBODY }
     * 
     */
    public MODIFYPRODUCTIOPKRES.FCUBSBODY createMODIFYPRODUCTIOPKRESFCUBSBODY() {
        return new MODIFYPRODUCTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEMT101IOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEMT101IOPKREQ.FCUBSBODY createCREATEMT101IOPKREQFCUBSBODY() {
        return new CREATEMT101IOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEFTCONTRACTIOPKREQ.FCUBSBODY createAUTHORIZEFTCONTRACTIOPKREQFCUBSBODY() {
        return new AUTHORIZEFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTIOPKREQ.FCUBSBODY }
     * 
     */
    public MODIFYPRODUCTIOPKREQ.FCUBSBODY createMODIFYPRODUCTIOPKREQFCUBSBODY() {
        return new MODIFYPRODUCTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEMT101IOPKRES.FCUBSBODY }
     * 
     */
    public CREATEMT101IOPKRES.FCUBSBODY createCREATEMT101IOPKRESFCUBSBODY() {
        return new CREATEMT101IOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYFTCONTRACTIOFSRES.FCUBSBODY }
     * 
     */
    public QUERYFTCONTRACTIOFSRES.FCUBSBODY createQUERYFTCONTRACTIOFSRESFCUBSBODY() {
        return new QUERYFTCONTRACTIOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEFTCONTRACTIOPKRES.FCUBSBODY createAUTHORIZEFTCONTRACTIOPKRESFCUBSBODY() {
        return new AUTHORIZEFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEMT101FSFSRES.FCUBSBODY }
     * 
     */
    public DELETEMT101FSFSRES.FCUBSBODY createDELETEMT101FSFSRESFCUBSBODY() {
        return new DELETEMT101FSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYMT101IOPKREQ.FCUBSBODY }
     * 
     */
    public MODIFYMT101IOPKREQ.FCUBSBODY createMODIFYMT101IOPKREQFCUBSBODY() {
        return new MODIFYMT101IOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYFTCONTRACTIOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYFTCONTRACTIOFSREQ.FCUBSBODY createQUERYFTCONTRACTIOFSREQFCUBSBODY() {
        return new QUERYFTCONTRACTIOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEMT101FSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEMT101FSFSREQ.FCUBSBODY createDELETEMT101FSFSREQFCUBSBODY() {
        return new DELETEMT101FSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTFSFSRES.FCUBSBODY }
     * 
     */
    public MODIFYPRODUCTFSFSRES.FCUBSBODY createMODIFYPRODUCTFSFSRESFCUBSBODY() {
        return new MODIFYPRODUCTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYPRODUCTFSFSREQ.FCUBSBODY }
     * 
     */
    public MODIFYPRODUCTFSFSREQ.FCUBSBODY createMODIFYPRODUCTFSFSREQFCUBSBODY() {
        return new MODIFYPRODUCTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEFTCONTRACTFSFSRES.FCUBSBODY createAUTHORIZEFTCONTRACTFSFSRESFCUBSBODY() {
        return new AUTHORIZEFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEFTCONTRACTFSFSREQ.FCUBSBODY createAUTHORIZEFTCONTRACTFSFSREQFCUBSBODY() {
        return new AUTHORIZEFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101FSFSREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEMT101FSFSREQ.FCUBSBODY createAUTHORIZEMT101FSFSREQFCUBSBODY() {
        return new AUTHORIZEMT101FSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYMT101IOPKRES.FCUBSBODY }
     * 
     */
    public MODIFYMT101IOPKRES.FCUBSBODY createMODIFYMT101IOPKRESFCUBSBODY() {
        return new MODIFYMT101IOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEFTCONTRACTIOPKREQ.FCUBSBODY createCREATEFTCONTRACTIOPKREQFCUBSBODY() {
        return new CREATEFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEMT101FSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEMT101FSFSREQ.FCUBSBODY createCREATEMT101FSFSREQFCUBSBODY() {
        return new CREATEMT101FSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101FSFSRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEMT101FSFSRES.FCUBSBODY createAUTHORIZEMT101FSFSRESFCUBSBODY() {
        return new AUTHORIZEMT101FSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEMT101FSFSRES.FCUBSBODY }
     * 
     */
    public CREATEMT101FSFSRES.FCUBSBODY createCREATEMT101FSFSRESFCUBSBODY() {
        return new CREATEMT101FSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEFTCONTRACTIOPKRES.FCUBSBODY createCREATEFTCONTRACTIOPKRESFCUBSBODY() {
        return new CREATEFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101IOPKRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEMT101IOPKRES.FCUBSBODY createAUTHORIZEMT101IOPKRESFCUBSBODY() {
        return new AUTHORIZEMT101IOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEMT101IOPKREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEMT101IOPKREQ.FCUBSBODY createAUTHORIZEMT101IOPKREQFCUBSBODY() {
        return new AUTHORIZEMT101IOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYMT101FSFSREQ.FCUBSBODY }
     * 
     */
    public MODIFYMT101FSFSREQ.FCUBSBODY createMODIFYMT101FSFSREQFCUBSBODY() {
        return new MODIFYMT101FSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public LIQUIDATEFTCONTRACTIOPKRES.FCUBSBODY createLIQUIDATEFTCONTRACTIOPKRESFCUBSBODY() {
        return new LIQUIDATEFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public LIQUIDATEFTCONTRACTIOPKREQ.FCUBSBODY createLIQUIDATEFTCONTRACTIOPKREQFCUBSBODY() {
        return new LIQUIDATEFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public AMENDFTCONTRACTFSFSREQ.FCUBSBODY createAMENDFTCONTRACTFSFSREQFCUBSBODY() {
        return new AMENDFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEPRODUCTFSFSREQ.FCUBSBODY createCREATEPRODUCTFSFSREQFCUBSBODY() {
        return new CREATEPRODUCTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link MODIFYMT101FSFSRES.FCUBSBODY }
     * 
     */
    public MODIFYMT101FSFSRES.FCUBSBODY createMODIFYMT101FSFSRESFCUBSBODY() {
        return new MODIFYMT101FSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public AMENDFTCONTRACTFSFSRES.FCUBSBODY createAMENDFTCONTRACTFSFSRESFCUBSBODY() {
        return new AMENDFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEPRODUCTFSFSRES.FCUBSBODY createCREATEPRODUCTFSFSRESFCUBSBODY() {
        return new CREATEPRODUCTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEFTCONTRACTFSFSREQ.FCUBSBODY createDELETEFTCONTRACTFSFSREQFCUBSBODY() {
        return new DELETEFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTFSFSREQ.FCUBSBODY }
     * 
     */
    public DELETEPRODUCTFSFSREQ.FCUBSBODY createDELETEPRODUCTFSFSREQFCUBSBODY() {
        return new DELETEPRODUCTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public DELETEFTCONTRACTFSFSRES.FCUBSBODY createDELETEFTCONTRACTFSFSRESFCUBSBODY() {
        return new DELETEFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTFSFSRES.FCUBSBODY }
     * 
     */
    public DELETEPRODUCTFSFSRES.FCUBSBODY createDELETEPRODUCTFSFSRESFCUBSBODY() {
        return new DELETEPRODUCTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REASSIGNFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public REASSIGNFTCONTRACTFSFSREQ.FCUBSBODY createREASSIGNFTCONTRACTFSFSREQFCUBSBODY() {
        return new REASSIGNFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public LIQUIDATEFTCONTRACTFSFSRES.FCUBSBODY createLIQUIDATEFTCONTRACTFSFSRESFCUBSBODY() {
        return new LIQUIDATEFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REASSIGNFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public REASSIGNFTCONTRACTFSFSRES.FCUBSBODY createREASSIGNFTCONTRACTFSFSRESFCUBSBODY() {
        return new REASSIGNFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link LIQUIDATEFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public LIQUIDATEFTCONTRACTFSFSREQ.FCUBSBODY createLIQUIDATEFTCONTRACTFSFSREQFCUBSBODY() {
        return new LIQUIDATEFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTIOPKREQ.FCUBSBODY }
     * 
     */
    public CREATEPRODUCTIOPKREQ.FCUBSBODY createCREATEPRODUCTIOPKREQFCUBSBODY() {
        return new CREATEPRODUCTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEPRODUCTIOPKRES.FCUBSBODY }
     * 
     */
    public CREATEPRODUCTIOPKRES.FCUBSBODY createCREATEPRODUCTIOPKRESFCUBSBODY() {
        return new CREATEPRODUCTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEMT101IOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEMT101IOPKREQ.FCUBSBODY createDELETEMT101IOPKREQFCUBSBODY() {
        return new DELETEMT101IOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYMT101IOFSRES.FCUBSBODY }
     * 
     */
    public QUERYMT101IOFSRES.FCUBSBODY createQUERYMT101IOFSRESFCUBSBODY() {
        return new QUERYMT101IOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public CREATEFTCONTRACTFSFSREQ.FCUBSBODY createCREATEFTCONTRACTFSFSREQFCUBSBODY() {
        return new CREATEFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CREATEFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public CREATEFTCONTRACTFSFSRES.FCUBSBODY createCREATEFTCONTRACTFSFSRESFCUBSBODY() {
        return new CREATEFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public REVERSEFTCONTRACTFSFSRES.FCUBSBODY createREVERSEFTCONTRACTFSFSRESFCUBSBODY() {
        return new REVERSEFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYMT101IOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYMT101IOFSREQ.FCUBSBODY createQUERYMT101IOFSREQFCUBSBODY() {
        return new QUERYMT101IOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTFSFSRES.FCUBSBODY }
     * 
     */
    public CANCELFTCONTRACTFSFSRES.FCUBSBODY createCANCELFTCONTRACTFSFSRESFCUBSBODY() {
        return new CANCELFTCONTRACTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public REVERSEFTCONTRACTFSFSREQ.FCUBSBODY createREVERSEFTCONTRACTFSFSREQFCUBSBODY() {
        return new REVERSEFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public CANCELFTCONTRACTIOPKREQ.FCUBSBODY createCANCELFTCONTRACTIOPKREQFCUBSBODY() {
        return new CANCELFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public CANCELFTCONTRACTIOPKRES.FCUBSBODY createCANCELFTCONTRACTIOPKRESFCUBSBODY() {
        return new CANCELFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYPRODUCTIOFSRES.FCUBSBODY }
     * 
     */
    public QUERYPRODUCTIOFSRES.FCUBSBODY createQUERYPRODUCTIOFSRESFCUBSBODY() {
        return new QUERYPRODUCTIOFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link CANCELFTCONTRACTFSFSREQ.FCUBSBODY }
     * 
     */
    public CANCELFTCONTRACTFSFSREQ.FCUBSBODY createCANCELFTCONTRACTFSFSREQFCUBSBODY() {
        return new CANCELFTCONTRACTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link QUERYPRODUCTIOFSREQ.FCUBSBODY }
     * 
     */
    public QUERYPRODUCTIOFSREQ.FCUBSBODY createQUERYPRODUCTIOFSREQFCUBSBODY() {
        return new QUERYPRODUCTIOFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTFSFSREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEPRODUCTFSFSREQ.FCUBSBODY createAUTHORIZEPRODUCTFSFSREQFCUBSBODY() {
        return new AUTHORIZEPRODUCTFSFSREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public AMENDFTCONTRACTIOPKREQ.FCUBSBODY createAMENDFTCONTRACTIOPKREQFCUBSBODY() {
        return new AMENDFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTIOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEPRODUCTIOPKREQ.FCUBSBODY createDELETEPRODUCTIOPKREQFCUBSBODY() {
        return new DELETEPRODUCTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AMENDFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public AMENDFTCONTRACTIOPKRES.FCUBSBODY createAMENDFTCONTRACTIOPKRESFCUBSBODY() {
        return new AMENDFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public REVERSEFTCONTRACTIOPKREQ.FCUBSBODY createREVERSEFTCONTRACTIOPKREQFCUBSBODY() {
        return new REVERSEFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTFSFSRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEPRODUCTFSFSRES.FCUBSBODY createAUTHORIZEPRODUCTFSFSRESFCUBSBODY() {
        return new AUTHORIZEPRODUCTFSFSRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link REVERSEFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public REVERSEFTCONTRACTIOPKRES.FCUBSBODY createREVERSEFTCONTRACTIOPKRESFCUBSBODY() {
        return new REVERSEFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEPRODUCTIOPKRES.FCUBSBODY }
     * 
     */
    public DELETEPRODUCTIOPKRES.FCUBSBODY createDELETEPRODUCTIOPKRESFCUBSBODY() {
        return new DELETEPRODUCTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTIOPKRES.FCUBSBODY }
     * 
     */
    public AUTHORIZEPRODUCTIOPKRES.FCUBSBODY createAUTHORIZEPRODUCTIOPKRESFCUBSBODY() {
        return new AUTHORIZEPRODUCTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTIOPKRES.FCUBSBODY }
     * 
     */
    public DELETEFTCONTRACTIOPKRES.FCUBSBODY createDELETEFTCONTRACTIOPKRESFCUBSBODY() {
        return new DELETEFTCONTRACTIOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link AUTHORIZEPRODUCTIOPKREQ.FCUBSBODY }
     * 
     */
    public AUTHORIZEPRODUCTIOPKREQ.FCUBSBODY createAUTHORIZEPRODUCTIOPKREQFCUBSBODY() {
        return new AUTHORIZEPRODUCTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEFTCONTRACTIOPKREQ.FCUBSBODY }
     * 
     */
    public DELETEFTCONTRACTIOPKREQ.FCUBSBODY createDELETEFTCONTRACTIOPKREQFCUBSBODY() {
        return new DELETEFTCONTRACTIOPKREQ.FCUBSBODY();
    }

    /**
     * Create an instance of {@link DELETEMT101IOPKRES.FCUBSBODY }
     * 
     */
    public DELETEMT101IOPKRES.FCUBSBODY createDELETEMT101IOPKRESFCUBSBODY() {
        return new DELETEMT101IOPKRES.FCUBSBODY();
    }

    /**
     * Create an instance of {@link ProdMisDetailsLiquidateIOType }
     * 
     */
    public ProdMisDetailsLiquidateIOType createProdMisDetailsLiquidateIOType() {
        return new ProdMisDetailsLiquidateIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsDeleteIOType }
     * 
     */
    public ProdEventDtlsDeleteIOType createProdEventDtlsDeleteIOType() {
        return new ProdEventDtlsDeleteIOType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsConfirmIOType }
     * 
     */
    public ProdMisDetailsConfirmIOType createProdMisDetailsConfirmIOType() {
        return new ProdMisDetailsConfirmIOType();
    }

    /**
     * Create an instance of {@link Ftdmt101PKType }
     * 
     */
    public Ftdmt101PKType createFtdmt101PKType() {
        return new Ftdmt101PKType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsCloseIOType }
     * 
     */
    public ProdMisDetailsCloseIOType createProdMisDetailsCloseIOType() {
        return new ProdMisDetailsCloseIOType();
    }

    /**
     * Create an instance of {@link FTContractLiquidateType }
     * 
     */
    public FTContractLiquidateType createFTContractLiquidateType() {
        return new FTContractLiquidateType();
    }

    /**
     * Create an instance of {@link BranchRestrictionConfirmIOType }
     * 
     */
    public BranchRestrictionConfirmIOType createBranchRestrictionConfirmIOType() {
        return new BranchRestrictionConfirmIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionAuthorizeIOType }
     * 
     */
    public BranchRestrictionAuthorizeIOType createBranchRestrictionAuthorizeIOType() {
        return new BranchRestrictionAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ChgLiqType }
     * 
     */
    public ChgLiqType createChgLiqType() {
        return new ChgLiqType();
    }

    /**
     * Create an instance of {@link ProductUDFCloseIOType }
     * 
     */
    public ProductUDFCloseIOType createProductUDFCloseIOType() {
        return new ProductUDFCloseIOType();
    }

    /**
     * Create an instance of {@link CUSTMISType }
     * 
     */
    public CUSTMISType createCUSTMISType() {
        return new CUSTMISType();
    }

    /**
     * Create an instance of {@link ChargeClaimeType }
     * 
     */
    public ChargeClaimeType createChargeClaimeType() {
        return new ChargeClaimeType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsReverseIOType }
     * 
     */
    public ProdMisDetailsReverseIOType createProdMisDetailsReverseIOType() {
        return new ProdMisDetailsReverseIOType();
    }

    /**
     * Create an instance of {@link ProductReopenIOType }
     * 
     */
    public ProductReopenIOType createProductReopenIOType() {
        return new ProductReopenIOType();
    }

    /**
     * Create an instance of {@link ContractOverrides }
     * 
     */
    public ContractOverrides createContractOverrides() {
        return new ContractOverrides();
    }

    /**
     * Create an instance of {@link SettlementClearingDetails }
     * 
     */
    public SettlementClearingDetails createSettlementClearingDetails() {
        return new SettlementClearingDetails();
    }

    /**
     * Create an instance of {@link ContractReassignType }
     * 
     */
    public ContractReassignType createContractReassignType() {
        return new ContractReassignType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingQueryIOType }
     * 
     */
    public RoleToHeadMappingQueryIOType createRoleToHeadMappingQueryIOType() {
        return new RoleToHeadMappingQueryIOType();
    }

    /**
     * Create an instance of {@link MISDETAILSRESType }
     * 
     */
    public MISDETAILSRESType createMISDETAILSRESType() {
        return new MISDETAILSRESType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsRolloverIOType }
     * 
     */
    public ProdMisDetailsRolloverIOType createProdMisDetailsRolloverIOType() {
        return new ProdMisDetailsRolloverIOType();
    }

    /**
     * Create an instance of {@link FTContractFullType }
     * 
     */
    public FTContractFullType createFTContractFullType() {
        return new FTContractFullType();
    }

    /**
     * Create an instance of {@link ERRORDETAILSType }
     * 
     */
    public ERRORDETAILSType createERRORDETAILSType() {
        return new ERRORDETAILSType();
    }

    /**
     * Create an instance of {@link MISCHANGELOGDetails }
     * 
     */
    public MISCHANGELOGDetails createMISCHANGELOGDetails() {
        return new MISCHANGELOGDetails();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingAuthorizeIOType }
     * 
     */
    public RoleToHeadMappingAuthorizeIOType createRoleToHeadMappingAuthorizeIOType() {
        return new RoleToHeadMappingAuthorizeIOType();
    }

    /**
     * Create an instance of {@link UDFDETAILSType }
     * 
     */
    public UDFDETAILSType createUDFDETAILSType() {
        return new UDFDETAILSType();
    }

    /**
     * Create an instance of {@link CustRestrictionReopenIOType }
     * 
     */
    public CustRestrictionReopenIOType createCustRestrictionReopenIOType() {
        return new CustRestrictionReopenIOType();
    }

    /**
     * Create an instance of {@link ProductUDFQueryIOType }
     * 
     */
    public ProductUDFQueryIOType createProductUDFQueryIOType() {
        return new ProductUDFQueryIOType();
    }

    /**
     * Create an instance of {@link MISDETAILSType }
     * 
     */
    public MISDETAILSType createMISDETAILSType() {
        return new MISDETAILSType();
    }

    /**
     * Create an instance of {@link CustRestrictionAuthorizeIOType }
     * 
     */
    public CustRestrictionAuthorizeIOType createCustRestrictionAuthorizeIOType() {
        return new CustRestrictionAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ProductQueryIOType }
     * 
     */
    public ProductQueryIOType createProductQueryIOType() {
        return new ProductQueryIOType();
    }

    /**
     * Create an instance of {@link TaxDetailType }
     * 
     */
    public TaxDetailType createTaxDetailType() {
        return new TaxDetailType();
    }

    /**
     * Create an instance of {@link MIMAINTACCType }
     * 
     */
    public MIMAINTACCType createMIMAINTACCType() {
        return new MIMAINTACCType();
    }

    /**
     * Create an instance of {@link MISAMENDDetails }
     * 
     */
    public MISAMENDDetails createMISAMENDDetails() {
        return new MISAMENDDetails();
    }

    /**
     * Create an instance of {@link CustRestrictionDeleteIOType }
     * 
     */
    public CustRestrictionDeleteIOType createCustRestrictionDeleteIOType() {
        return new CustRestrictionDeleteIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsCloseIOType }
     * 
     */
    public ProdEventDtlsCloseIOType createProdEventDtlsCloseIOType() {
        return new ProdEventDtlsCloseIOType();
    }

    /**
     * Create an instance of {@link Ftdmt101PKACRType }
     * 
     */
    public Ftdmt101PKACRType createFtdmt101PKACRType() {
        return new Ftdmt101PKACRType();
    }

    /**
     * Create an instance of {@link BranchRestrictionQueryIOType }
     * 
     */
    public BranchRestrictionQueryIOType createBranchRestrictionQueryIOType() {
        return new BranchRestrictionQueryIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsQueryIOType }
     * 
     */
    public ProdEventDtlsQueryIOType createProdEventDtlsQueryIOType() {
        return new ProdEventDtlsQueryIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingDeleteIOType }
     * 
     */
    public RoleToHeadMappingDeleteIOType createRoleToHeadMappingDeleteIOType() {
        return new RoleToHeadMappingDeleteIOType();
    }

    /**
     * Create an instance of {@link WARNINGDETAILSType }
     * 
     */
    public WARNINGDETAILSType createWARNINGDETAILSType() {
        return new WARNINGDETAILSType();
    }

    /**
     * Create an instance of {@link SettlementDetailsType }
     * 
     */
    public SettlementDetailsType createSettlementDetailsType() {
        return new SettlementDetailsType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsReopenIOType }
     * 
     */
    public ProdMisDetailsReopenIOType createProdMisDetailsReopenIOType() {
        return new ProdMisDetailsReopenIOType();
    }

    /**
     * Create an instance of {@link ProductCloseIOType }
     * 
     */
    public ProductCloseIOType createProductCloseIOType() {
        return new ProductCloseIOType();
    }

    /**
     * Create an instance of {@link StopPaymentType }
     * 
     */
    public StopPaymentType createStopPaymentType() {
        return new StopPaymentType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingLiquidateIOType }
     * 
     */
    public RoleToHeadMappingLiquidateIOType createRoleToHeadMappingLiquidateIOType() {
        return new RoleToHeadMappingLiquidateIOType();
    }

    /**
     * Create an instance of {@link CHGSResType }
     * 
     */
    public CHGSResType createCHGSResType() {
        return new CHGSResType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsAuthorizeIOType }
     * 
     */
    public ProdMisDetailsAuthorizeIOType createProdMisDetailsAuthorizeIOType() {
        return new ProdMisDetailsAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ProductUDFReopenIOType }
     * 
     */
    public ProductUDFReopenIOType createProductUDFReopenIOType() {
        return new ProductUDFReopenIOType();
    }

    /**
     * Create an instance of {@link WARNINGType }
     * 
     */
    public WARNINGType createWARNINGType() {
        return new WARNINGType();
    }

    /**
     * Create an instance of {@link ADVDETAILSType }
     * 
     */
    public ADVDETAILSType createADVDETAILSType() {
        return new ADVDETAILSType();
    }

    /**
     * Create an instance of {@link ProductUDFDeleteIOType }
     * 
     */
    public ProductUDFDeleteIOType createProductUDFDeleteIOType() {
        return new ProductUDFDeleteIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionReopenIOType }
     * 
     */
    public BranchRestrictionReopenIOType createBranchRestrictionReopenIOType() {
        return new BranchRestrictionReopenIOType();
    }

    /**
     * Create an instance of {@link ProductUDFLiquidateIOType }
     * 
     */
    public ProductUDFLiquidateIOType createProductUDFLiquidateIOType() {
        return new ProductUDFLiquidateIOType();
    }

    /**
     * Create an instance of {@link ProductUDFAuthorizeIOType }
     * 
     */
    public ProductUDFAuthorizeIOType createProductUDFAuthorizeIOType() {
        return new ProductUDFAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsAuthorizeIOType }
     * 
     */
    public ProdEventDtlsAuthorizeIOType createProdEventDtlsAuthorizeIOType() {
        return new ProdEventDtlsAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ProductUDFConfirmIOType }
     * 
     */
    public ProductUDFConfirmIOType createProductUDFConfirmIOType() {
        return new ProductUDFConfirmIOType();
    }

    /**
     * Create an instance of {@link TaxRuleDetailType }
     * 
     */
    public TaxRuleDetailType createTaxRuleDetailType() {
        return new TaxRuleDetailType();
    }

    /**
     * Create an instance of {@link CusttfrContractDtls }
     * 
     */
    public CusttfrContractDtls createCusttfrContractDtls() {
        return new CusttfrContractDtls();
    }

    /**
     * Create an instance of {@link MISDETAILSREQType }
     * 
     */
    public MISDETAILSREQType createMISDETAILSREQType() {
        return new MISDETAILSREQType();
    }

    /**
     * Create an instance of {@link CustRestrictionReverseIOType }
     * 
     */
    public CustRestrictionReverseIOType createCustRestrictionReverseIOType() {
        return new CustRestrictionReverseIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionReverseIOType }
     * 
     */
    public BranchRestrictionReverseIOType createBranchRestrictionReverseIOType() {
        return new BranchRestrictionReverseIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionCloseIOType }
     * 
     */
    public BranchRestrictionCloseIOType createBranchRestrictionCloseIOType() {
        return new BranchRestrictionCloseIOType();
    }

    /**
     * Create an instance of {@link FTContractIOType }
     * 
     */
    public FTContractIOType createFTContractIOType() {
        return new FTContractIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsReverseIOType }
     * 
     */
    public ProdEventDtlsReverseIOType createProdEventDtlsReverseIOType() {
        return new ProdEventDtlsReverseIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingConfirmIOType }
     * 
     */
    public RoleToHeadMappingConfirmIOType createRoleToHeadMappingConfirmIOType() {
        return new RoleToHeadMappingConfirmIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingReopenIOType }
     * 
     */
    public RoleToHeadMappingReopenIOType createRoleToHeadMappingReopenIOType() {
        return new RoleToHeadMappingReopenIOType();
    }

    /**
     * Create an instance of {@link ProductUDFRolloverIOType }
     * 
     */
    public ProductUDFRolloverIOType createProductUDFRolloverIOType() {
        return new ProductUDFRolloverIOType();
    }

    /**
     * Create an instance of {@link CustRestrictionConfirmIOType }
     * 
     */
    public CustRestrictionConfirmIOType createCustRestrictionConfirmIOType() {
        return new CustRestrictionConfirmIOType();
    }

    /**
     * Create an instance of {@link ProductDeleteIOType }
     * 
     */
    public ProductDeleteIOType createProductDeleteIOType() {
        return new ProductDeleteIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsLiquidateIOType }
     * 
     */
    public ProdEventDtlsLiquidateIOType createProdEventDtlsLiquidateIOType() {
        return new ProdEventDtlsLiquidateIOType();
    }

    /**
     * Create an instance of {@link BranchRestrictionRolloverIOType }
     * 
     */
    public BranchRestrictionRolloverIOType createBranchRestrictionRolloverIOType() {
        return new BranchRestrictionRolloverIOType();
    }

    /**
     * Create an instance of {@link FCUBSNotifHeaderType }
     * 
     */
    public FCUBSNotifHeaderType createFCUBSNotifHeaderType() {
        return new FCUBSNotifHeaderType();
    }

    /**
     * Create an instance of {@link CustRestrictionRolloverIOType }
     * 
     */
    public CustRestrictionRolloverIOType createCustRestrictionRolloverIOType() {
        return new CustRestrictionRolloverIOType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsQueryIOType }
     * 
     */
    public ProdMisDetailsQueryIOType createProdMisDetailsQueryIOType() {
        return new ProdMisDetailsQueryIOType();
    }

    /**
     * Create an instance of {@link CustRestrictionQueryIOType }
     * 
     */
    public CustRestrictionQueryIOType createCustRestrictionQueryIOType() {
        return new CustRestrictionQueryIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingCloseIOType }
     * 
     */
    public RoleToHeadMappingCloseIOType createRoleToHeadMappingCloseIOType() {
        return new RoleToHeadMappingCloseIOType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsConfirmIOType }
     * 
     */
    public ProdEventDtlsConfirmIOType createProdEventDtlsConfirmIOType() {
        return new ProdEventDtlsConfirmIOType();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link TaxMainType }
     * 
     */
    public TaxMainType createTaxMainType() {
        return new TaxMainType();
    }

    /**
     * Create an instance of {@link BranchRestrictionDeleteIOType }
     * 
     */
    public BranchRestrictionDeleteIOType createBranchRestrictionDeleteIOType() {
        return new BranchRestrictionDeleteIOType();
    }

    /**
     * Create an instance of {@link CustRestrictionLiquidateIOType }
     * 
     */
    public CustRestrictionLiquidateIOType createCustRestrictionLiquidateIOType() {
        return new CustRestrictionLiquidateIOType();
    }

    /**
     * Create an instance of {@link MISBALTRANLOGDetails }
     * 
     */
    public MISBALTRANLOGDetails createMISBALTRANLOGDetails() {
        return new MISBALTRANLOGDetails();
    }

    /**
     * Create an instance of {@link ProductAuthorizeIOType }
     * 
     */
    public ProductAuthorizeIOType createProductAuthorizeIOType() {
        return new ProductAuthorizeIOType();
    }

    /**
     * Create an instance of {@link ChargeDetailsType1 }
     * 
     */
    public ChargeDetailsType1 createChargeDetailsType1() {
        return new ChargeDetailsType1();
    }

    /**
     * Create an instance of {@link SettlementMainType }
     * 
     */
    public SettlementMainType createSettlementMainType() {
        return new SettlementMainType();
    }

    /**
     * Create an instance of {@link FTContractPKType }
     * 
     */
    public FTContractPKType createFTContractPKType() {
        return new FTContractPKType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsRolloverIOType }
     * 
     */
    public ProdEventDtlsRolloverIOType createProdEventDtlsRolloverIOType() {
        return new ProdEventDtlsRolloverIOType();
    }

    /**
     * Create an instance of {@link ProjectDetails }
     * 
     */
    public ProjectDetails createProjectDetails() {
        return new ProjectDetails();
    }

    /**
     * Create an instance of {@link SettlementAddlDetailsType }
     * 
     */
    public SettlementAddlDetailsType createSettlementAddlDetailsType() {
        return new SettlementAddlDetailsType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingReverseIOType }
     * 
     */
    public RoleToHeadMappingReverseIOType createRoleToHeadMappingReverseIOType() {
        return new RoleToHeadMappingReverseIOType();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingRolloverIOType }
     * 
     */
    public RoleToHeadMappingRolloverIOType createRoleToHeadMappingRolloverIOType() {
        return new RoleToHeadMappingRolloverIOType();
    }

    /**
     * Create an instance of {@link CHGSReqType }
     * 
     */
    public CHGSReqType createCHGSReqType() {
        return new CHGSReqType();
    }

    /**
     * Create an instance of {@link CustRestrictionCloseIOType }
     * 
     */
    public CustRestrictionCloseIOType createCustRestrictionCloseIOType() {
        return new CustRestrictionCloseIOType();
    }

    /**
     * Create an instance of {@link CHGSAppType }
     * 
     */
    public CHGSAppType createCHGSAppType() {
        return new CHGSAppType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsReopenIOType }
     * 
     */
    public ProdEventDtlsReopenIOType createProdEventDtlsReopenIOType() {
        return new ProdEventDtlsReopenIOType();
    }

    /**
     * Create an instance of {@link ProdMisDetailsDeleteIOType }
     * 
     */
    public ProdMisDetailsDeleteIOType createProdMisDetailsDeleteIOType() {
        return new ProdMisDetailsDeleteIOType();
    }

    /**
     * Create an instance of {@link ProductUDFReverseIOType }
     * 
     */
    public ProductUDFReverseIOType createProductUDFReverseIOType() {
        return new ProductUDFReverseIOType();
    }

    /**
     * Create an instance of {@link ChgAssType }
     * 
     */
    public ChgAssType createChgAssType() {
        return new ChgAssType();
    }

    /**
     * Create an instance of {@link MISRefineRateDetails }
     * 
     */
    public MISRefineRateDetails createMISRefineRateDetails() {
        return new MISRefineRateDetails();
    }

    /**
     * Create an instance of {@link BranchRestrictionLiquidateIOType }
     * 
     */
    public BranchRestrictionLiquidateIOType createBranchRestrictionLiquidateIOType() {
        return new BranchRestrictionLiquidateIOType();
    }

    /**
     * Create an instance of {@link ERRORType }
     * 
     */
    public ERRORType createERRORType() {
        return new ERRORType();
    }

    /**
     * Create an instance of {@link ProdEventDtlsModifyIOType.EventDetails.AccountingEntries }
     * 
     */
    public ProdEventDtlsModifyIOType.EventDetails.AccountingEntries createProdEventDtlsModifyIOTypeEventDetailsAccountingEntries() {
        return new ProdEventDtlsModifyIOType.EventDetails.AccountingEntries();
    }

    /**
     * Create an instance of {@link ProdEventDtlsModifyIOType.EventDetails.Advices }
     * 
     */
    public ProdEventDtlsModifyIOType.EventDetails.Advices createProdEventDtlsModifyIOTypeEventDetailsAdvices() {
        return new ProdEventDtlsModifyIOType.EventDetails.Advices();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingFullType.MappingDetails }
     * 
     */
    public RoleToHeadMappingFullType.MappingDetails createRoleToHeadMappingFullTypeMappingDetails() {
        return new RoleToHeadMappingFullType.MappingDetails();
    }

    /**
     * Create an instance of {@link ProductUDFCreateIOType.FieldDetails }
     * 
     */
    public ProductUDFCreateIOType.FieldDetails createProductUDFCreateIOTypeFieldDetails() {
        return new ProductUDFCreateIOType.FieldDetails();
    }

    /**
     * Create an instance of {@link CustRestrictionModifyIOType.CustCategories }
     * 
     */
    public CustRestrictionModifyIOType.CustCategories createCustRestrictionModifyIOTypeCustCategories() {
        return new CustRestrictionModifyIOType.CustCategories();
    }

    /**
     * Create an instance of {@link CustRestrictionModifyIOType.Customers }
     * 
     */
    public CustRestrictionModifyIOType.Customers createCustRestrictionModifyIOTypeCustomers() {
        return new CustRestrictionModifyIOType.Customers();
    }

    /**
     * Create an instance of {@link ProdMisDetailsModifyIOType.DefaultMisCodes }
     * 
     */
    public ProdMisDetailsModifyIOType.DefaultMisCodes createProdMisDetailsModifyIOTypeDefaultMisCodes() {
        return new ProdMisDetailsModifyIOType.DefaultMisCodes();
    }

    /**
     * Create an instance of {@link ProductUDFFullType.FieldDetails }
     * 
     */
    public ProductUDFFullType.FieldDetails createProductUDFFullTypeFieldDetails() {
        return new ProductUDFFullType.FieldDetails();
    }

    /**
     * Create an instance of {@link ProductModifyIOType.ChargeDetails }
     * 
     */
    public ProductModifyIOType.ChargeDetails createProductModifyIOTypeChargeDetails() {
        return new ProductModifyIOType.ChargeDetails();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingModifyIOType.MappingDetails }
     * 
     */
    public RoleToHeadMappingModifyIOType.MappingDetails createRoleToHeadMappingModifyIOTypeMappingDetails() {
        return new RoleToHeadMappingModifyIOType.MappingDetails();
    }

    /**
     * Create an instance of {@link Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe }
     * 
     */
    public Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe createFtdmt101ModifyIOTypeMstmsMt101DetailMstmsMt101DetailSe() {
        return new Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe();
    }

    /**
     * Create an instance of {@link RoleToHeadMappingCreateIOType.MappingDetails }
     * 
     */
    public RoleToHeadMappingCreateIOType.MappingDetails createRoleToHeadMappingCreateIOTypeMappingDetails() {
        return new RoleToHeadMappingCreateIOType.MappingDetails();
    }

    /**
     * Create an instance of {@link ProdEventDtlsCreateIOType.EventDetails.AccountingEntries }
     * 
     */
    public ProdEventDtlsCreateIOType.EventDetails.AccountingEntries createProdEventDtlsCreateIOTypeEventDetailsAccountingEntries() {
        return new ProdEventDtlsCreateIOType.EventDetails.AccountingEntries();
    }

    /**
     * Create an instance of {@link ProdEventDtlsCreateIOType.EventDetails.Advices }
     * 
     */
    public ProdEventDtlsCreateIOType.EventDetails.Advices createProdEventDtlsCreateIOTypeEventDetailsAdvices() {
        return new ProdEventDtlsCreateIOType.EventDetails.Advices();
    }

    /**
     * Create an instance of {@link BranchRestrictionModifyIOType.BranchList }
     * 
     */
    public BranchRestrictionModifyIOType.BranchList createBranchRestrictionModifyIOTypeBranchList() {
        return new BranchRestrictionModifyIOType.BranchList();
    }

    /**
     * Create an instance of {@link BranchRestrictionModifyIOType.CcyList }
     * 
     */
    public BranchRestrictionModifyIOType.CcyList createBranchRestrictionModifyIOTypeCcyList() {
        return new BranchRestrictionModifyIOType.CcyList();
    }

    /**
     * Create an instance of {@link CustRestrictionCreateIOType.CustCategories }
     * 
     */
    public CustRestrictionCreateIOType.CustCategories createCustRestrictionCreateIOTypeCustCategories() {
        return new CustRestrictionCreateIOType.CustCategories();
    }

    /**
     * Create an instance of {@link CustRestrictionCreateIOType.Customers }
     * 
     */
    public CustRestrictionCreateIOType.Customers createCustRestrictionCreateIOTypeCustomers() {
        return new CustRestrictionCreateIOType.Customers();
    }

    /**
     * Create an instance of {@link BranchRestrictionCreateIOType.BranchList }
     * 
     */
    public BranchRestrictionCreateIOType.BranchList createBranchRestrictionCreateIOTypeBranchList() {
        return new BranchRestrictionCreateIOType.BranchList();
    }

    /**
     * Create an instance of {@link BranchRestrictionCreateIOType.CcyList }
     * 
     */
    public BranchRestrictionCreateIOType.CcyList createBranchRestrictionCreateIOTypeCcyList() {
        return new BranchRestrictionCreateIOType.CcyList();
    }

    /**
     * Create an instance of {@link ProductFullType.ChargeDetails }
     * 
     */
    public ProductFullType.ChargeDetails createProductFullTypeChargeDetails() {
        return new ProductFullType.ChargeDetails();
    }

    /**
     * Create an instance of {@link ProductFullType.TaxScheme.TaxDetails }
     * 
     */
    public ProductFullType.TaxScheme.TaxDetails createProductFullTypeTaxSchemeTaxDetails() {
        return new ProductFullType.TaxScheme.TaxDetails();
    }

    /**
     * Create an instance of {@link CustRestrictionFullType.CustCategories }
     * 
     */
    public CustRestrictionFullType.CustCategories createCustRestrictionFullTypeCustCategories() {
        return new CustRestrictionFullType.CustCategories();
    }

    /**
     * Create an instance of {@link CustRestrictionFullType.Customers }
     * 
     */
    public CustRestrictionFullType.Customers createCustRestrictionFullTypeCustomers() {
        return new CustRestrictionFullType.Customers();
    }

    /**
     * Create an instance of {@link ProdMisDetailsFullType.DefaultMisCodes }
     * 
     */
    public ProdMisDetailsFullType.DefaultMisCodes createProdMisDetailsFullTypeDefaultMisCodes() {
        return new ProdMisDetailsFullType.DefaultMisCodes();
    }

    /**
     * Create an instance of {@link ProductUDFModifyIOType.FieldDetails }
     * 
     */
    public ProductUDFModifyIOType.FieldDetails createProductUDFModifyIOTypeFieldDetails() {
        return new ProductUDFModifyIOType.FieldDetails();
    }

    /**
     * Create an instance of {@link Ftdmt101IOType.MstmsMt101Detail.MstmsMt101DetailSe }
     * 
     */
    public Ftdmt101IOType.MstmsMt101Detail.MstmsMt101DetailSe createFtdmt101IOTypeMstmsMt101DetailMstmsMt101DetailSe() {
        return new Ftdmt101IOType.MstmsMt101Detail.MstmsMt101DetailSe();
    }

    /**
     * Create an instance of {@link ProductCreateIOType.ChargeDetails }
     * 
     */
    public ProductCreateIOType.ChargeDetails createProductCreateIOTypeChargeDetails() {
        return new ProductCreateIOType.ChargeDetails();
    }

    /**
     * Create an instance of {@link ProductCreateIOType.TaxScheme.TaxDetails }
     * 
     */
    public ProductCreateIOType.TaxScheme.TaxDetails createProductCreateIOTypeTaxSchemeTaxDetails() {
        return new ProductCreateIOType.TaxScheme.TaxDetails();
    }

    /**
     * Create an instance of {@link Ftdmt101FullType.MstmsMt101Detail.MstmsMt101DetailSe }
     * 
     */
    public Ftdmt101FullType.MstmsMt101Detail.MstmsMt101DetailSe createFtdmt101FullTypeMstmsMt101DetailMstmsMt101DetailSe() {
        return new Ftdmt101FullType.MstmsMt101Detail.MstmsMt101DetailSe();
    }

    /**
     * Create an instance of {@link ProdMisDetailsCreateIOType.DefaultMisCodes }
     * 
     */
    public ProdMisDetailsCreateIOType.DefaultMisCodes createProdMisDetailsCreateIOTypeDefaultMisCodes() {
        return new ProdMisDetailsCreateIOType.DefaultMisCodes();
    }

    /**
     * Create an instance of {@link ProdEventDtlsFullType.EventDetails.AccountingEntries }
     * 
     */
    public ProdEventDtlsFullType.EventDetails.AccountingEntries createProdEventDtlsFullTypeEventDetailsAccountingEntries() {
        return new ProdEventDtlsFullType.EventDetails.AccountingEntries();
    }

    /**
     * Create an instance of {@link ProdEventDtlsFullType.EventDetails.Advices }
     * 
     */
    public ProdEventDtlsFullType.EventDetails.Advices createProdEventDtlsFullTypeEventDetailsAdvices() {
        return new ProdEventDtlsFullType.EventDetails.Advices();
    }

    /**
     * Create an instance of {@link BranchRestrictionFullType.BranchList }
     * 
     */
    public BranchRestrictionFullType.BranchList createBranchRestrictionFullTypeBranchList() {
        return new BranchRestrictionFullType.BranchList();
    }

    /**
     * Create an instance of {@link BranchRestrictionFullType.CcyList }
     * 
     */
    public BranchRestrictionFullType.CcyList createBranchRestrictionFullTypeCcyList() {
        return new BranchRestrictionFullType.CcyList();
    }

    /**
     * Create an instance of {@link FCUBSHEADERType.ADDL.PARAM }
     * 
     */
    public FCUBSHEADERType.ADDL.PARAM createFCUBSHEADERTypeADDLPARAM() {
        return new FCUBSHEADERType.ADDL.PARAM();
    }

}
