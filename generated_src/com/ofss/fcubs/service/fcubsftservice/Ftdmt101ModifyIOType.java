
package com.ofss.fcubs.service.fcubsftservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Ftdmt101-Modify-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Ftdmt101-Modify-IO-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CUSTOMER_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SENDERS_REF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CUST_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSTRUCTING_PARTY1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSTRUCTING_PARTY2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_IDENTIFIER_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_PARTY_IDENTIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_ADDRESS_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_ADDRESS_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_ADDRESS_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORD_ADDRESS_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AC_SERVICING_INST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AC_SERVICING_INST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REQUEST_EXECUTION_DT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="AUTHORIZATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RECEIVER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MODNO" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="Mstms-Mt101-Detail" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TXN_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FX_DEAL_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TXN_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="TXN_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXCHANGE_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="Mstms-Mt101-Detail-Se" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="SENDERS_REF" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TXN_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTING_PARTY1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTING_PARTY2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_IDENTIFIER_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_PARTY_IDENTIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_ADDRESS_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_ADDRESS_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_ADDRESS_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OC_ADDRESS_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AC_SERVICING_INST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AC_SERVICING_INST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INTERMEDIARY_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AWI_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_ID_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BEN_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REMITTANCE_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REMITTANCE_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REMITTANCE_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REMITTANCE_INFO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REGULATORY_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REGULATORY_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="REGULATORY_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ORG_ORDERED_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                             &lt;element name="ORG_ORDERED_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CHARGE_DETAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CHARGE_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="INSTRUCTION_CODE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ftdmt101-Modify-IO-Type", propOrder = {
    "customerno",
    "sendersref",
    "custrefno",
    "instructingparty1",
    "instructingparty2",
    "ordaccountno",
    "ordidentifiercode",
    "ordpartyidentifier",
    "ordaddressline1",
    "ordaddressline2",
    "ordaddressline3",
    "ordaddressline4",
    "acservicinginst1",
    "acservicinginst2",
    "requestexecutiondt",
    "authorization",
    "receiver",
    "modno",
    "mstmsMt101Detail"
})
public class Ftdmt101ModifyIOType {

    @XmlElement(name = "CUSTOMER_NO")
    protected String customerno;
    @XmlElement(name = "SENDERS_REF")
    protected String sendersref;
    @XmlElement(name = "CUST_REFNO")
    protected String custrefno;
    @XmlElement(name = "INSTRUCTING_PARTY1")
    protected String instructingparty1;
    @XmlElement(name = "INSTRUCTING_PARTY2")
    protected String instructingparty2;
    @XmlElement(name = "ORD_ACCOUNT_NO")
    protected String ordaccountno;
    @XmlElement(name = "ORD_IDENTIFIER_CODE")
    protected String ordidentifiercode;
    @XmlElement(name = "ORD_PARTY_IDENTIFIER")
    protected String ordpartyidentifier;
    @XmlElement(name = "ORD_ADDRESS_LINE1")
    protected String ordaddressline1;
    @XmlElement(name = "ORD_ADDRESS_LINE2")
    protected String ordaddressline2;
    @XmlElement(name = "ORD_ADDRESS_LINE3")
    protected String ordaddressline3;
    @XmlElement(name = "ORD_ADDRESS_LINE4")
    protected String ordaddressline4;
    @XmlElement(name = "AC_SERVICING_INST1")
    protected String acservicinginst1;
    @XmlElement(name = "AC_SERVICING_INST2")
    protected String acservicinginst2;
    @XmlElement(name = "REQUEST_EXECUTION_DT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar requestexecutiondt;
    @XmlElement(name = "AUTHORIZATION")
    protected String authorization;
    @XmlElement(name = "RECEIVER")
    protected String receiver;
    @XmlElement(name = "MODNO")
    protected BigInteger modno;
    @XmlElement(name = "Mstms-Mt101-Detail")
    protected List<Ftdmt101ModifyIOType.MstmsMt101Detail> mstmsMt101Detail;

    /**
     * Gets the value of the customerno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERNO() {
        return customerno;
    }

    /**
     * Sets the value of the customerno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERNO(String value) {
        this.customerno = value;
    }

    /**
     * Gets the value of the sendersref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSENDERSREF() {
        return sendersref;
    }

    /**
     * Sets the value of the sendersref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSENDERSREF(String value) {
        this.sendersref = value;
    }

    /**
     * Gets the value of the custrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTREFNO() {
        return custrefno;
    }

    /**
     * Sets the value of the custrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTREFNO(String value) {
        this.custrefno = value;
    }

    /**
     * Gets the value of the instructingparty1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTINGPARTY1() {
        return instructingparty1;
    }

    /**
     * Sets the value of the instructingparty1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTINGPARTY1(String value) {
        this.instructingparty1 = value;
    }

    /**
     * Gets the value of the instructingparty2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRUCTINGPARTY2() {
        return instructingparty2;
    }

    /**
     * Sets the value of the instructingparty2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRUCTINGPARTY2(String value) {
        this.instructingparty2 = value;
    }

    /**
     * Gets the value of the ordaccountno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDACCOUNTNO() {
        return ordaccountno;
    }

    /**
     * Sets the value of the ordaccountno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDACCOUNTNO(String value) {
        this.ordaccountno = value;
    }

    /**
     * Gets the value of the ordidentifiercode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDIDENTIFIERCODE() {
        return ordidentifiercode;
    }

    /**
     * Sets the value of the ordidentifiercode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDIDENTIFIERCODE(String value) {
        this.ordidentifiercode = value;
    }

    /**
     * Gets the value of the ordpartyidentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDPARTYIDENTIFIER() {
        return ordpartyidentifier;
    }

    /**
     * Sets the value of the ordpartyidentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDPARTYIDENTIFIER(String value) {
        this.ordpartyidentifier = value;
    }

    /**
     * Gets the value of the ordaddressline1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDADDRESSLINE1() {
        return ordaddressline1;
    }

    /**
     * Sets the value of the ordaddressline1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDADDRESSLINE1(String value) {
        this.ordaddressline1 = value;
    }

    /**
     * Gets the value of the ordaddressline2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDADDRESSLINE2() {
        return ordaddressline2;
    }

    /**
     * Sets the value of the ordaddressline2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDADDRESSLINE2(String value) {
        this.ordaddressline2 = value;
    }

    /**
     * Gets the value of the ordaddressline3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDADDRESSLINE3() {
        return ordaddressline3;
    }

    /**
     * Sets the value of the ordaddressline3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDADDRESSLINE3(String value) {
        this.ordaddressline3 = value;
    }

    /**
     * Gets the value of the ordaddressline4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDADDRESSLINE4() {
        return ordaddressline4;
    }

    /**
     * Sets the value of the ordaddressline4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDADDRESSLINE4(String value) {
        this.ordaddressline4 = value;
    }

    /**
     * Gets the value of the acservicinginst1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACSERVICINGINST1() {
        return acservicinginst1;
    }

    /**
     * Sets the value of the acservicinginst1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACSERVICINGINST1(String value) {
        this.acservicinginst1 = value;
    }

    /**
     * Gets the value of the acservicinginst2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACSERVICINGINST2() {
        return acservicinginst2;
    }

    /**
     * Sets the value of the acservicinginst2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACSERVICINGINST2(String value) {
        this.acservicinginst2 = value;
    }

    /**
     * Gets the value of the requestexecutiondt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getREQUESTEXECUTIONDT() {
        return requestexecutiondt;
    }

    /**
     * Sets the value of the requestexecutiondt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setREQUESTEXECUTIONDT(XMLGregorianCalendar value) {
        this.requestexecutiondt = value;
    }

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHORIZATION() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHORIZATION(String value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECEIVER() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECEIVER(String value) {
        this.receiver = value;
    }

    /**
     * Gets the value of the modno property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMODNO() {
        return modno;
    }

    /**
     * Sets the value of the modno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMODNO(BigInteger value) {
        this.modno = value;
    }

    /**
     * Gets the value of the mstmsMt101Detail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mstmsMt101Detail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMstmsMt101Detail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Ftdmt101ModifyIOType.MstmsMt101Detail }
     * 
     * 
     */
    public List<Ftdmt101ModifyIOType.MstmsMt101Detail> getMstmsMt101Detail() {
        if (mstmsMt101Detail == null) {
            mstmsMt101Detail = new ArrayList<Ftdmt101ModifyIOType.MstmsMt101Detail>();
        }
        return this.mstmsMt101Detail;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TXN_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FX_DEAL_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TXN_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="TXN_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXCHANGE_RATE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="Mstms-Mt101-Detail-Se" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="SENDERS_REF" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TXN_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTING_PARTY1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTING_PARTY2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_IDENTIFIER_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_PARTY_IDENTIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_ADDRESS_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_ADDRESS_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_ADDRESS_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OC_ADDRESS_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AC_SERVICING_INST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AC_SERVICING_INST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INTERMEDIARY_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AWI_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_ID_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BEN_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REMITTANCE_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REMITTANCE_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REMITTANCE_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REMITTANCE_INFO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REGULATORY_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REGULATORY_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="REGULATORY_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ORG_ORDERED_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                   &lt;element name="ORG_ORDERED_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CHARGE_DETAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CHARGE_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="INSTRUCTION_CODE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txnrefno",
        "fxdealrefno",
        "txnamt",
        "txnccy",
        "exchangerate",
        "mstmsMt101DetailSe"
    })
    public static class MstmsMt101Detail {

        @XmlElement(name = "TXN_REFNO")
        protected String txnrefno;
        @XmlElement(name = "FX_DEAL_REFNO")
        protected String fxdealrefno;
        @XmlElement(name = "TXN_AMT")
        protected BigDecimal txnamt;
        @XmlElement(name = "TXN_CCY")
        protected String txnccy;
        @XmlElement(name = "EXCHANGE_RATE")
        protected BigDecimal exchangerate;
        @XmlElement(name = "Mstms-Mt101-Detail-Se")
        protected List<Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe> mstmsMt101DetailSe;

        /**
         * Gets the value of the txnrefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTXNREFNO() {
            return txnrefno;
        }

        /**
         * Sets the value of the txnrefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTXNREFNO(String value) {
            this.txnrefno = value;
        }

        /**
         * Gets the value of the fxdealrefno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFXDEALREFNO() {
            return fxdealrefno;
        }

        /**
         * Sets the value of the fxdealrefno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFXDEALREFNO(String value) {
            this.fxdealrefno = value;
        }

        /**
         * Gets the value of the txnamt property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTXNAMT() {
            return txnamt;
        }

        /**
         * Sets the value of the txnamt property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTXNAMT(BigDecimal value) {
            this.txnamt = value;
        }

        /**
         * Gets the value of the txnccy property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTXNCCY() {
            return txnccy;
        }

        /**
         * Sets the value of the txnccy property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTXNCCY(String value) {
            this.txnccy = value;
        }

        /**
         * Gets the value of the exchangerate property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getEXCHANGERATE() {
            return exchangerate;
        }

        /**
         * Sets the value of the exchangerate property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setEXCHANGERATE(BigDecimal value) {
            this.exchangerate = value;
        }

        /**
         * Gets the value of the mstmsMt101DetailSe property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the mstmsMt101DetailSe property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMstmsMt101DetailSe().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe }
         * 
         * 
         */
        public List<Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe> getMstmsMt101DetailSe() {
            if (mstmsMt101DetailSe == null) {
                mstmsMt101DetailSe = new ArrayList<Ftdmt101ModifyIOType.MstmsMt101Detail.MstmsMt101DetailSe>();
            }
            return this.mstmsMt101DetailSe;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SENDERS_REF" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TXN_REFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTING_PARTY1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTING_PARTY2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_ACCOUNT_NO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_IDENTIFIER_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_PARTY_IDENTIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_ADDRESS_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_ADDRESS_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_ADDRESS_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OC_ADDRESS_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AC_SERVICING_INST1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AC_SERVICING_INST2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INTERMEDIARY_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_BIC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_PID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_NCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AWI_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_ID_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_LINE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_LINE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_LINE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BEN_LINE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REMITTANCE_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REMITTANCE_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REMITTANCE_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REMITTANCE_INFO4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REGULATORY_INFO1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REGULATORY_INFO2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="REGULATORY_INFO3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ORG_ORDERED_AMT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *         &lt;element name="ORG_ORDERED_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CHARGE_DETAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CHARGE_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="INSTRUCTION_CODE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "sendersref",
            "txnrefno",
            "instructioncode1",
            "instructingparty1",
            "instructingparty2",
            "ocaccountno",
            "ocidentifiercode",
            "ocpartyidentifier",
            "ocaddressline1",
            "ocaddressline2",
            "ocaddressline3",
            "ocaddressline4",
            "acservicinginst1",
            "acservicinginst2",
            "intermediarybic",
            "intermediarypid",
            "intermediaryncc",
            "intermediaryline1",
            "intermediaryline2",
            "intermediaryline3",
            "intermediaryline4",
            "awibic",
            "awipid",
            "awincc",
            "awiline1",
            "awiline2",
            "awiline3",
            "awiline4",
            "benaccount",
            "benidcode",
            "benline1",
            "benline2",
            "benline3",
            "benline4",
            "remittanceinfo1",
            "remittanceinfo2",
            "remittanceinfo3",
            "remittanceinfo4",
            "regulatoryinfo1",
            "regulatoryinfo2",
            "regulatoryinfo3",
            "orgorderedamt",
            "orgorderedccy",
            "chargedetail",
            "chargeaccount",
            "instructioncode2",
            "instructioncode3",
            "instructioncode4",
            "instructioncode5",
            "instructioncode6"
        })
        public static class MstmsMt101DetailSe {

            @XmlElement(name = "SENDERS_REF", required = true)
            protected String sendersref;
            @XmlElement(name = "TXN_REFNO")
            protected String txnrefno;
            @XmlElement(name = "INSTRUCTION_CODE1")
            protected String instructioncode1;
            @XmlElement(name = "INSTRUCTING_PARTY1")
            protected String instructingparty1;
            @XmlElement(name = "INSTRUCTING_PARTY2")
            protected String instructingparty2;
            @XmlElement(name = "OC_ACCOUNT_NO")
            protected String ocaccountno;
            @XmlElement(name = "OC_IDENTIFIER_CODE")
            protected String ocidentifiercode;
            @XmlElement(name = "OC_PARTY_IDENTIFIER")
            protected String ocpartyidentifier;
            @XmlElement(name = "OC_ADDRESS_LINE1")
            protected String ocaddressline1;
            @XmlElement(name = "OC_ADDRESS_LINE2")
            protected String ocaddressline2;
            @XmlElement(name = "OC_ADDRESS_LINE3")
            protected String ocaddressline3;
            @XmlElement(name = "OC_ADDRESS_LINE4")
            protected String ocaddressline4;
            @XmlElement(name = "AC_SERVICING_INST1")
            protected String acservicinginst1;
            @XmlElement(name = "AC_SERVICING_INST2")
            protected String acservicinginst2;
            @XmlElement(name = "INTERMEDIARY_BIC")
            protected String intermediarybic;
            @XmlElement(name = "INTERMEDIARY_PID")
            protected String intermediarypid;
            @XmlElement(name = "INTERMEDIARY_NCC")
            protected String intermediaryncc;
            @XmlElement(name = "INTERMEDIARY_LINE1")
            protected String intermediaryline1;
            @XmlElement(name = "INTERMEDIARY_LINE2")
            protected String intermediaryline2;
            @XmlElement(name = "INTERMEDIARY_LINE3")
            protected String intermediaryline3;
            @XmlElement(name = "INTERMEDIARY_LINE4")
            protected String intermediaryline4;
            @XmlElement(name = "AWI_BIC")
            protected String awibic;
            @XmlElement(name = "AWI_PID")
            protected String awipid;
            @XmlElement(name = "AWI_NCC")
            protected String awincc;
            @XmlElement(name = "AWI_LINE1")
            protected String awiline1;
            @XmlElement(name = "AWI_LINE2")
            protected String awiline2;
            @XmlElement(name = "AWI_LINE3")
            protected String awiline3;
            @XmlElement(name = "AWI_LINE4")
            protected String awiline4;
            @XmlElement(name = "BEN_ACCOUNT")
            protected String benaccount;
            @XmlElement(name = "BEN_ID_CODE")
            protected String benidcode;
            @XmlElement(name = "BEN_LINE1")
            protected String benline1;
            @XmlElement(name = "BEN_LINE2")
            protected String benline2;
            @XmlElement(name = "BEN_LINE3")
            protected String benline3;
            @XmlElement(name = "BEN_LINE4")
            protected String benline4;
            @XmlElement(name = "REMITTANCE_INFO1")
            protected String remittanceinfo1;
            @XmlElement(name = "REMITTANCE_INFO2")
            protected String remittanceinfo2;
            @XmlElement(name = "REMITTANCE_INFO3")
            protected String remittanceinfo3;
            @XmlElement(name = "REMITTANCE_INFO4")
            protected String remittanceinfo4;
            @XmlElement(name = "REGULATORY_INFO1")
            protected String regulatoryinfo1;
            @XmlElement(name = "REGULATORY_INFO2")
            protected String regulatoryinfo2;
            @XmlElement(name = "REGULATORY_INFO3")
            protected String regulatoryinfo3;
            @XmlElement(name = "ORG_ORDERED_AMT")
            protected BigDecimal orgorderedamt;
            @XmlElement(name = "ORG_ORDERED_CCY")
            protected String orgorderedccy;
            @XmlElement(name = "CHARGE_DETAIL")
            protected String chargedetail;
            @XmlElement(name = "CHARGE_ACCOUNT")
            protected String chargeaccount;
            @XmlElement(name = "INSTRUCTION_CODE2")
            protected String instructioncode2;
            @XmlElement(name = "INSTRUCTION_CODE3")
            protected String instructioncode3;
            @XmlElement(name = "INSTRUCTION_CODE4")
            protected String instructioncode4;
            @XmlElement(name = "INSTRUCTION_CODE5")
            protected String instructioncode5;
            @XmlElement(name = "INSTRUCTION_CODE6")
            protected String instructioncode6;

            /**
             * Gets the value of the sendersref property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSENDERSREF() {
                return sendersref;
            }

            /**
             * Sets the value of the sendersref property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSENDERSREF(String value) {
                this.sendersref = value;
            }

            /**
             * Gets the value of the txnrefno property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTXNREFNO() {
                return txnrefno;
            }

            /**
             * Sets the value of the txnrefno property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTXNREFNO(String value) {
                this.txnrefno = value;
            }

            /**
             * Gets the value of the instructioncode1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE1() {
                return instructioncode1;
            }

            /**
             * Sets the value of the instructioncode1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE1(String value) {
                this.instructioncode1 = value;
            }

            /**
             * Gets the value of the instructingparty1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTINGPARTY1() {
                return instructingparty1;
            }

            /**
             * Sets the value of the instructingparty1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTINGPARTY1(String value) {
                this.instructingparty1 = value;
            }

            /**
             * Gets the value of the instructingparty2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTINGPARTY2() {
                return instructingparty2;
            }

            /**
             * Sets the value of the instructingparty2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTINGPARTY2(String value) {
                this.instructingparty2 = value;
            }

            /**
             * Gets the value of the ocaccountno property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCACCOUNTNO() {
                return ocaccountno;
            }

            /**
             * Sets the value of the ocaccountno property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCACCOUNTNO(String value) {
                this.ocaccountno = value;
            }

            /**
             * Gets the value of the ocidentifiercode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCIDENTIFIERCODE() {
                return ocidentifiercode;
            }

            /**
             * Sets the value of the ocidentifiercode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCIDENTIFIERCODE(String value) {
                this.ocidentifiercode = value;
            }

            /**
             * Gets the value of the ocpartyidentifier property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCPARTYIDENTIFIER() {
                return ocpartyidentifier;
            }

            /**
             * Sets the value of the ocpartyidentifier property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCPARTYIDENTIFIER(String value) {
                this.ocpartyidentifier = value;
            }

            /**
             * Gets the value of the ocaddressline1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCADDRESSLINE1() {
                return ocaddressline1;
            }

            /**
             * Sets the value of the ocaddressline1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCADDRESSLINE1(String value) {
                this.ocaddressline1 = value;
            }

            /**
             * Gets the value of the ocaddressline2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCADDRESSLINE2() {
                return ocaddressline2;
            }

            /**
             * Sets the value of the ocaddressline2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCADDRESSLINE2(String value) {
                this.ocaddressline2 = value;
            }

            /**
             * Gets the value of the ocaddressline3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCADDRESSLINE3() {
                return ocaddressline3;
            }

            /**
             * Sets the value of the ocaddressline3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCADDRESSLINE3(String value) {
                this.ocaddressline3 = value;
            }

            /**
             * Gets the value of the ocaddressline4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOCADDRESSLINE4() {
                return ocaddressline4;
            }

            /**
             * Sets the value of the ocaddressline4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOCADDRESSLINE4(String value) {
                this.ocaddressline4 = value;
            }

            /**
             * Gets the value of the acservicinginst1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACSERVICINGINST1() {
                return acservicinginst1;
            }

            /**
             * Sets the value of the acservicinginst1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACSERVICINGINST1(String value) {
                this.acservicinginst1 = value;
            }

            /**
             * Gets the value of the acservicinginst2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getACSERVICINGINST2() {
                return acservicinginst2;
            }

            /**
             * Sets the value of the acservicinginst2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setACSERVICINGINST2(String value) {
                this.acservicinginst2 = value;
            }

            /**
             * Gets the value of the intermediarybic property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYBIC() {
                return intermediarybic;
            }

            /**
             * Sets the value of the intermediarybic property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYBIC(String value) {
                this.intermediarybic = value;
            }

            /**
             * Gets the value of the intermediarypid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYPID() {
                return intermediarypid;
            }

            /**
             * Sets the value of the intermediarypid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYPID(String value) {
                this.intermediarypid = value;
            }

            /**
             * Gets the value of the intermediaryncc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYNCC() {
                return intermediaryncc;
            }

            /**
             * Sets the value of the intermediaryncc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYNCC(String value) {
                this.intermediaryncc = value;
            }

            /**
             * Gets the value of the intermediaryline1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYLINE1() {
                return intermediaryline1;
            }

            /**
             * Sets the value of the intermediaryline1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYLINE1(String value) {
                this.intermediaryline1 = value;
            }

            /**
             * Gets the value of the intermediaryline2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYLINE2() {
                return intermediaryline2;
            }

            /**
             * Sets the value of the intermediaryline2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYLINE2(String value) {
                this.intermediaryline2 = value;
            }

            /**
             * Gets the value of the intermediaryline3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYLINE3() {
                return intermediaryline3;
            }

            /**
             * Sets the value of the intermediaryline3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYLINE3(String value) {
                this.intermediaryline3 = value;
            }

            /**
             * Gets the value of the intermediaryline4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINTERMEDIARYLINE4() {
                return intermediaryline4;
            }

            /**
             * Sets the value of the intermediaryline4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINTERMEDIARYLINE4(String value) {
                this.intermediaryline4 = value;
            }

            /**
             * Gets the value of the awibic property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWIBIC() {
                return awibic;
            }

            /**
             * Sets the value of the awibic property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWIBIC(String value) {
                this.awibic = value;
            }

            /**
             * Gets the value of the awipid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWIPID() {
                return awipid;
            }

            /**
             * Sets the value of the awipid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWIPID(String value) {
                this.awipid = value;
            }

            /**
             * Gets the value of the awincc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWINCC() {
                return awincc;
            }

            /**
             * Sets the value of the awincc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWINCC(String value) {
                this.awincc = value;
            }

            /**
             * Gets the value of the awiline1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWILINE1() {
                return awiline1;
            }

            /**
             * Sets the value of the awiline1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWILINE1(String value) {
                this.awiline1 = value;
            }

            /**
             * Gets the value of the awiline2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWILINE2() {
                return awiline2;
            }

            /**
             * Sets the value of the awiline2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWILINE2(String value) {
                this.awiline2 = value;
            }

            /**
             * Gets the value of the awiline3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWILINE3() {
                return awiline3;
            }

            /**
             * Sets the value of the awiline3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWILINE3(String value) {
                this.awiline3 = value;
            }

            /**
             * Gets the value of the awiline4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAWILINE4() {
                return awiline4;
            }

            /**
             * Sets the value of the awiline4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAWILINE4(String value) {
                this.awiline4 = value;
            }

            /**
             * Gets the value of the benaccount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENACCOUNT() {
                return benaccount;
            }

            /**
             * Sets the value of the benaccount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENACCOUNT(String value) {
                this.benaccount = value;
            }

            /**
             * Gets the value of the benidcode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENIDCODE() {
                return benidcode;
            }

            /**
             * Sets the value of the benidcode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENIDCODE(String value) {
                this.benidcode = value;
            }

            /**
             * Gets the value of the benline1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENLINE1() {
                return benline1;
            }

            /**
             * Sets the value of the benline1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENLINE1(String value) {
                this.benline1 = value;
            }

            /**
             * Gets the value of the benline2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENLINE2() {
                return benline2;
            }

            /**
             * Sets the value of the benline2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENLINE2(String value) {
                this.benline2 = value;
            }

            /**
             * Gets the value of the benline3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENLINE3() {
                return benline3;
            }

            /**
             * Sets the value of the benline3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENLINE3(String value) {
                this.benline3 = value;
            }

            /**
             * Gets the value of the benline4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBENLINE4() {
                return benline4;
            }

            /**
             * Sets the value of the benline4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBENLINE4(String value) {
                this.benline4 = value;
            }

            /**
             * Gets the value of the remittanceinfo1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREMITTANCEINFO1() {
                return remittanceinfo1;
            }

            /**
             * Sets the value of the remittanceinfo1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREMITTANCEINFO1(String value) {
                this.remittanceinfo1 = value;
            }

            /**
             * Gets the value of the remittanceinfo2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREMITTANCEINFO2() {
                return remittanceinfo2;
            }

            /**
             * Sets the value of the remittanceinfo2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREMITTANCEINFO2(String value) {
                this.remittanceinfo2 = value;
            }

            /**
             * Gets the value of the remittanceinfo3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREMITTANCEINFO3() {
                return remittanceinfo3;
            }

            /**
             * Sets the value of the remittanceinfo3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREMITTANCEINFO3(String value) {
                this.remittanceinfo3 = value;
            }

            /**
             * Gets the value of the remittanceinfo4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREMITTANCEINFO4() {
                return remittanceinfo4;
            }

            /**
             * Sets the value of the remittanceinfo4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREMITTANCEINFO4(String value) {
                this.remittanceinfo4 = value;
            }

            /**
             * Gets the value of the regulatoryinfo1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGULATORYINFO1() {
                return regulatoryinfo1;
            }

            /**
             * Sets the value of the regulatoryinfo1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGULATORYINFO1(String value) {
                this.regulatoryinfo1 = value;
            }

            /**
             * Gets the value of the regulatoryinfo2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGULATORYINFO2() {
                return regulatoryinfo2;
            }

            /**
             * Sets the value of the regulatoryinfo2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGULATORYINFO2(String value) {
                this.regulatoryinfo2 = value;
            }

            /**
             * Gets the value of the regulatoryinfo3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGULATORYINFO3() {
                return regulatoryinfo3;
            }

            /**
             * Sets the value of the regulatoryinfo3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGULATORYINFO3(String value) {
                this.regulatoryinfo3 = value;
            }

            /**
             * Gets the value of the orgorderedamt property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getORGORDEREDAMT() {
                return orgorderedamt;
            }

            /**
             * Sets the value of the orgorderedamt property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setORGORDEREDAMT(BigDecimal value) {
                this.orgorderedamt = value;
            }

            /**
             * Gets the value of the orgorderedccy property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getORGORDEREDCCY() {
                return orgorderedccy;
            }

            /**
             * Sets the value of the orgorderedccy property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setORGORDEREDCCY(String value) {
                this.orgorderedccy = value;
            }

            /**
             * Gets the value of the chargedetail property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCHARGEDETAIL() {
                return chargedetail;
            }

            /**
             * Sets the value of the chargedetail property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCHARGEDETAIL(String value) {
                this.chargedetail = value;
            }

            /**
             * Gets the value of the chargeaccount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCHARGEACCOUNT() {
                return chargeaccount;
            }

            /**
             * Sets the value of the chargeaccount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCHARGEACCOUNT(String value) {
                this.chargeaccount = value;
            }

            /**
             * Gets the value of the instructioncode2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE2() {
                return instructioncode2;
            }

            /**
             * Sets the value of the instructioncode2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE2(String value) {
                this.instructioncode2 = value;
            }

            /**
             * Gets the value of the instructioncode3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE3() {
                return instructioncode3;
            }

            /**
             * Sets the value of the instructioncode3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE3(String value) {
                this.instructioncode3 = value;
            }

            /**
             * Gets the value of the instructioncode4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE4() {
                return instructioncode4;
            }

            /**
             * Sets the value of the instructioncode4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE4(String value) {
                this.instructioncode4 = value;
            }

            /**
             * Gets the value of the instructioncode5 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE5() {
                return instructioncode5;
            }

            /**
             * Sets the value of the instructioncode5 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE5(String value) {
                this.instructioncode5 = value;
            }

            /**
             * Gets the value of the instructioncode6 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getINSTRUCTIONCODE6() {
                return instructioncode6;
            }

            /**
             * Sets the value of the instructioncode6 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setINSTRUCTIONCODE6(String value) {
                this.instructioncode6 = value;
            }

        }

    }

}
