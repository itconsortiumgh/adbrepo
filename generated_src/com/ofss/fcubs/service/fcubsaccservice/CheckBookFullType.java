
package com.ofss.fcubs.service.fcubsaccservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CheckBook-Full-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckBook-Full-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ACC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FCHKNO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CHKL" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ODRDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ISSDT" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ODRDET" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INCLCKBKPRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHQTYP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PRNSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EXTREFNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHQBOOKTYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELADDRS4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DELMODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LANGCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REQSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REQMODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKERDTSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECKERDTSTAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MODNO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AUTHSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RECSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ONCEAUTH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cheque-Status" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CHKBKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHKNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="STAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UDFDETAILS" type="{http://fcubs.ofss.com/service/FCUBSAccService}UDFDETAILSType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckBook-Full-Type", propOrder = {
    "brn",
    "acc",
    "fchkno",
    "chkl",
    "odrdt",
    "issdt",
    "odrdet",
    "inclckbkprn",
    "chqtyp",
    "prnstat",
    "extrefno",
    "chqbooktype",
    "deladdrs1",
    "deladdrs2",
    "deladdrs3",
    "deladdrs4",
    "delmode",
    "langcode",
    "reqstat",
    "reqmode",
    "makerid",
    "makerdtstamp",
    "checkerid",
    "checkerdtstamp",
    "modno",
    "authstat",
    "recstat",
    "onceauth",
    "chequeStatus",
    "udfdetails"
})
public class CheckBookFullType {

    @XmlElement(name = "BRN")
    protected String brn;
    @XmlElement(name = "ACC", required = true)
    protected String acc;
    @XmlElement(name = "FCHKNO", required = true)
    protected BigDecimal fchkno;
    @XmlElement(name = "CHKL", required = true)
    protected BigDecimal chkl;
    @XmlElement(name = "ODRDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar odrdt;
    @XmlElement(name = "ISSDT")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar issdt;
    @XmlElement(name = "ODRDET")
    protected String odrdet;
    @XmlElement(name = "INCLCKBKPRN")
    protected String inclckbkprn;
    @XmlElement(name = "CHQTYP")
    protected String chqtyp;
    @XmlElement(name = "PRNSTAT")
    protected String prnstat;
    @XmlElement(name = "EXTREFNO")
    protected String extrefno;
    @XmlElement(name = "CHQBOOKTYPE")
    protected String chqbooktype;
    @XmlElement(name = "DELADDRS1")
    protected String deladdrs1;
    @XmlElement(name = "DELADDRS2")
    protected String deladdrs2;
    @XmlElement(name = "DELADDRS3")
    protected String deladdrs3;
    @XmlElement(name = "DELADDRS4")
    protected String deladdrs4;
    @XmlElement(name = "DELMODE")
    protected String delmode;
    @XmlElement(name = "LANGCODE")
    protected String langcode;
    @XmlElement(name = "REQSTAT")
    protected String reqstat;
    @XmlElement(name = "REQMODE")
    protected String reqmode;
    @XmlElement(name = "MAKERID")
    protected String makerid;
    @XmlElement(name = "MAKERDTSTAMP")
    protected String makerdtstamp;
    @XmlElement(name = "CHECKERID")
    protected String checkerid;
    @XmlElement(name = "CHECKERDTSTAMP")
    protected String checkerdtstamp;
    @XmlElement(name = "MODNO")
    protected BigDecimal modno;
    @XmlElement(name = "AUTHSTAT")
    protected String authstat;
    @XmlElement(name = "RECSTAT")
    protected String recstat;
    @XmlElement(name = "ONCEAUTH")
    protected String onceauth;
    @XmlElement(name = "Cheque-Status")
    protected List<CheckBookFullType.ChequeStatus> chequeStatus;
    @XmlElement(name = "UDFDETAILS")
    protected List<UDFDETAILSType> udfdetails;

    /**
     * Gets the value of the brn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the acc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACC() {
        return acc;
    }

    /**
     * Sets the value of the acc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACC(String value) {
        this.acc = value;
    }

    /**
     * Gets the value of the fchkno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFCHKNO() {
        return fchkno;
    }

    /**
     * Sets the value of the fchkno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFCHKNO(BigDecimal value) {
        this.fchkno = value;
    }

    /**
     * Gets the value of the chkl property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCHKL() {
        return chkl;
    }

    /**
     * Sets the value of the chkl property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCHKL(BigDecimal value) {
        this.chkl = value;
    }

    /**
     * Gets the value of the odrdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getODRDT() {
        return odrdt;
    }

    /**
     * Sets the value of the odrdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setODRDT(XMLGregorianCalendar value) {
        this.odrdt = value;
    }

    /**
     * Gets the value of the issdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getISSDT() {
        return issdt;
    }

    /**
     * Sets the value of the issdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setISSDT(XMLGregorianCalendar value) {
        this.issdt = value;
    }

    /**
     * Gets the value of the odrdet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getODRDET() {
        return odrdet;
    }

    /**
     * Sets the value of the odrdet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setODRDET(String value) {
        this.odrdet = value;
    }

    /**
     * Gets the value of the inclckbkprn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINCLCKBKPRN() {
        return inclckbkprn;
    }

    /**
     * Sets the value of the inclckbkprn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINCLCKBKPRN(String value) {
        this.inclckbkprn = value;
    }

    /**
     * Gets the value of the chqtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHQTYP() {
        return chqtyp;
    }

    /**
     * Sets the value of the chqtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHQTYP(String value) {
        this.chqtyp = value;
    }

    /**
     * Gets the value of the prnstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRNSTAT() {
        return prnstat;
    }

    /**
     * Sets the value of the prnstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRNSTAT(String value) {
        this.prnstat = value;
    }

    /**
     * Gets the value of the extrefno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTREFNO() {
        return extrefno;
    }

    /**
     * Sets the value of the extrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTREFNO(String value) {
        this.extrefno = value;
    }

    /**
     * Gets the value of the chqbooktype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHQBOOKTYPE() {
        return chqbooktype;
    }

    /**
     * Sets the value of the chqbooktype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHQBOOKTYPE(String value) {
        this.chqbooktype = value;
    }

    /**
     * Gets the value of the deladdrs1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS1() {
        return deladdrs1;
    }

    /**
     * Sets the value of the deladdrs1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS1(String value) {
        this.deladdrs1 = value;
    }

    /**
     * Gets the value of the deladdrs2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS2() {
        return deladdrs2;
    }

    /**
     * Sets the value of the deladdrs2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS2(String value) {
        this.deladdrs2 = value;
    }

    /**
     * Gets the value of the deladdrs3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS3() {
        return deladdrs3;
    }

    /**
     * Sets the value of the deladdrs3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS3(String value) {
        this.deladdrs3 = value;
    }

    /**
     * Gets the value of the deladdrs4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELADDRS4() {
        return deladdrs4;
    }

    /**
     * Sets the value of the deladdrs4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELADDRS4(String value) {
        this.deladdrs4 = value;
    }

    /**
     * Gets the value of the delmode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDELMODE() {
        return delmode;
    }

    /**
     * Sets the value of the delmode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDELMODE(String value) {
        this.delmode = value;
    }

    /**
     * Gets the value of the langcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGCODE() {
        return langcode;
    }

    /**
     * Sets the value of the langcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGCODE(String value) {
        this.langcode = value;
    }

    /**
     * Gets the value of the reqstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREQSTAT() {
        return reqstat;
    }

    /**
     * Sets the value of the reqstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREQSTAT(String value) {
        this.reqstat = value;
    }

    /**
     * Gets the value of the reqmode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREQMODE() {
        return reqmode;
    }

    /**
     * Sets the value of the reqmode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREQMODE(String value) {
        this.reqmode = value;
    }

    /**
     * Gets the value of the makerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKERID() {
        return makerid;
    }

    /**
     * Sets the value of the makerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKERID(String value) {
        this.makerid = value;
    }

    /**
     * Gets the value of the makerdtstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAKERDTSTAMP() {
        return makerdtstamp;
    }

    /**
     * Sets the value of the makerdtstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAKERDTSTAMP(String value) {
        this.makerdtstamp = value;
    }

    /**
     * Gets the value of the checkerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKERID() {
        return checkerid;
    }

    /**
     * Sets the value of the checkerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKERID(String value) {
        this.checkerid = value;
    }

    /**
     * Gets the value of the checkerdtstamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHECKERDTSTAMP() {
        return checkerdtstamp;
    }

    /**
     * Sets the value of the checkerdtstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHECKERDTSTAMP(String value) {
        this.checkerdtstamp = value;
    }

    /**
     * Gets the value of the modno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMODNO() {
        return modno;
    }

    /**
     * Sets the value of the modno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMODNO(BigDecimal value) {
        this.modno = value;
    }

    /**
     * Gets the value of the authstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHSTAT() {
        return authstat;
    }

    /**
     * Sets the value of the authstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHSTAT(String value) {
        this.authstat = value;
    }

    /**
     * Gets the value of the recstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECSTAT() {
        return recstat;
    }

    /**
     * Sets the value of the recstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECSTAT(String value) {
        this.recstat = value;
    }

    /**
     * Gets the value of the onceauth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getONCEAUTH() {
        return onceauth;
    }

    /**
     * Sets the value of the onceauth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setONCEAUTH(String value) {
        this.onceauth = value;
    }

    /**
     * Gets the value of the chequeStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chequeStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChequeStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckBookFullType.ChequeStatus }
     * 
     * 
     */
    public List<CheckBookFullType.ChequeStatus> getChequeStatus() {
        if (chequeStatus == null) {
            chequeStatus = new ArrayList<CheckBookFullType.ChequeStatus>();
        }
        return this.chequeStatus;
    }

    /**
     * Gets the value of the udfdetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the udfdetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUDFDETAILS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UDFDETAILSType }
     * 
     * 
     */
    public List<UDFDETAILSType> getUDFDETAILS() {
        if (udfdetails == null) {
            udfdetails = new ArrayList<UDFDETAILSType>();
        }
        return this.udfdetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CHKBKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHKNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="STAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chkbkno",
        "chkno",
        "stat"
    })
    public static class ChequeStatus {

        @XmlElement(name = "CHKBKNO")
        protected String chkbkno;
        @XmlElement(name = "CHKNO", required = true)
        protected String chkno;
        @XmlElement(name = "STAT")
        protected String stat;

        /**
         * Gets the value of the chkbkno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHKBKNO() {
            return chkbkno;
        }

        /**
         * Sets the value of the chkbkno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHKBKNO(String value) {
            this.chkbkno = value;
        }

        /**
         * Gets the value of the chkno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHKNO() {
            return chkno;
        }

        /**
         * Sets the value of the chkno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHKNO(String value) {
            this.chkno = value;
        }

        /**
         * Gets the value of the stat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTAT() {
            return stat;
        }

        /**
         * Sets the value of the stat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTAT(String value) {
            this.stat = value;
        }

    }

}
