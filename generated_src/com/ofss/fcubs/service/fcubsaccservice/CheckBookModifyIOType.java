
package com.ofss.fcubs.service.fcubsaccservice;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CheckBook-Modify-IO-Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckBook-Modify-IO-Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FCHKNO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ODRDET" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REQSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BRN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MODNO" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="CHKL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cheque-Status" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CHKBKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHKNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="STAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UDFDETAILS" type="{http://fcubs.ofss.com/service/FCUBSAccService}UDFDETAILSType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckBook-Modify-IO-Type", propOrder = {
    "acc",
    "fchkno",
    "odrdet",
    "reqstat",
    "brn",
    "modno",
    "chkl",
    "chequeStatus",
    "udfdetails"
})
public class CheckBookModifyIOType {

    @XmlElement(name = "ACC", required = true)
    protected String acc;
    @XmlElement(name = "FCHKNO", required = true)
    protected BigDecimal fchkno;
    @XmlElement(name = "ODRDET")
    protected String odrdet;
    @XmlElement(name = "REQSTAT")
    protected String reqstat;
    @XmlElement(name = "BRN")
    protected String brn;
    @XmlElement(name = "MODNO")
    protected BigInteger modno;
    @XmlElement(name = "CHKL")
    protected String chkl;
    @XmlElement(name = "Cheque-Status")
    protected List<CheckBookModifyIOType.ChequeStatus> chequeStatus;
    @XmlElement(name = "UDFDETAILS")
    protected List<UDFDETAILSType> udfdetails;

    /**
     * Gets the value of the acc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACC() {
        return acc;
    }

    /**
     * Sets the value of the acc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACC(String value) {
        this.acc = value;
    }

    /**
     * Gets the value of the fchkno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFCHKNO() {
        return fchkno;
    }

    /**
     * Sets the value of the fchkno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFCHKNO(BigDecimal value) {
        this.fchkno = value;
    }

    /**
     * Gets the value of the odrdet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getODRDET() {
        return odrdet;
    }

    /**
     * Sets the value of the odrdet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setODRDET(String value) {
        this.odrdet = value;
    }

    /**
     * Gets the value of the reqstat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREQSTAT() {
        return reqstat;
    }

    /**
     * Sets the value of the reqstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREQSTAT(String value) {
        this.reqstat = value;
    }

    /**
     * Gets the value of the brn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRN() {
        return brn;
    }

    /**
     * Sets the value of the brn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRN(String value) {
        this.brn = value;
    }

    /**
     * Gets the value of the modno property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMODNO() {
        return modno;
    }

    /**
     * Sets the value of the modno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMODNO(BigInteger value) {
        this.modno = value;
    }

    /**
     * Gets the value of the chkl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHKL() {
        return chkl;
    }

    /**
     * Sets the value of the chkl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHKL(String value) {
        this.chkl = value;
    }

    /**
     * Gets the value of the chequeStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chequeStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChequeStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckBookModifyIOType.ChequeStatus }
     * 
     * 
     */
    public List<CheckBookModifyIOType.ChequeStatus> getChequeStatus() {
        if (chequeStatus == null) {
            chequeStatus = new ArrayList<CheckBookModifyIOType.ChequeStatus>();
        }
        return this.chequeStatus;
    }

    /**
     * Gets the value of the udfdetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the udfdetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUDFDETAILS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UDFDETAILSType }
     * 
     * 
     */
    public List<UDFDETAILSType> getUDFDETAILS() {
        if (udfdetails == null) {
            udfdetails = new ArrayList<UDFDETAILSType>();
        }
        return this.udfdetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CHKBKNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHKNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="STAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chkbkno",
        "chkno",
        "stat"
    })
    public static class ChequeStatus {

        @XmlElement(name = "CHKBKNO")
        protected String chkbkno;
        @XmlElement(name = "CHKNO", required = true)
        protected String chkno;
        @XmlElement(name = "STAT")
        protected String stat;

        /**
         * Gets the value of the chkbkno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHKBKNO() {
            return chkbkno;
        }

        /**
         * Sets the value of the chkbkno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHKBKNO(String value) {
            this.chkbkno = value;
        }

        /**
         * Gets the value of the chkno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHKNO() {
            return chkno;
        }

        /**
         * Sets the value of the chkno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHKNO(String value) {
            this.chkno = value;
        }

        /**
         * Gets the value of the stat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTAT() {
            return stat;
        }

        /**
         * Sets the value of the stat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTAT(String value) {
            this.stat = value;
        }

    }

}
