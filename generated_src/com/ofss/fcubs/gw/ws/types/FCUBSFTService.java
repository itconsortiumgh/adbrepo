
package com.ofss.fcubs.gw.ws.types;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.0
 * 
 */
@WebServiceClient(name = "FCUBSFTService", targetNamespace = "http://types.ws.gw.fcubs.ofss.com", wsdlLocation = "http://10.10.1.40:8112/FCUBSFTService/FCUBSFTService?WSDL")
public class FCUBSFTService
    extends Service
{

    private final static URL FCUBSFTSERVICE_WSDL_LOCATION;
    private final static WebServiceException FCUBSFTSERVICE_EXCEPTION;
    private final static QName FCUBSFTSERVICE_QNAME = new QName("http://types.ws.gw.fcubs.ofss.com", "FCUBSFTService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://10.10.1.40:8112/FCUBSFTService/FCUBSFTService?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        FCUBSFTSERVICE_WSDL_LOCATION = url;
        FCUBSFTSERVICE_EXCEPTION = e;
    }

    public FCUBSFTService() {
        super(__getWsdlLocation(), FCUBSFTSERVICE_QNAME);
    }

    public FCUBSFTService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * 
     * @return
     *     returns FCUBSFTServiceSEI
     */
    @WebEndpoint(name = "FCUBSFTServiceSEI")
    public FCUBSFTServiceSEI getFCUBSFTServiceSEI() {
        return super.getPort(new QName("http://types.ws.gw.fcubs.ofss.com", "FCUBSFTServiceSEI"), FCUBSFTServiceSEI.class);
    }

    private static URL __getWsdlLocation() {
        if (FCUBSFTSERVICE_EXCEPTION!= null) {
            throw FCUBSFTSERVICE_EXCEPTION;
        }
        return FCUBSFTSERVICE_WSDL_LOCATION;
    }

}
