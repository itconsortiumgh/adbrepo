
package com.ofss.fcubs.gw.ws.types;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKQUERYIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKQUERYIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CHECKDETAILSQUERYIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CHECKDETAILSQUERYIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCBALIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCBALIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCLASSTFRIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCLASSTFRIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCSUMMIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.QUERYACCSUMMIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.QUERYCUSTACCIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.QUERYCUSTACCIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.QUERYGENADVICEIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.QUERYGENADVICEIOFSRES;
import com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWFSFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWFSFSRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWIOPKREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWIOPKRES;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSQUERYIOFSREQ;
import com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSQUERYIOFSRES;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.0
 * 
 */
@WebService(name = "FCUBSAccServiceSEI", targetNamespace = "http://types.ws.gw.fcubs.ofss.com")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface FCUBSAccServiceSEI {


    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRFSFSRES
     */
    @WebMethod(operationName = "AuthorizeAcClassTfrFS")
    @WebResult(name = "AUTHORIZEACCLASSTFR_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public AUTHORIZEACCLASSTFRFSFSRES authorizeAcClassTfrFS(
        @WebParam(name = "AUTHORIZEACCLASSTFR_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        AUTHORIZEACCLASSTFRFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.AUTHORIZEACCLASSTFRIOPKRES
     */
    @WebMethod(operationName = "AuthorizeAcClassTfrIO")
    @WebResult(name = "AUTHORIZEACCLASSTFR_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public AUTHORIZEACCLASSTFRIOPKRES authorizeAcClassTfrIO(
        @WebParam(name = "AUTHORIZEACCLASSTFR_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        AUTHORIZEACCLASSTFRIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCFSFSRES
     */
    @WebMethod(operationName = "AuthorizeCustAccFS")
    @WebResult(name = "AUTHORIZECUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public AUTHORIZECUSTACCFSFSRES authorizeCustAccFS(
        @WebParam(name = "AUTHORIZECUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        AUTHORIZECUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.AUTHORIZECUSTACCIOPKRES
     */
    @WebMethod(operationName = "AuthorizeCustAccIO")
    @WebResult(name = "AUTHORIZECUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public AUTHORIZECUSTACCIOPKRES authorizeCustAccIO(
        @WebParam(name = "AUTHORIZECUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        AUTHORIZECUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEFSFSRES
     */
    @WebMethod(operationName = "CheckBookAuthoriseFS")
    @WebResult(name = "CHECKBOOKAUTHORISE_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKAUTHORISEFSFSRES checkBookAuthoriseFS(
        @WebParam(name = "CHECKBOOKAUTHORISE_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKAUTHORISEFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKAUTHORISEIOPKRES
     */
    @WebMethod(operationName = "CheckBookAuthoriseIO")
    @WebResult(name = "CHECKBOOKAUTHORISE_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKAUTHORISEIOPKRES checkBookAuthoriseIO(
        @WebParam(name = "CHECKBOOKAUTHORISE_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKAUTHORISEIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEFSFSRES
     */
    @WebMethod(operationName = "CheckBookDeleteFS")
    @WebResult(name = "CHECKBOOKDELETE_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKDELETEFSFSRES checkBookDeleteFS(
        @WebParam(name = "CHECKBOOKDELETE_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKDELETEFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKDELETEIOPKRES
     */
    @WebMethod(operationName = "CheckBookDeleteIO")
    @WebResult(name = "CHECKBOOKDELETE_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKDELETEIOPKRES checkBookDeleteIO(
        @WebParam(name = "CHECKBOOKDELETE_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKDELETEIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYFSFSRES
     */
    @WebMethod(operationName = "CheckBookModifyFS")
    @WebResult(name = "CHECKBOOKMODIFY_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKMODIFYFSFSRES checkBookModifyFS(
        @WebParam(name = "CHECKBOOKMODIFY_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKMODIFYFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKMODIFYIOPKRES
     */
    @WebMethod(operationName = "CheckBookModifyIO")
    @WebResult(name = "CHECKBOOKMODIFY_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKMODIFYIOPKRES checkBookModifyIO(
        @WebParam(name = "CHECKBOOKMODIFY_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKMODIFYIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWFSFSRES
     */
    @WebMethod(operationName = "CheckBookNewFS")
    @WebResult(name = "CHECKBOOKNEW_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKNEWFSFSRES checkBookNewFS(
        @WebParam(name = "CHECKBOOKNEW_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKNEWFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKNEWIOPKRES
     */
    @WebMethod(operationName = "CheckBookNewIO")
    @WebResult(name = "CHECKBOOKNEW_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKNEWIOPKRES checkBookNewIO(
        @WebParam(name = "CHECKBOOKNEW_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKNEWIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKBOOKQUERYIOFSRES
     */
    @WebMethod(operationName = "CheckBookQueryIO")
    @WebResult(name = "CHECKBOOKQUERY_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKBOOKQUERYIOFSRES checkBookQueryIO(
        @WebParam(name = "CHECKBOOKQUERY_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKBOOKQUERYIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CHECKDETAILSQUERYIOFSRES
     */
    @WebMethod(operationName = "CheckDetailsQueryIO")
    @WebResult(name = "CHECKDETAILSQUERY_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CHECKDETAILSQUERYIOFSRES checkDetailsQueryIO(
        @WebParam(name = "CHECKDETAILSQUERY_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CHECKDETAILSQUERYIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCFSFSRES
     */
    @WebMethod(operationName = "CloseCustAccFS")
    @WebResult(name = "CLOSECUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CLOSECUSTACCFSFSRES closeCustAccFS(
        @WebParam(name = "CLOSECUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CLOSECUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CLOSECUSTACCIOPKRES
     */
    @WebMethod(operationName = "CloseCustAccIO")
    @WebResult(name = "CLOSECUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CLOSECUSTACCIOPKRES closeCustAccIO(
        @WebParam(name = "CLOSECUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CLOSECUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRFSFSRES
     */
    @WebMethod(operationName = "CreateAcClassTfrFS")
    @WebResult(name = "CREATEACCLASSTFR_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATEACCLASSTFRFSFSRES createAcClassTfrFS(
        @WebParam(name = "CREATEACCLASSTFR_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATEACCLASSTFRFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATEACCLASSTFRIOPKRES
     */
    @WebMethod(operationName = "CreateAcClassTfrIO")
    @WebResult(name = "CREATEACCLASSTFR_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATEACCLASSTFRIOPKRES createAcClassTfrIO(
        @WebParam(name = "CREATEACCLASSTFR_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATEACCLASSTFRIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCFSFSRES
     */
    @WebMethod(operationName = "CreateCustAccFS")
    @WebResult(name = "CREATECUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATECUSTACCFSFSRES createCustAccFS(
        @WebParam(name = "CREATECUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATECUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATECUSTACCIOPKRES
     */
    @WebMethod(operationName = "CreateCustAccIO")
    @WebResult(name = "CREATECUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATECUSTACCIOPKRES createCustAccIO(
        @WebParam(name = "CREATECUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATECUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMFSFSRES
     */
    @WebMethod(operationName = "CreateTDSimFS")
    @WebResult(name = "CREATETDSIM_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATETDSIMFSFSRES createTDSimFS(
        @WebParam(name = "CREATETDSIM_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATETDSIMFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.CREATETDSIMIOPKRES
     */
    @WebMethod(operationName = "CreateTDSimIO")
    @WebResult(name = "CREATETDSIM_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public CREATETDSIMIOPKRES createTDSimIO(
        @WebParam(name = "CREATETDSIM_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        CREATETDSIMIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRFSFSRES
     */
    @WebMethod(operationName = "DeleteAcClassTfrFS")
    @WebResult(name = "DELETEACCLASSTFR_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public DELETEACCLASSTFRFSFSRES deleteAcClassTfrFS(
        @WebParam(name = "DELETEACCLASSTFR_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        DELETEACCLASSTFRFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.DELETEACCLASSTFRIOPKRES
     */
    @WebMethod(operationName = "DeleteAcClassTfrIO")
    @WebResult(name = "DELETEACCLASSTFR_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public DELETEACCLASSTFRIOPKRES deleteAcClassTfrIO(
        @WebParam(name = "DELETEACCLASSTFR_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        DELETEACCLASSTFRIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCFSFSRES
     */
    @WebMethod(operationName = "DeleteCustAccFS")
    @WebResult(name = "DELETECUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public DELETECUSTACCFSFSRES deleteCustAccFS(
        @WebParam(name = "DELETECUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        DELETECUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.DELETECUSTACCIOPKRES
     */
    @WebMethod(operationName = "DeleteCustAccIO")
    @WebResult(name = "DELETECUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public DELETECUSTACCIOPKRES deleteCustAccIO(
        @WebParam(name = "DELETECUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        DELETECUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRFSFSRES
     */
    @WebMethod(operationName = "ModifyAcClassTfrFS")
    @WebResult(name = "MODIFYACCLASSTFR_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public MODIFYACCLASSTFRFSFSRES modifyAcClassTfrFS(
        @WebParam(name = "MODIFYACCLASSTFR_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        MODIFYACCLASSTFRFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.MODIFYACCLASSTFRIOPKRES
     */
    @WebMethod(operationName = "ModifyAcClassTfrIO")
    @WebResult(name = "MODIFYACCLASSTFR_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public MODIFYACCLASSTFRIOPKRES modifyAcClassTfrIO(
        @WebParam(name = "MODIFYACCLASSTFR_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        MODIFYACCLASSTFRIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCFSFSRES
     */
    @WebMethod(operationName = "ModifyCustAccFS")
    @WebResult(name = "MODIFYCUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public MODIFYCUSTACCFSFSRES modifyCustAccFS(
        @WebParam(name = "MODIFYCUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        MODIFYCUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.MODIFYCUSTACCIOPKRES
     */
    @WebMethod(operationName = "ModifyCustAccIO")
    @WebResult(name = "MODIFYCUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public MODIFYCUSTACCIOPKRES modifyCustAccIO(
        @WebParam(name = "MODIFYCUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        MODIFYCUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.QUERYACCLASSTFRIOFSRES
     */
    @WebMethod(operationName = "QueryAcClassTfrIO")
    @WebResult(name = "QUERYACCLASSTFR_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public QUERYACCLASSTFRIOFSRES queryAcClassTfrIO(
        @WebParam(name = "QUERYACCLASSTFR_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        QUERYACCLASSTFRIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.QUERYACCBALIOFSRES
     */
    @WebMethod(operationName = "QueryAccBalIO")
    @WebResult(name = "QUERYACCBAL_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public QUERYACCBALIOFSRES queryAccBalIO(
        @WebParam(name = "QUERYACCBAL_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        QUERYACCBALIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.QUERYACCSUMMIOFSRES
     */
    @WebMethod(operationName = "QueryAccSummIO")
    @WebResult(name = "QUERYACCSUMM_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public QUERYACCSUMMIOFSRES queryAccSummIO(
        @WebParam(name = "QUERYACCSUMM_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        QUERYACCSUMMIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.QUERYCUSTACCIOFSRES
     */
    @WebMethod(operationName = "QueryCustAccIO")
    @WebResult(name = "QUERYCUSTACC_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public QUERYCUSTACCIOFSRES queryCustAccIO(
        @WebParam(name = "QUERYCUSTACC_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        QUERYCUSTACCIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.QUERYGENADVICEIOFSRES
     */
    @WebMethod(operationName = "QueryGenAdviceIO")
    @WebResult(name = "QUERYGENADVICE_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public QUERYGENADVICEIOFSRES queryGenAdviceIO(
        @WebParam(name = "QUERYGENADVICE_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        QUERYGENADVICEIOFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCFSFSRES
     */
    @WebMethod(operationName = "ReopenCustAccFS")
    @WebResult(name = "REOPENCUSTACC_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public REOPENCUSTACCFSFSRES reopenCustAccFS(
        @WebParam(name = "REOPENCUSTACC_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        REOPENCUSTACCFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.REOPENCUSTACCIOPKRES
     */
    @WebMethod(operationName = "ReopenCustAccIO")
    @WebResult(name = "REOPENCUSTACC_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public REOPENCUSTACCIOPKRES reopenCustAccIO(
        @WebParam(name = "REOPENCUSTACC_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        REOPENCUSTACCIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEFSFSRES
     */
    @WebMethod(operationName = "StopPaymentsAuthorizeFS")
    @WebResult(name = "STOPPAYMENTSAUTHORIZE_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSAUTHORIZEFSFSRES stopPaymentsAuthorizeFS(
        @WebParam(name = "STOPPAYMENTSAUTHORIZE_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSAUTHORIZEFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSAUTHORIZEIOPKRES
     */
    @WebMethod(operationName = "StopPaymentsAuthorizeIO")
    @WebResult(name = "STOPPAYMENTSAUTHORIZE_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSAUTHORIZEIOPKRES stopPaymentsAuthorizeIO(
        @WebParam(name = "STOPPAYMENTSAUTHORIZE_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSAUTHORIZEIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEFSFSRES
     */
    @WebMethod(operationName = "StopPaymentsDeleteFS")
    @WebResult(name = "STOPPAYMENTSDELETE_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSDELETEFSFSRES stopPaymentsDeleteFS(
        @WebParam(name = "STOPPAYMENTSDELETE_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSDELETEFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSDELETEIOPKRES
     */
    @WebMethod(operationName = "StopPaymentsDeleteIO")
    @WebResult(name = "STOPPAYMENTSDELETE_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSDELETEIOPKRES stopPaymentsDeleteIO(
        @WebParam(name = "STOPPAYMENTSDELETE_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSDELETEIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYFSFSRES
     */
    @WebMethod(operationName = "StopPaymentsModifyFS")
    @WebResult(name = "STOPPAYMENTSMODIFY_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSMODIFYFSFSRES stopPaymentsModifyFS(
        @WebParam(name = "STOPPAYMENTSMODIFY_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSMODIFYFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSMODIFYIOPKRES
     */
    @WebMethod(operationName = "StopPaymentsModifyIO")
    @WebResult(name = "STOPPAYMENTSMODIFY_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSMODIFYIOPKRES stopPaymentsModifyIO(
        @WebParam(name = "STOPPAYMENTSMODIFY_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSMODIFYIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWFSFSRES
     */
    @WebMethod(operationName = "StopPaymentsNewFS")
    @WebResult(name = "STOPPAYMENTSNEW_FSFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSNEWFSFSRES stopPaymentsNewFS(
        @WebParam(name = "STOPPAYMENTSNEW_FSFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSNEWFSFSREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSNEWIOPKRES
     */
    @WebMethod(operationName = "StopPaymentsNewIO")
    @WebResult(name = "STOPPAYMENTSNEW_IOPK_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSNEWIOPKRES stopPaymentsNewIO(
        @WebParam(name = "STOPPAYMENTSNEW_IOPK_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSNEWIOPKREQ requestMsg);

    /**
     * 
     * @param requestMsg
     * @return
     *     returns com.ofss.fcubs.service.fcubsaccservice.STOPPAYMENTSQUERYIOFSRES
     */
    @WebMethod(operationName = "StopPaymentsQueryIO")
    @WebResult(name = "STOPPAYMENTSQUERY_IOFS_RES", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "ResponseMsg")
    public STOPPAYMENTSQUERYIOFSRES stopPaymentsQueryIO(
        @WebParam(name = "STOPPAYMENTSQUERY_IOFS_REQ", targetNamespace = "http://fcubs.ofss.com/service/FCUBSAccService", partName = "RequestMsg")
        STOPPAYMENTSQUERYIOFSREQ requestMsg);

}
